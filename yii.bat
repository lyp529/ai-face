@echo off

rem -------------------------------------------------------------
rem  Yii command line bootstrap script for Windows.
rem -------------------------------------------------------------

@setlocal

set YII_PATH=%~dp0/php/

if "%PHP_COMMAND%" == "" set PHP_COMMAND=php.exe

cd php
"%PHP_COMMAND%" "%YII_PATH%yii" %*

@endlocal
