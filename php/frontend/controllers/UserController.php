<?php

namespace frontend\controllers;

use common\constants\C;
use common\models\db\Hospital;
use common\models\db\UserMember;
use common\models\table\TableCfgHospitalRequest;
use common\services\SM;
use frontend\models\form\HospitalForm;
use frontend\models\form\HospitalRequestForm;
use Throwable;
use yii\db\StaleObjectException;


/**
 * Class UserController 用户控制器
 * @package backend\BaseBackendController
 */
class UserController extends BaseFrontendController
{
    /**
     * 微信授权注册
     * @return string
     */
    public function actionRegister() {
        $code = $this->param('code');
        $referrerId = $this->param('referrer_id');
        $this->beginTransaction();
        return $this->done(SM::getUserService()->register($code,$referrerId));
    }

    /**
     * 获取首页信息
     * @return string
     */
    public function actionGetHomeInfo() {
        return $this->done(SM::getUserService()->getHomeInfo());
    }

    /**
     * 上传用户头像
     * @return string
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function actionAddUserIcon() {
        $return = ["code"=>400,"msg"=>"上传失败！"];
        $memberId = SM::getUserService()->getMemberId();
        $data = SM::getUploadService()->uploadImage($memberId);
        if ($data['code'] == 200) {
            $user = UserMember::findOne($memberId);
            $user->icon = $data['data'];
            $user->update() && $return = ["code"=>200,"msg"=>"上传成功！"];
        }
        return $this->done($return);
    }
    /**
     * 医院入驻申请表
     * @return string
     */
    public function actionRequestHospital() {
        $form = HospitalRequestForm::initByParam();
        $this->beginTransaction();
        return $this->done(SM::getUserService()->requestHospital($form));
    }

    /**
     * 是否已经申请了医院入驻（成功或申请中）
     * @return string
     */
    public function actionIsRequestHospital() {
        $isRequest = false;
        $request = TableCfgHospitalRequest::findOne(['member_id'=>SM::getUserService()->getMemberId()]);
        $request && $request->state != 3 && $isRequest = true;
        return $this->done($isRequest);
    }

    /**
     * 获取医院服务类目标签
     * @return string
     */
    public function actionGetHospitalCategory() {
        return $this->done(["code"=>200,"msg"=>"获取成功！","data"=>C::HOSPITAL_CATEGORY]);
    }

    /**
     * 新增医院
     * @return string
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionAddHospital() {
        $form = HospitalForm::initByParam();
        $this->beginTransaction();
        return $this->done(SM::getUserService()->addHospital($form));
    }

    /**
     * 获取医院列表
     * @return string
     */
    public function actionGetHospitalList() {
        $page = $this->param("page",1);
        $rows = $this->param("size",10);
        $addressX = $this->param("address_x");
        $addressY = $this->param("address_y");
        $name = $this->param("name");
        return $this->done(SM::getUserService()->getHospitalList($addressX, $addressY, ($page - 1) * $rows,$rows, $name));
    }

    /**
     * 获取医院经纬度
     * @return string
     */
    public function actionGetHospitalXY() {
        $id = $this->param("id");
        $hospital = Hospital::findOne($id);
        if ($hospital) {
            $data = ["code"=>200,"msg"=>"获取成功！","data"=>['id'=>$hospital->id,'address_x'=>$hospital->address_x,'address_y'=>$hospital->address_y]];
        }else {
            $data = ["code"=>400,"msg"=>"该医院不存在！"];
        }
        return $this->done($data);
    }

    /**
     * 获取医院详情
     * @return string
     */
    public function actionGetHospitalDetails() {
        $id = $this->param("id");
        $addressX = $this->param("address_x");
        $addressY = $this->param("address_y");
        return $this->done(SM::getUserService()->getHospitalDetails($id, $addressX, $addressY));
    }

    /**
     * 申请代理商
     * @return string
     */
    public function actionRequestAgent() {
        $username = $this->param('username');
        $contact_phone = $this->param('contact_phone');
        return $this->done(SM::getUserService()->requestAgent($username, $contact_phone));
    }

    /**
     * 获取两级下线会员
     * @return string
     */
    public function actionExtendUser() {
        return $this->done(SM::getUserService()->extendUser());
    }

    /**
     * 个人中心
     * @return string
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function actionMyCenter() {
        $username = $this->param('username');
        return $this->done(SM::getUserService()->myCenter($username));
    }

    /**
     * 佣金版面
     * @return string
     */
    public function actionBrokeragePlate() {
        return $this->done(SM::getUserService()->brokeragePlate());
    }

    /**
     * 申请提现
     * @return string
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function actionWithdraw() {
        $number = $this->param('bank_number');
        $username = $this->param('bank_username');
        $phone = $this->param('phone');
        $amount = $this->param('amount');
        $this->beginTransaction();
        return $this->done(SM::getUserService()->withdraw($number,$username,$phone,$amount));
    }

    /**
     * 提现记录
     * @return string
     */
    public function actionGetWithdrawLog() {
        return $this->done(SM::getUserService()->getWithdrawLog());
    }
}
