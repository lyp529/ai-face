<?php

namespace frontend\controllers;

use common\services\SM;
use Throwable;
use yii\db\StaleObjectException;

/**
 * Class NotifyController 通知控制器
 * @package backend\BaseBackendController
 */
class NotifyController extends BaseFrontendController
{
    /**
     * 预约医院微信支付异步通知
     * @return string
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionNotifyHospital() {
        return SM::getNotifyService()->notifyHospital();
    }

    /**
     * 面向报告微信支付异步通知
     * @return string
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function actionNotifyFace() {
        return SM::getNotifyService()->notifyFace();
    }
}
