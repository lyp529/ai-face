<?php

namespace frontend\controllers;

use common\services\SM;
use common\utils\FaceAnalyzeUtil;
use Throwable;
use yii\db\StaleObjectException;

/**
 * Class OrderController 订单控制器
 * @package backend\BaseBackendController
 */
class OrderController extends BaseFrontendController
{
    /**
     * 获取预约订单列
     * @return string
     */
    public function actionGetHospitalOrderList() {
        return $this->done(SM::getOrderService()->getHospitalOrderList());
    }

    /**
     * 获取医院订单详情
     * @return string
     */
    public function actionGetHospitalOrderDetails() {
        $id = $this->param("id");
        $addressX = $this->param("address_x");
        $addressY = $this->param("address_y");
        return $this->done(SM::getOrderService()->getHospitalOrderDetails($id, $addressX, $addressY));
    }
    /**
     * 统一下单（医院）
     * @return string
     */
    public function actionHospitalAppointment() {
        $hospitalId = $this->param('hospital_id');
        $services = $this->param('services');
        $username = $this->param('username');
        $contactPhone = $this->param('contact_phone');
        $appointmentTime = $this->param('appointment_time');
        $this->beginTransaction();
        return $this->done(SM::getOrderService()->hospitalAppointment($hospitalId, $services, $username, $contactPhone, $appointmentTime));
    }

    /**
     * 人脸检测与分析
     * @return string
     */
    public function actionFaceDetect() {
        $url = $this->param('url');
        return $this->done(SM::getOrderService()->faceDetect($url));
    }

    /**
     * 删除人脸检测与分析图片（重新上传的情况下使用）
     */
    public function actionDelFaceDetectImg() {
        $url = $this->param('url');
        $urlArr=parse_url($url);
        @unlink($_SERVER['DOCUMENT_ROOT'].$urlArr['path']);
    }

    /**
     * 提交面相报告
     * @return string
     */
    public function actionFaceAnalyze() {
        $orderId = $this->param('order_id');
        return $this->done(SM::getOrderService()->faceAnalyze($orderId));
    }

    /**
     * 面相报告支付
     * @return string
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function actionFaceAnalyzePay() {
        $orderId = $this->param('order_id');
        return $this->done(SM::getOrderService()->faceAnalyzePay($orderId));
    }

    public function actionText() {
        $url = $this->param('url');
        return (new FaceAnalyzeUtil())->faceAnalyzeUrl($url);
    }
    public function actionTextBase64() {
        $data = SM::getUploadService()->uploadImage(SM::getUserService()->getMemberId());
        if ($data['code'] != 200) {
            return '上传失败';
        }
        return (new FaceAnalyzeUtil())->faceAnalyzeUrlBase64($data['data']);
    }
    /**
     * 获取面相报告
     * @return string
     */
    public function actionGetFaceReport() {
        $orderId = $this->param('order_id');
        return $this->done(SM::getOrderService()->getFaceReport($orderId));
    }

    /**
     * 面相报告留言
     * @return string
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionFaceOrderRemark() {
        $orderId = $this->param('order_id');
        $remark = $this->param('remark');
        return $this->done(SM::getOrderService()->faceOrderRemark($orderId,$remark));
    }

    /**
     * 医院账户获取预约订单详情
     * @return string
     */
    public function actionGetReportOrderDetails() {
        $id = $this->param('id');
        return $this->done(SM::getOrderService()->getReportOrderDetails($id));
    }

    /**
     * 医院账户获取预约订单列
     * @return string
     */
    public function actionGetReportOrderList() {
        return $this->done(SM::getOrderService()->getReportOrderList());
    }
}
