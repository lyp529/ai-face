<?php

namespace frontend\controllers;

use common\services\SM;

/**
 * Class UploadController 上传控制器
 * @package backend\BaseBackendController
 */
class UploadController extends BaseFrontendController
{
    /**
     * 小程序上传图片
     * @return string
     */
    public function actionUploadImage() {
        return $this->done(SM::getUploadService()->uploadImage(SM::getUserService()->getMemberId()));
    }
}
