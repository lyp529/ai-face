<?php


namespace frontend\controllers;


use common\base\BaseWebController;
use common\constants\C;
use common\models\db\UserMember;
use common\services\SM;
use common\utils\YiiUtil;
use Throwable;
use yii\base\Action;
use yii\db\StaleObjectException;
use yii\web\BadRequestHttpException;

/**
 * Class BaseFrontendController 前台控制器基类
 * @package payment\controllers
 */
class BaseFrontendController extends BaseWebController {

    /**
     * @param Action $action
     * @return bool
     * @throws BadRequestHttpException
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function beforeAction($action) {
        $uniqueId = $action->getUniqueId();
        if (!$this->isSkipLoginCheckRoute($uniqueId)) {
            $token = $_SERVER['HTTP_TOKEN'];
            if (!$token) {
                $this->done(["code"=>400,"msg"=>"请注册登录！"]);
            }
            $member = UserMember::findOne(['token' => $token]);
            if (!$member) return $this->done(["code"=>403,"msg"=>""]);
            if ($member->token_time < time()) {
                $member->token_time = 30 * 86400 + time();
                $member->update();
            }
            YiiUtil::session(C::SESSION_KEY_USER_ID, $member->id);
        }
        return parent::beforeAction($action);
    }

    /**
     * @param $route
     * @return bool
     */
    private function isSkipLoginCheckRoute($route) {
        $skipLoginCheckRoute = [
            "user/register" => true,
            "user/get-hospital-category" => true,
            "notify/notify-face" => true,
            "notify/notify-hospital" => true,
        ];
        return isset($skipLoginCheckRoute[$route]);
    }
}