<?php
namespace frontend\models\form;

use common\base\BaseForm;
use common\constants\C;
use common\models\db\Hospital;
use common\models\db\HospitalCase;
use common\services\SM;

/**
 * Class HospitalForm 新增医院表的模型
 * @package models\form
 */
class HospitalForm extends BaseForm {
    /**
     * @var int 医院ID
     */
    public $id;
    /**
     * @var string 医院名称
     */
    public $name;
    /**
     * @var string|array 医院服务类目
     */
    public $category;
    /**
     * @var string 联系方式
     */
    public $contact_way;
    /**
     * @var string 门店详细地址
     */
    public $address;
    /**
     * @var string 门店简介
     */
    public $brief;
    /**
     * @var string 经度
     */
    public $address_x;
    /**
     * @var string 纬度
     */
    public $address_y;
    /**
     * @var string 距离
     */
    public $distance;
    /**
     * @var string 医院头像
     */
    public $icon;
    /**
     * @var string 头部图片
     */
    public $head_img;
    /**
     * @var array|string 案例图片路径【数组】
     */
    public $img_url;


    /**
     * 获取医院详情
     * @param Hospital $hospital
     * @param $addressX
     * @param $addressY
     */
    public function getHospitalDetails(Hospital $hospital, $addressX, $addressY) {
        $distance = SM::getUserService()->getDistance($hospital->address_x, $hospital->address_y, (float)$addressX, (float)$addressY);

        $this->id = $hospital->id;
        $this->icon = $hospital->icon;
        $this->head_img = $hospital->head_img;
        $this->name = $hospital->name;
        $this->address = $hospital->address;
        $this->address_x = $hospital->address_x;
        $this->address_y = $hospital->address_y;
        $this->distance = '距离'.$distance.'公里';
        $this->brief = $hospital->brief;
        $this->category = explode(',',$hospital->category);
        $this->img_url = (new HospitalCase())->getHospitalCase($hospital->id);
    }
}
