<?php
namespace frontend\models\form;

use common\base\BaseForm;

/**
 * Class HospitalRequestForm 新增医院申请表的模型
 * @package models\form
 */
class HospitalRequestForm extends BaseForm {
    /**
     * @var string 医院名称
     */
    public $name;
    /**
     * @var string 详细地址
     */
    public $address;
    /**
     * @var string 联系人
     */
    public $contact_person;
    /**
     * @var string 联系方式（手机、座机）
     */
    public $contact_way;
    /**
     * @var string 医院logo
     */
    public $logo_url;
    /**
     * @var string 营业执照
     */
    public $business_license;
    /**
     * @var string 法人正面身份证
     */
    public $id_card_f;
    /**
     * @var string 法人反面身份证
     */
    public $id_card_r;
}
