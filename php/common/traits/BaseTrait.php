<?php

namespace common\traits;

use ReflectionClass;

trait BaseTrait
{
    private static $varMap = [];

    /**
     * 从值获取变量名,注意如果有相同的值会有问题
     */
    public static function getPropertyName($value)
    {
        try {
            if (empty(self::$varMap)) {
                $class = new ReflectionClass(__CLASS__);
                $constMap = $class->getConstants(); //返回所有常量名和值
                self::$varMap = array_flip($constMap);
            }
        } catch (\ReflectionException $e) {
            echo $e;
        }
        return isset(self::$varMap[$value]) ? self::$varMap[$value] : "";

    }
}
