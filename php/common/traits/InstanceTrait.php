<?php


namespace common\traits;


/**
 * Trait InstanceTrait 单例特性代码
 * @package common\traits
 */
trait InstanceTrait {
    /**
     * @var null|static 类单例
     */
    static $shareIns = null;

    /**
     * @return static 获取单例对象
     */
    public static function getIns() {
        if (self::$shareIns === null) {
            self::$shareIns = new static();
        }
        return self::$shareIns;
    }
}