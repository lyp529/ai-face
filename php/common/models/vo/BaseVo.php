<?php


namespace common\models\vo;


use common\models\BaseModel;
use yii\helpers\Json;

/**
 * Class FormVo 所有 ValueObject 的基类
 * @package common\models\vo
 */
class BaseVo extends BaseModel {
    /**
     * @param $jsonStr
     * @return static[]
     */
    public static function initListByJsonStr($jsonStr) {
        $rows = Json::decode($jsonStr);
        return static::initListByArr($rows);
    }

    /**
     * 将数组的数据转为 vo 对象列表
     * @param array $rows 普通数组
     * @return static[]
     */
    public static function initListByArr(array $rows) {
        $voList = [];
        foreach ($rows as $row) {
            $vo = new static();
            $vo->setAttributes($row);
            $voList[] = $vo;
        }
        return $voList;
    }
}