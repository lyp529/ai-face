<?php


namespace common\models\vo;


use common\exceptions\SystemException;

/**
 * Class FileVo 文件vo
 * @package common\models\vo
 */
class FileVo extends BaseVo {
    /**
     * @var int 文件id
     */
    public $id;
    /**
     * @var string 文件名字（上传时的名字）
     */
    public $filename;
    /**
     * @var string 访问路径
     */
    public $url;
}