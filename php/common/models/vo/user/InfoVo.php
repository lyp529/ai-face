<?php


namespace common\models\vo\user;

use common\models\db\UserAdmin;
use common\models\vo\backend\HomeVo;
use common\models\vo\BaseVo;

/**
 * Class UserInfoVo 后台所需的用户数据信息
 * @package common\models\vo
 */
class InfoVo extends BaseVo
{
    /**
     * @var int 用户id
     */
    public $id;
    /**
     * @var string 用户名字
     */
    public $name;
    /**
     * @var string[] 角色权限
     */
    public $roles;
    /**
     * @var HomeVo 首页数据
     */
    public $homeVo;

    /**
     * 初始化数据
     */
    public function init() {
        parent::init();
        $this->homeVo = new HomeVo();
    }

    /**
     * @param $adminId
     */
    public function initByUserModel($adminId) {
        $admin = UserAdmin::findOne($adminId);
        $this->id = $admin->id;
        $this->name = $admin->username;
        $this->roles = ["admin"];
        $this->homeVo->loginTime = $admin->login_time;
        $this->homeVo->loginIp = $admin->login_ip;
        $this->homeVo->lastLoginTime = $admin->last_login_time;
        $this->homeVo->lastLoginIp = $admin->last_login_ip;
    }
}
