<?php


namespace common\models\vo;


/**
 * Class ResponseVo 响应数据
 * @package common\models\vo
 */
class ResponseVo extends BaseVo {
    /**
     * @var int 返回代码
     */
    public $c;
    /**
     * @var string 返回消息
     */
    public $m;
    /**
     * @var mixed 返回数据
     */
    public $d;
    /**
     * @var string 调试返回消息
     */
    public $debug;
}