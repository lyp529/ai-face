<?php


namespace common\models\vo\auth;


use common\models\vo\BaseVo;

/**
 * Class ModuleVo 权限模块vo
 * @package common\models\vo\auth
 */
class ModuleVo extends BaseVo {
    /**
     * @var string 父级菜单名
     */
    public $parentLabel;
    /**
     * @var int 模块值
     */
    public $module;
    /**
     * @var string 模块名字
     */
    public $label;
    /**
     * @var ActionVo[] 动作列表
     */
    public $actionVoList = [];

    /**
     * @param array $values
     * @param bool $safeOnly
     */
    public function setAttributes($values, $safeOnly = true) {
        parent::setAttributes($values, $safeOnly);
        $this->module = (int)$this->module;
        $this->actionVoList = ActionVo::initListByArr($this->actionVoList);
    }
}