<?php


namespace common\models\vo\auth;


use common\models\vo\BaseVo;

/**
 * Class ActionVo 权限动作vo
 * @package common\models\vo\auth
 */
class ActionVo extends BaseVo {
    /**
     * @var int 动作值
     */
    public $action;
    /**
     * @var string 动作标签
     */
    public $label;
    /**
     * @var bool 是否有权限
     */
    public $has = false;

    /**
     * @param array $values
     * @param bool $safeOnly
     */
    public function setAttributes($values, $safeOnly = true) {
        parent::setAttributes($values, $safeOnly);
        $this->action = (int)$this->action;
        $this->has = $this->has === "true";
    }
}