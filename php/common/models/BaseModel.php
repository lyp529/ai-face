<?php


namespace common\models;


use ReflectionObject;
use yii\base\Model;

/**
 * Class BaseModel 所有非数据表模型的基类
 * @package common\models
 */
class BaseModel extends Model
{

    /**
     * 获取第一个错误
     * @param string $attribute
     * @return string
     */
    public function getFirstError($attribute = "") {
        if (empty($attribute)) {
            $errors = $this->getErrors();
            if (count($errors) > 0) {
                $error = current($errors);
                if (is_array($error)) {
                    return current($error);
                } else {
                    return $error;
                }
            }
            return "";
        }
        return parent::getFirstError($attribute);
    }

    /**
     * 自动规则。
     * 使用反射获取类中所有的属性 加到 Model 的 rules() 中
     * @see Model::rules()
     * @return array
     */
    public function rules() {
        $reflect = new ReflectionObject($this);
        $defaultProperties = $reflect->getDefaultProperties();
        if (isset($defaultProperties["_attributes"])) {
            unset($defaultProperties["_attributes"]);
        }
        return [
            [array_keys($defaultProperties), "safe"]
        ];
    }
}
