<?php
namespace common\models\db;

use common\models\table\TableCfgHospitalRequest;

/**
 * 数据表 TableCfgHospitalRequest 的方法扩展 
 */
class HospitalRequest extends TableCfgHospitalRequest 
{
    public function getHospitalRequest($memberId) {
        $where = ['member_id'=>$memberId];
        return HospitalRequest::findOne($where);
    }
}