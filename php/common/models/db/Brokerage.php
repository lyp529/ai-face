<?php
namespace common\models\db;

use common\models\table\TableTabBrokerage;

/**
 * 数据表 TableTabBrokerage 的方法扩展 
 */
class Brokerage extends TableTabBrokerage 
{
    public function add($memberId, $amount, $brType, $state, $icon='', $username='', $bankNumber='', $bankUsername='', $phone='') {
        $brokerage = new TableTabBrokerage();
        $brokerage->member_id = $memberId;
        $brokerage->amount = $amount;
        $brokerage->br_type = $brType;
        $brokerage->state = $state;
        $icon && $brokerage->icon = $icon;
        $username && $brokerage->username = $username;
        $bankNumber && $brokerage->bank_number = $bankNumber;
        $bankUsername && $brokerage->bank_username = $bankUsername;
        $phone && $brokerage->phone = $phone;
        return $brokerage->save();
    }
}