<?php
namespace common\models\db;

use common\models\table\TableCfgHospitalCase;

/**
 * 数据表 TableCfgHospitalCase 的方法扩展 
 */
class HospitalCase extends TableCfgHospitalCase 
{
    public function getHospitalCase($hId) {
        $case = HospitalCase::find()->where(['h_id'=>$hId])->all();
        $list = [];
        foreach ($case as $key=>$value) {
            $list[$key]['sort'] = $value->sort;
            $list[$key]['img_url'] = $value->img_url;
        }
        return $list;
    }
}