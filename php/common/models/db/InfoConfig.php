<?php
namespace common\models\db;

use common\models\table\TableCfgInfoConfig;

/**
 * 数据表 TableCfgInfoConfig 的方法扩展 
 */
class InfoConfig extends TableCfgInfoConfig 
{
    public function getInfoConfigList() {
        return TableCfgInfoConfig::find()->all();
    }
}