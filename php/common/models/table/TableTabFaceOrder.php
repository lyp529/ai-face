<?php

namespace common\models\table;

use Yii;

/**
 * This is the model class for table "tab_face_order".
 *
 * @property int $id 订单ID
 * @property int $member_id 用户ID
 * @property int $re_id 报告ID
 * @property int $order_number 订单号
 * @property string $amount 金额
 * @property string $url 图片url
 * @property int $state 订单状态（1未付款、2付款成功、3付款失败）
 * @property int $gender 1女性，2男性
 * @property int $age 年龄
 * @property string $remark 备注
 * @property int $creation_time 创建时间
 * @property int $update_time 更新时间
 */
class TableTabFaceOrder extends \common\models\table\BaseTable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tab_face_order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['member_id', 're_id', 'order_number', 'amount', 'url', 'gender', 'age', 'creation_time'], 'required'],
            [['member_id', 're_id', 'order_number', 'state', 'gender', 'age', 'creation_time', 'update_time'], 'integer'],
            [['amount'], 'number'],
            [['remark'], 'string'],
            [['url'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '订单ID',
            'member_id' => '用户ID',
            're_id' => '报告ID',
            'order_number' => '订单号',
            'amount' => '金额',
            'url' => '图片url',
            'state' => '订单状态（1未付款、2付款成功、3付款失败）',
            'gender' => '1女性，2男性',
            'age' => '年龄',
            'remark' => '备注',
            'creation_time' => '创建时间',
            'update_time' => '更新时间',
        ];
    }
}
