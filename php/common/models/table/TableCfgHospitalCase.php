<?php

namespace common\models\table;

use Yii;

/**
 * This is the model class for table "cfg_hospital_case".
 *
 * @property int $h_id 医院ID
 * @property int $sort 排序
 * @property string $img_url 图片路径
 */
class TableCfgHospitalCase extends \common\models\table\BaseTable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cfg_hospital_case';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['h_id', 'sort', 'img_url'], 'required'],
            [['h_id', 'sort'], 'integer'],
            [['img_url'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'h_id' => '医院ID',
            'sort' => '排序',
            'img_url' => '图片路径',
        ];
    }
}
