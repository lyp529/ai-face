<?php

namespace common\models\table;

use Yii;

/**
 * This is the model class for table "tab_hospital".
 *
 * @property int $id 医院ID
 * @property int $member_id 用户ID
 * @property string $category 医院服务类目
 * @property string $name 医院名称
 * @property string $icon 医院头像
 * @property string $head_img 头部图片
 * @property string $contact_way 联系方式
 * @property string $address 门店详细地址
 * @property string $brief 门店简介
 * @property string $address_x 经度
 * @property string $address_y 纬度
 * @property int $creation_time 创建时间
 */
class TableTabHospital extends \common\models\table\BaseTable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tab_hospital';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['member_id', 'category', 'name', 'icon', 'head_img', 'contact_way', 'address', 'brief', 'address_x', 'address_y', 'creation_time'], 'required'],
            [['member_id', 'creation_time'], 'integer'],
            [['brief'], 'string'],
            [['address_x', 'address_y'], 'number'],
            [['category'], 'string', 'max' => 500],
            [['name', 'contact_way', 'address'], 'string', 'max' => 100],
            [['icon', 'head_img'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '医院ID',
            'member_id' => '用户ID',
            'category' => '医院服务类目',
            'name' => '医院名称',
            'icon' => '医院头像',
            'head_img' => '头部图片',
            'contact_way' => '联系方式',
            'address' => '门店详细地址',
            'brief' => '门店简介',
            'address_x' => '经度',
            'address_y' => '纬度',
            'creation_time' => '创建时间',
        ];
    }
}
