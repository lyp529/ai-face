<?php

namespace common\models\table;

use Yii;

/**
 * This is the model class for table "cfg_hospital_request".
 *
 * @property int $id
 * @property int $member_id 用户ID
 * @property string $logo_url 医院logo-url
 * @property string $name 医院名称
 * @property string $address 详细地址
 * @property string $contact_person 联系人
 * @property string $contact_way 联系方式（手机、座机）
 * @property string $business_license 营业执照url
 * @property string $id_card_f 法人正面身份证url
 * @property string $id_card_r 法人反面身份证url
 * @property int $state 状态（1审核中、2审核成功、3审核失败）
 * @property int $creation_time 创建时间
 * @property int $update_time 审核时间
 */
class TableCfgHospitalRequest extends \common\models\table\BaseTable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cfg_hospital_request';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['member_id', 'logo_url', 'name', 'address', 'contact_person', 'contact_way', 'business_license', 'id_card_f', 'id_card_r', 'creation_time', 'update_time'], 'required'],
            [['member_id', 'state', 'creation_time', 'update_time'], 'integer'],
            [['logo_url', 'business_license', 'id_card_f', 'id_card_r'], 'string', 'max' => 200],
            [['name', 'address', 'contact_person', 'contact_way'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => '用户ID',
            'logo_url' => '医院logo-url',
            'name' => '医院名称',
            'address' => '详细地址',
            'contact_person' => '联系人',
            'contact_way' => '联系方式（手机、座机）',
            'business_license' => '营业执照url',
            'id_card_f' => '法人正面身份证url',
            'id_card_r' => '法人反面身份证url',
            'state' => '状态（1审核中、2审核成功、3审核失败）',
            'creation_time' => '创建时间',
            'update_time' => '审核时间',
        ];
    }
}
