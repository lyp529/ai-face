<?php

namespace common\models\table;

use Yii;

/**
 * This is the model class for table "tab_brokerage".
 *
 * @property int $id
 * @property int $member_id 用户id
 * @property string $icon 头像
 * @property string $username 用户名
 * @property string $amount 金额
 * @property int $br_type 变动类型（1入账佣金 2提现佣金）
 * @property int $state 状态（1申请中、2完成、3失败）
 * @property string $bank_number 银行卡号
 * @property string $bank_username 银行卡姓名
 * @property string $phone 手机
 * @property int $creation_time 创建时间
 * @property int $update_time 更新时间
 */
class TableTabBrokerage extends \common\models\table\BaseTable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tab_brokerage';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['member_id', 'amount', 'br_type', 'state', 'creation_time'], 'required'],
            [['member_id', 'br_type', 'state', 'creation_time', 'update_time'], 'integer'],
            [['amount'], 'number'],
            [['icon'], 'string', 'max' => 200],
            [['username', 'bank_number', 'bank_username', 'phone'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => '用户id',
            'icon' => '头像',
            'username' => '用户名',
            'amount' => '金额',
            'br_type' => '变动类型（1入账佣金 2提现佣金）',
            'state' => '状态（1申请中、2完成、3失败）',
            'bank_number' => '银行卡号',
            'bank_username' => '银行卡姓名',
            'phone' => '手机',
            'creation_time' => '创建时间',
            'update_time' => '更新时间',
        ];
    }
}
