<?php

namespace common\models\table;

use Yii;

/**
 * This is the model class for table "cfg_member_relation".
 *
 * @property int $superior_id 上级ID
 * @property int $member_id 用户ID
 */
class TableCfgMemberRelation extends \common\models\table\BaseTable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cfg_member_relation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['superior_id', 'member_id'], 'required'],
            [['superior_id', 'member_id'], 'integer'],
            [['member_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'superior_id' => '上级ID',
            'member_id' => '用户ID',
        ];
    }
}
