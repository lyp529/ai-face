<?php

namespace common\models\table;

use Yii;

/**
 * This is the model class for table "tab_agent".
 *
 * @property int $id
 * @property int $member_id 用户ID
 * @property string $username 姓名
 * @property int $state 状态
 * @property int $contact_phone 联系电话
 * @property int $creation_time 创建时间
 * @property int $update_time 更新时间
 */
class TableTabAgent extends \common\models\table\BaseTable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tab_agent';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['member_id', 'username', 'contact_phone', 'creation_time', 'update_time'], 'required'],
            [['member_id', 'state', 'contact_phone', 'creation_time', 'update_time'], 'integer'],
            [['username'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => '用户ID',
            'username' => '姓名',
            'state' => '状态',
            'contact_phone' => '联系电话',
            'creation_time' => '创建时间',
            'update_time' => '更新时间',
        ];
    }
}
