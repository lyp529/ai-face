<?php

namespace common\models\table;

use Yii;

/**
 * This is the model class for table "user_admin".
 *
 * @property int $id 管理员ID
 * @property string $username 管理员登录名
 * @property string $password 密码
 * @property int $login_time 最近登录时间
 * @property string $login_ip 最近登录ip
 * @property int $last_login_time 上次登录时间
 * @property string $last_login_ip 上次登录IP
 * @property int $session_dead_time session有效截止时间
 */
class TableUserAdmin extends \common\models\table\BaseTable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_admin';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            [['login_time', 'last_login_time', 'session_dead_time'], 'integer'],
            [['username', 'login_ip', 'last_login_ip'], 'string', 'max' => 100],
            [['password'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '管理员ID',
            'username' => '管理员登录名',
            'password' => '密码',
            'login_time' => '最近登录时间',
            'login_ip' => '最近登录ip',
            'last_login_time' => '上次登录时间',
            'last_login_ip' => '上次登录IP',
            'session_dead_time' => 'session有效截止时间',
        ];
    }
}
