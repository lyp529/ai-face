<?php

namespace common\models\table;

use Yii;

/**
 * This is the model class for table "user_member".
 *
 * @property int $id 用户ID
 * @property string $brokerage 佣金
 * @property int $referrer_id 推荐人
 * @property string $icon 头像
 * @property string $username 微信名
 * @property string $token
 * @property string $openid 授权用户唯一标识
 * @property int $is_hospital 医院账户（1 是，2 否）
 * @property int $is_agent 代理商账户（1 是，2 否）
 * @property int $creation_time 创建时间
 * @property int $token_time token过期时间
 */
class TableUserMember extends \common\models\table\BaseTable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_member';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['brokerage'], 'number'],
            [['referrer_id', 'is_hospital', 'is_agent', 'creation_time', 'token_time'], 'integer'],
            [['token', 'openid', 'creation_time', 'token_time'], 'required'],
            [['icon'], 'string', 'max' => 200],
            [['username'], 'string', 'max' => 100],
            [['token', 'openid'], 'string', 'max' => 50],
            [['token'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '用户ID',
            'brokerage' => '佣金',
            'referrer_id' => '推荐人',
            'icon' => '头像',
            'username' => '微信名',
            'token' => 'Token',
            'openid' => '授权用户唯一标识',
            'is_hospital' => '医院账户（1 是，2 否）',
            'is_agent' => '代理商账户（1 是，2 否）',
            'creation_time' => '创建时间',
            'token_time' => 'token过期时间',
        ];
    }
}
