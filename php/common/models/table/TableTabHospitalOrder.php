<?php

namespace common\models\table;

use Yii;

/**
 * This is the model class for table "tab_hospital_order".
 *
 * @property int $id 订单ID
 * @property int $member_id 用户ID
 * @property int $order_number 订单号
 * @property string $amount 金额
 * @property string $services 服务项目
 * @property int $hospital_id 医院ID
 * @property string $username 姓名
 * @property string $contact_phone 联系电话
 * @property int $appointment_time 预约时间
 * @property int $state 订单状态（1预约中、2预约成功、3预约失败）
 * @property string $remark 备注
 * @property int $creation_time 创建时间
 * @property int $update_time 更新时间
 */
class TableTabHospitalOrder extends \common\models\table\BaseTable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tab_hospital_order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['member_id', 'order_number', 'amount', 'services', 'hospital_id', 'username', 'contact_phone', 'appointment_time', 'state', 'creation_time', 'update_time'], 'required'],
            [['member_id', 'order_number', 'hospital_id', 'appointment_time', 'state', 'creation_time', 'update_time'], 'integer'],
            [['amount'], 'number'],
            [['remark'], 'string'],
            [['services'], 'string', 'max' => 500],
            [['username', 'contact_phone'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '订单ID',
            'member_id' => '用户ID',
            'order_number' => '订单号',
            'amount' => '金额',
            'services' => '服务项目',
            'hospital_id' => '医院ID',
            'username' => '姓名',
            'contact_phone' => '联系电话',
            'appointment_time' => '预约时间',
            'state' => '订单状态（1预约中、2预约成功、3预约失败）',
            'remark' => '备注',
            'creation_time' => '创建时间',
            'update_time' => '更新时间',
        ];
    }
}
