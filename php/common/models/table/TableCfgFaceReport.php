<?php

namespace common\models\table;

use Yii;

/**
 * This is the model class for table "cfg_face_report".
 *
 * @property int $id 报告ID
 * @property string $overview 面相概述
 * @property string $eye_analysis 眼睛解析
 * @property string $mouth_analysis 嘴相解析
 * @property string $nose_analysis 鼻相解析
 * @property int $eye_top 眼睛上面的距离
 * @property int $eye_bottom 眼睛下面的距离
 * @property int $mouth_top 嘴巴上面的距离
 * @property int $mouth_bottom 嘴巴下面的距离
 * @property int $nose_top 鼻子上面的距离
 * @property int $nose_bottom 鼻子下面的距离
 */
class TableCfgFaceReport extends \common\models\table\BaseTable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cfg_face_report';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['overview', 'eye_analysis', 'mouth_analysis', 'nose_analysis'], 'string'],
            [['eye_top', 'eye_bottom', 'mouth_top', 'mouth_bottom', 'nose_top', 'nose_bottom'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '报告ID',
            'overview' => '面相概述',
            'eye_analysis' => '眼睛解析',
            'mouth_analysis' => '嘴相解析',
            'nose_analysis' => '鼻相解析',
            'eye_top' => '眼睛上面的距离',
            'eye_bottom' => '眼睛下面的距离',
            'mouth_top' => '嘴巴上面的距离',
            'mouth_bottom' => '嘴巴下面的距离',
            'nose_top' => '鼻子上面的距离',
            'nose_bottom' => '鼻子下面的距离',
        ];
    }
}
