<?php

namespace common\models\table;

use Yii;

/**
 * This is the model class for table "cfg_info_config".
 *
 * @property string $cfg_code 配置代码
 * @property string $cfg_name 配置名
 * @property int $cfg_value 所需值
 * @property string $cfg_content 多文字使用（例如首页AI文案）
 */
class TableCfgInfoConfig extends \common\models\table\BaseTable
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cfg_info_config';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cfg_code', 'cfg_name'], 'required'],
            [['cfg_value'], 'integer'],
            [['cfg_content'], 'string'],
            [['cfg_code', 'cfg_name'], 'string', 'max' => 100],
            [['cfg_code'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cfg_code' => '配置代码',
            'cfg_name' => '配置名',
            'cfg_value' => '所需值',
            'cfg_content' => '多文字使用（例如首页AI文案）',
        ];
    }
}
