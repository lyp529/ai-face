<?php
namespace common\classes;

/**
 * 微信支付
 * @param $mchId string 商户号
 * @author codehui <admin@codehui.net> 2018-3-26
 */
class WeChatPay {
    public $mchId = "1515314401"; //商户号
    const WX_KEY = "ksdiwnownvoowogghror32348ju290kk"; //申请支付后有给予一个商户账号和密码，登陆后自己设置的key
    const UNIFIED_ORDER ='https://api.mch.weixin.qq.com/pay/unifiedorder'; // 统一下单

    /**
     * 统一下单
     * @param $openId
     * @param $orderNumber
     * @param $amount
     * @param $body
     * @param $notifyUrl
     * @return array
     */
    public function unifiedOrder($openId, $orderNumber, $amount, $body, $notifyUrl) {
        $weChatOauth = new WeChatOauth();
        $nonceStr = $this->nonce_str();//随机字符串
        $spBillCreateIp = $_SERVER['SERVER_ADDR'];//终端的ip
        $amount = $amount * 100;//总金额
        $tradeType = 'JSAPI'; //交易类型（默认）

        $post['appid'] = $weChatOauth->appId;
        $post['body'] = $body;
        $post['mch_id'] = $this->mchId;
        $post['nonce_str'] = $nonceStr;//随机字符串
        $post['notify_url'] = $notifyUrl;
        $post['openid'] = $openId;
        $post['out_trade_no'] = $orderNumber;
        $post['spbill_create_ip'] = $spBillCreateIp;
        $post['total_fee'] = (int)$amount;//总金额
        $post['trade_type'] = $tradeType; //交易类型（默认）
        $sign = $this->sign($post);//签名
        $post_xml = '<xml>
	<appid><![CDATA[' . $post['appid'] . ']]></appid>
	<body><![CDATA[' . $post['body'] . ']]></body>
	<mch_id><![CDATA[' . $post['mch_id'] . ']]></mch_id>
	<nonce_str><![CDATA[' . $post['nonce_str'] . ']]></nonce_str>
	<notify_url><![CDATA[' . $post['notify_url'] . ']]></notify_url>
	<openid><![CDATA[' . $post['openid'] . ']]></openid>
	<out_trade_no><![CDATA[' . $post['out_trade_no'] . ']]></out_trade_no>
	<spbill_create_ip><![CDATA[' . $post['spbill_create_ip'] . ']]></spbill_create_ip>
	<total_fee><![CDATA['. $post['total_fee'] .']]></total_fee>
	<trade_type><![CDATA[' . $post['trade_type'] . ']]></trade_type>
	<sign>'.$sign .'</sign>
</xml>';

        //统一接口prepay_id
        $xml = $this->http_request(self::UNIFIED_ORDER,$post_xml);
        $array = $this->xml($xml);

        if($array['return_code'] == 'SUCCESS' && $array['result_code'] == 'SUCCESS'){
            $time = time();
            $tmp['appId'] = $array['appid'];
            $tmp['nonceStr'] = $this->nonce_str();
            $tmp['package'] = 'prepay_id='.$array['prepay_id'];
            $tmp['signType'] = 'MD5';
            $tmp['timeStamp'] = "$time";

            $data['appId'] = $array['appid'];
            $data['nonceStr'] = $tmp['nonceStr'];//随机字符串
            $data['package'] = $tmp['package'];//统一下单接口返回的 prepay_id 参数值，提交格式如：prepay_id=*
            $data['signType'] = 'MD5';//签名算法，暂支持 MD5
            $data['timeStamp'] = (string)$time;//时间戳
            $data['paySign'] = $this->sign($tmp);//签名,具体签名方案参见微信公众号支付帮助文档;
        }elseif ($array['return_code'] == 'SUCCESS') {
            $data = ["code"=>400,"msg"=>self::unifiedOrderErrorCode($array['err_code'])];
        }else{
            $data = ["code"=>400,"msg"=>'支付失败'];
        }
        return ["code"=>200,"data"=>$data];
    }

    /**
     * 随机32位字符串
     * @return string
     */
    public function nonce_str(){
        $result = '';
        $str = 'QWERTYUIOPASDFGHJKLZXVBNMqwertyuioplkjhgfdsamnbvcxz';
        for ($i=0;$i<32;$i++){
            $result .= $str[rand(0,48)];
        }
        return $result;
    }

    /**
     * 生成订单号
     * @param $openid
     * @return string
     */
    private function order_number($openid){
        return md5($openid.time().rand(10,99));//32位
    }

    /**
     * 签名 $data要先排好顺序
     * @param $data
     * @return string
     */
    public function sign($data){
        $stringA = '';
        foreach ($data as $key=>$value){
            if(!$value) continue;
            if($stringA) $stringA .= '&'.$key."=".$value;
            else $stringA = $key."=".$value;
        }
        $wx_key = self::WX_KEY;//申请支付后有给予一个商户账号和密码，登陆后自己设置的key
        $stringSignTemp = $stringA.'&key='.$wx_key;
        return strtoupper(md5($stringSignTemp));
    }

    /**
     * curl请求
     * @param $url
     * @param array $data
     * @return bool|string
     */
    public function http_request($url,$data=array())
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

        // POST数据

        curl_setopt($ch, CURLOPT_POST, 1);

        // 把post的变量加上

        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        $output = curl_exec($ch);

        curl_close($ch);

        return $output;
    }

    /**
     * 获取xml
     * @param $xml
     * @return string
     */
    public function xml($xml){
        //将XML转为array
        //禁止引用外部xml实体
        libxml_disable_entity_loader(true);
        return json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
    }

    private static function unifiedOrderErrorCode($code='') {
        $msg = '支付失败';
        $data = ['INVALID_REQUEST' => '商户订单号重复', 'NOTENOUGH' => '余额不足', 'ORDERPAID' => '已支付', 'ORDERCLOSED' => '当前订单已关闭，请重新下单', 'SYSTEMERROR' => '系统超时'];
        $data[$code] && $msg = $data[$code];
        return $msg;
    }
}

