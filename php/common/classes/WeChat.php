<?php
namespace common\classes;

use common\models\db\FaceOrder;
use common\models\db\HospitalOrder;
use common\models\table\TableCfgInfoConfig;
use common\services\SM;
use Throwable;
use yii\db\StaleObjectException;

/**
 * 微信
 * @author codehui <admin@codehui.net> 2018-3-26
 */
class WeChat {
    const CHECK_ORDER ='https://api.mch.weixin.qq.com/pay/orderquery'; // 查询订单

    /**
     * 获取支付结果通知数据
     * @param $type
     * @return string
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function getNotifyData($type){
        //禁止引用外部xml实体
        libxml_disable_entity_loader(true);
        //获取返回的xml
        $xml = file_get_contents("php://input");

        //将xml转化为json格式
        $postObj = json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA));
        //转成数组
        $result = json_decode($postObj, true);
        $weChatPay = new WeChatPay();
        if ($result['return_code'] === 'SUCCESS' && $result['result_code'] === 'SUCCESS') {
            if ($this->Check_sign($result, $weChatPay::WX_KEY)) {
                $hospitalOrder = false;
                $type === 'hospital' && SM::getNotifyService()->notifyHospitalOrder($result['out_trade_no']);
                $type === 'face' && SM::getNotifyService()->notifyFaceOrder($result['out_trade_no']);
                if ($hospitalOrder) {
                    return '<xml>
                                  <return_code><![CDATA[SUCCESS]></return_code>
                                  <return_msg><![CDATA[OK]></return_msg>
                                </xml>';
                }
            }
        }
        return '<xml>
           <return_code>FAIL</return_code>
           <return_msg>支付通知失败</return_msg>
        </xml> ';
    }
    public function Check_sign($array, $key)
    {
        //验证签名是否正确
        $sign = $array['sign'];
        $check = '';
        foreach ($array as $k => $v) {
            if ($k != 'sign') {
                $check .= "&{$k}=$v";
            }
        }
        $check .= "&key={$key}";
        $check = trim($check, '&');
        $check = strtoupper(md5($check));
        if ($check != $sign) {
            return false;
        }
        return true;
    }
    /**
     * 获取签名
     * @param $params
     * @param $key
     * @return string
     */
    public static function getSign($params, $key)
    {
        ksort($params, SORT_STRING);
        $unSignParaString = self::formatQueryParaMap($params, false);
        return strtoupper(md5($unSignParaString . "&key=" . $key));
    }
    protected static function formatQueryParaMap($paraMap, $urlEncode = false)
    {
        $buff = "";
        ksort($paraMap);
        foreach ($paraMap as $k => $v) {
            if (null != $v && "null" != $v) {
                if ($urlEncode) {
                    $v = urlencode($v);
                }
                $buff .= $k . "=" . $v . "&";
            }
        }
        $reqPar = '';
        if (strlen($buff) > 0) {
            $reqPar = substr($buff, 0, strlen($buff) - 1);
        }
        return $reqPar;
    }

    public function checkOrder($orderNumber) {
        $weChatPay = new WeChatPay();
        $weChatOauth = new WeChatOauth();
        $nonceStr = $weChatPay->nonce_str();//随机字符串
        $post['appid'] = $weChatOauth->appId;
        $post['mch_id'] = $weChatPay->mchId;
        $post['nonce_str'] = $nonceStr;//随机字符串
        $post['out_trade_no'] = $orderNumber;
        $sign = $weChatPay->sign($post);//签名
        $post_xml = '<xml>
	<appid><![CDATA[' . $post['appid'] . ']]></appid>
	<mch_id><![CDATA[' . $post['mch_id'] . ']]></mch_id>
	<nonce_str><![CDATA[' . $post['nonce_str'] . ']]></nonce_str>
	<out_trade_no><![CDATA[' . $post['out_trade_no'] . ']]></out_trade_no>
	<sign>'.$sign .'</sign>
</xml>';
        $xml = $weChatPay->http_request(self::CHECK_ORDER,$post_xml);
        $array = $weChatPay->xml($xml);
        $data = [];
        if($array['return_code'] == 'SUCCESS' && $array['result_code'] == 'SUCCESS' && $array['trade_state'] == 'SUCCESS'){
            $data = ['out_trade_no'=>$array['out_trade_no'],'total_fee'=>$array['total_fee']/100,'trade_state'=>$array['trade_state']];
        }
        return $data;
    }
}
