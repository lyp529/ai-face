<?php
namespace common\classes;
/**
 * 微信授权登录获取用户信息
 * @param $appId string 微信应用appId
 * @param $appSecret string 微信应用appSecret
 * @author codehui <admin@codehui.net> 2018-3-26
 */
class WeChatOauth
{
    public $appId ="wxb2bbc1152fcfd16f";
    public $appSecret ="efaf393aba69fc61cdb7053994f271d4";
    public $error =['-1'=>'系统繁忙','40029'=>'code 无效','45011'=>'频率限制，每个用户每分钟100次'];// 错误信息
    const GET_CODE_SESSION_URL ='https://api.weixin.qq.com/sns/jscode2session';// 登录凭证校验
    const GET_USER_INFO_URL ='https://api.weixin.qq.com/sns/userinfo';// 获取用户基本信息
    const GET_ACCESS_TOKEN_URL ='https://api.weixin.qq.com/cgi-bin/token';// 获取小程序全局唯一后台接口调用凭据
    /**
     * 微信登录
     * @param string $code
     * @return array
     * @example
     */
    public function wxLogin($code){
        $codeSession = $this->getCodeSession($code);
        if(isset($codeSession['errcode'])){
            return ["code"=>400,"msg"=>$this->error[$codeSession['errcode']]];
        }
        return ["code"=>200,"openid"=>$codeSession['openid']];
    }

    /**
     * 获取code2Session
     * @param $code
     * @return mixed
     */
    private function getCodeSession($code){
        $getCodeSession = $this->combineURL(self::GET_CODE_SESSION_URL,[
            'appid'=> $this->appId,
            'secret'=> $this->appSecret,
            'js_code'=> $code,
            'grant_type'=>'authorization_code'
        ]);
        $codeSession = $this->httpsRequest($getCodeSession);
        return json_decode($codeSession,true);
    }

    /**
     * 获取access_token
     * @return mixed
     */
    private function getAccessToken(){
        $token = $this->combineURL(self::GET_ACCESS_TOKEN_URL,[
            'grant_type'=> 'client_credential',
            'appid'=> $this->appId,
            'secret'=> $this->appSecret
        ]);
        $accessToken = $this->httpsRequest($token);
        return json_decode($accessToken,true);
    }

    /**
     * 获取用户基本信息
     * @param $openid
     * @param $accessToken
     * @return mixed
     */
    private function getUserInfo($openid,$accessToken){
        $info = $this->combineURL(self::GET_USER_INFO_URL,[
            'access_token'=> $accessToken,
            'openid'=> $openid,
            'lang'=>'zh_CN'
        ]);
        $userInfo = $this->httpsRequest($info);
        return json_decode($userInfo,true);
    }

    /**
     * 拼接url
     * @param string $baseURL 请求的url
     * @param array $keysArr 参数列表数组
     * @return string 返回拼接的url
     */
    public function combineURL($baseURL, $keysArr){
        $combined = $baseURL ."?";
        $valueArr = array();
        foreach($keysArr as $key => $val){
            $valueArr[]="$key=$val";
        }
        $keyStr = implode("&", $valueArr);
        $combined .=($keyStr);
        return $combined;
    }
    /**
     * 获取服务器数据
     * @param string $url 请求的url
     * @return bool|string
     */
    public function httpsRequest($url){
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,1);
        $output = curl_exec($curl);
        curl_close($curl);
        return $output;
    }
}