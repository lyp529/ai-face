<?php


namespace common\constants;


/**
 * 日志常量
 * @remark 一经定义,不得修改,不然会引起数据错乱
 */
class L
{
	// ========================= 登录日志类型定义 log_login ========================
	const LOG_LOGIN = 30101; //登入
	const LOG_LOGOUT = 30102; //登出

	/**
	 * 登录日志类型
	 * @param null $type
	 * @return string|string[]
	 */
	public static function getLoginLog($type = null) {
		$labels = [
			self::LOG_LOGIN => "登入",
            self::LOG_LOGOUT => "登出",
		];
		return C::commonReturn($labels, $type);
	}

}

