<?php


namespace common\constants;


/**
 * 面相常量
 * @remark 一经定义,不得修改,不然会引起数据错乱
 */
class F
{
    /**
     * 脸型：甲字脸
     */
    const FEATURE_MA_TTS = 1;
    /**
     * 脸型：由字脸
     */
    const FEATURE_BY = 2;
    /**
     * 脸型：申字脸
     */
    const FEATURE_WORD = 3;
    /**
     * 脸型：水形圆脸
     */
    const FEATURE_WATER = 4;
    /**
     * 脸型：用字脸（脸型歪斜）
     */
    const FEATURE_SKEW = 5;


    /**
     * 额头：高
     */
    const FORE_HAD_HIGH = '早年事业比较顺，感情容易不顺；天庭饱满圆润，额头有肉，光滑明亮，一生近贵，前途广阔，智慧高，有学识。额如覆肝，长而丰隆，方而广阔。逻辑和思考能力优秀。';
    /**
     * 额头：普通
     */
    const FORE_HAD_COMMON = '30岁前容易受自身性格或父母状态的影响而心神不定，易迷茫，容易事倍功半，白白付出辛苦但是却收效甚微。贵人运强，不管是爱情还是事业，都属于越来越顺利的类型，晚年运相当不错。';


    /**
     * 眉毛：眉长
     */
    const BROW_LONG = 1;
    /**
     * 眉毛：眉短
     */
    const BROW_SHORT = 2;
    /**
     * 眉毛：普通眉毛
     */
    const BROW_COMMON = 3;
    /**
     * 眉毛：眉弯
     */
    const BROW_BEND = 4;
    /**
     * 眉毛：眉粗
     */
    const BROW_ROUGH = 5;


    /**
     * 眼睛：大
     */
    const EYE_BIG = 1;
    /**
     * 眼睛：小
     */
    const EYE_MINOR = 2;

    /**
     * 鼻子：直、有肉
     */
    const NOSE_STRAIGHT = 1;
    /**
     * 鼻子：大
     */
    const NOSE_BIG = 2;
    /**
     * 鼻子：小
     */
    const NOSE_MINOR = 3;
    /**
     * 鼻子：弯、歪斜
     */
    const NOSE_SKEW = 4;


    /**
     * 嘴巴：嘴大（男）
     */
    const MOUTH_BIG_MAN = 1;
    /**
     * 嘴巴：嘴大（女）
     */
    const MOUTH_BIG_GIRL = 2;
    /**
     * 嘴巴：嘴普通
     */
    const MOUTH_COMMON = 3;
    /**
     * 嘴巴：嘴小
     */
    const MOUTH_SMALL = 4;
    /**
     * 嘴巴：嘴型歪斜（男）
     */
    const MOUTH_SKEW_MAN = 5;
    /**
     * 嘴巴：嘴型歪斜（女）
     */
    const MOUTH_SKEW_GIRL = 6;



    /**
     * 获取脸型财运标签
     * @param null|int $category
     * @return array|string
     */
    public static function getFeatureLuck($category = null) {
        $labels = [
            F::FEATURE_MA_TTS => "早年财运好，晚年相对差；",
            F::FEATURE_BY => "早年财运不好，晚年好；",
            F::FEATURE_WORD => "早晚年财运不佳，中年相对好；",
            F::FEATURE_WATER => "肤色白，则名利双收,肤色黑暗，则财运不佳、贫困；",
            F::FEATURE_SKEW => "财运不佳，赚钱很辛苦、很累；",
        ];
        return C::commonReturn($labels, $category);
    }

    /**
     * 获取脸型婚姻标签（男）
     * @param null|int $category
     * @return array|string
     */
    public static function getFeatureMarriageMan($category = null) {
        $labels = [
            F::FEATURE_MA_TTS => "",
            F::FEATURE_BY => "早婚不利；",
            F::FEATURE_WORD => "坚强，能吃苦，但早婚不利；",
            F::FEATURE_WATER => "先天婚姻感情不理想；",
            F::FEATURE_SKEW => "感情不顺；",
        ];
        return C::commonReturn($labels, $category);
    }

    /**
     * 获取脸型婚姻标签（女）
     * @param null|int $category
     * @return array|string
     */
    public static function getFeatureMarriageGirl($category = null) {
        $labels = [
            F::FEATURE_MA_TTS => "女人甲字脸，额头大凸，先天婚姻感情不顺；",
            F::FEATURE_BY => "勤劳持家，是好女人，但早婚不利自己；",
            F::FEATURE_WORD => "比较能干，持家，但感情婚姻不美满；",
            F::FEATURE_WATER => "旺夫、旺家庭；",
            F::FEATURE_SKEW => "感情不顺；",
        ];
        return C::commonReturn($labels, $category);
    }

    /**
     * 获取脸型子女后代标签
     * @param null|int $category
     * @return array|string
     */
    public static function getFeatureOffspring($category = null) {
        $labels = [
            F::FEATURE_MA_TTS => "先天先生男；",
            F::FEATURE_BY => "",
            F::FEATURE_WORD => "",
            F::FEATURE_WATER => "",
            F::FEATURE_SKEW => "先生女；",
        ];
        return C::commonReturn($labels, $category);
    }

    /**
     * 获取脸型父母标签
     * @param null|int $category
     * @return array|string
     */
    public static function getFeatureParents($category = null) {
        $labels = [
            F::FEATURE_MA_TTS => "母亲身体比较差；",
            F::FEATURE_BY => "父亲身体比较差；",
            F::FEATURE_WORD => "父母身体都比较差；",
            F::FEATURE_WATER => "",
            F::FEATURE_SKEW => "",
        ];
        return C::commonReturn($labels, $category);
    }

    /**
     * 获取脸型学历标签
     * @param null|int $category
     * @return array|string
     */
    public static function getFeatureEducation($category = null) {
        $labels = [
            F::FEATURE_MA_TTS => "比较聪明，相对学习比较好，学历比较高；",
            F::FEATURE_BY => "很难有高学历；",
            F::FEATURE_WORD => "",
            F::FEATURE_WATER => "",
            F::FEATURE_SKEW => "很难有高学历；",
        ];
        return C::commonReturn($labels, $category);
    }

    /**
     * 获取眉毛标签
     * @param null|int $category
     * @return array|string
     */
    public static function getBrow($category = null) {
        $labels = [
            F::BROW_LONG => "浓而光彩有形,有贵人财运好。心胸豁达。行事贯彻始终，注意不要因情绪误事，或漠视他人的想法。",
            F::BROW_SHORT => "眉压田宅,会有家财外漏，或钱借出没回来。个性不稳定，容易感情用事，事业上易得到贵人相助。",
            F::BROW_COMMON => "眉断则，或散不聚,财散不聚。在逻辑描述上也会比一般人来的更好，而眼睛长也是聪明的贵相，人生中会有很多机遇。；",
            F::BROW_BEND => "内心善良，心肠特软，女生则是温润如水的温柔佳人，但容易没有主见，随波逐流，在事业上和需要决断的地方需要进步努力。",
            F::BROW_ROUGH => "您忠信重义，做事情谨守本分，在思考上有勇有谋，善于累积实力，在关键时刻抓住机会，一飞冲天。在爱情上也因为观察力强，找到的另一半往往也非常优秀。",
        ];
        return C::commonReturn($labels, $category);
    }

    /**
     * 获取眼睛标签
     * @param null|int $category
     * @return array|string
     */
    public static function getEye($category = null) {
        $labels = [
            F::EYE_BIG => "为人豁达、心地善良、个性敦厚、爽朗而乐观。适应环境的能力强，女生则是温润如水的温柔佳人，容易没有主见，在事业上和需要决断的地方需要进一步努力。",
            F::EYE_MINOR => "活动力强，但个性不稳定，容易受到感情用事的影响，在爱情上容易受伤，同时也是一个比较浪漫和有创意的人，比较难藏着自己的情绪。",
        ];
        return C::commonReturn($labels, $category);
    }

    /**
     * 获取鼻子标签
     * @param null|int $category
     * @return array|string
     */
    public static function getNose($category = null) {
        $labels = [
            F::NOSE_STRAIGHT => "个性好胜，心高气傲，倔强多疑、有心计，喜欢新奇事物，爱和他人竞争，不轻易妥协，自我防卫心强，颇为自傲。准头有肉，偏财运投机性的财运较多，通俗一些说就是运气好，时长有小财运撞到身上；",
            F::NOSE_BIG => "意志十分顽强，同时也充满了活力，喜欢追根究底的追问事物，喜欢交际，对于物质方面的追求欲极为强烈，工作能力强，斗志高，能打拼!您心胸宽大擅长察言观色，经常被人夸情商高。",
            F::NOSE_MINOR => "您心胸宽大擅长察言观色，情商高，同时您的鼻翼宽能够把赚进来的钱都给留住，何时该守财，对于赚钱机会的把握也更有能力。",
            F::NOSE_SKEW => "不利财运，泄财，比较辛苦；",
        ];
        return C::commonReturn($labels, $category);
    }

    /**
     * 获取嘴巴标签
     * @param null|int $category
     * @return array|string
     */
    public static function getMouth($category = null) {
        $labels = [
            F::MOUTH_BIG_MAN => "嘴大，精力充沛，好动和爱说话，而说起话来亦有声有色，吸引人听下去。嘴唇代表口福和交际，交际方面不会有很多认识的人，但有真心朋友。男人嘴大吃四方，有财，特别是晚年，容易生双胞胎；",
            F::MOUTH_BIG_GIRL => "嘴大，精力充沛，好动和爱说话，而说起话来亦有声有色，吸引人听下去。交际方面不会有很多认识的人，但有真心朋友。女人嘴大吃夫郎，容易生双胞胎；",
            F::MOUTH_COMMON => "您做事情特别踏实，属于一步一脚印类型，不太会说大话，对于事物细节的把控能力很强，不過也因為這样的特质，在做事情上特别辛苦，不太擅长于借力使力。",
            F::MOUTH_SMALL => "您的包容力比较强，天生的贵人就比较多，一生的福气也很棒，在恋爱和婚姻上也会得到别人的帮助，唯一注意的就是不要太懒，多运动更好。",
            F::MOUTH_SKEW_MAN => "命中幸苦，聚不了大财；",
            F::MOUTH_SKEW_GIRL => "也不利婚姻、感情；",
        ];
        return C::commonReturn($labels, $category);
    }
}

