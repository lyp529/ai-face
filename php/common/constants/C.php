<?php


namespace common\constants;

use common\utils\ArrayUtil;

/**
 * Class C 通用常量封装
 * @author 190727 by guorz
 *  IDEA 快速代码转 labels 正则表达之
 *  原代码：
 *      /**
 *       * 常量值：1标签
 *       *\/
 *      const CONSTANT_1 = 1;
 *  转换后：
 *      C::CONSTANT_1 => "1标签",
 *  使用方法：
 *      1.打开字符串替换工具（按 Ctrl + R 或 Ctrl + H）
 *      2.勾选工具【Regex】选框启动正则模式
 *      3.复制到搜索框：/\*\*\n\s+\*\s+.+：(.+)\n\s+\*\/\n\s+const ([A-Z_]+) = .+\n
 *      4.复制到替换框：C::$2 => "$1",\n
 *      5.将代码复制到 $labels = [] 的数组内
 *      6.点击工具【Replace】进行单个替换。（注意单个替换，不要全部替换）
 * @package common\constants
 */
class C
{
    /**
     * 响应代码：成功
     */
    const RESPONSE_CODE_DONE = 1;
    /**
     * 响应代码：失败
     */
    const RESPONSE_CODE_FAIL = -1;
    /**
     * 响应代码：程序异常
     */
    const RESPONSE_CODE_EXCEPTION = -2;
    /**
     * 响应代码：未登录
     */
    const RESPONSE_CODE_NOT_LOGIN = -3;

    /**
     * SESSION KEY：用户id
     */
    const SESSION_KEY_USER_ID = "USER_ID";

    /**
     * SESSION KEY：管理员id
     */
    const SESSION_KEY_ADMIN_ID = "ADMIN_ID";
    /**
     * SESSION KEY：用户名
     */
    const SESSION_KEY_USERNAME = "USERNAME";
    /**
     * SESSION KEY：用户经度
     */
    const SESSION_KEY_ADDRESS_X = "ADDRESS_X";
    /**
     * SESSION KEY：用户纬度
     */
    const SESSION_KEY_ADDRESS_Y = "ADDRESS_Y";

    /**
     * 数据库字段：true
     */
    const TRUE = 1;
    /**
     * 数据库字段：false
     */
    const FALSE = 0;

    /**
     * 成功标志
     */
    const SUCCESS = 'SUCCESS';

    /**
     * 默认会员头像
     */
    const DEFAULT_MEMBER_ICON = 'https://ai.judexinxi.cn/img/icon/default_icon.png';

    /**
     * 默认会员微信名
     */
    const DEFAULT_MEMBER_WE_CHAT_NAME = '未获取微信头像、名称';

    /**
     * 审核状态：审核中
     */
    const AUDIT_ING = 1;

    /**
     * 审核状态：审核成功
     */
    const AUDIT_SUCCESS = 2;

    /**
     * 审核状态：审核失败
     */
    const AUDIT_FAILURE = 3;

    /**
     * 医院预约：预约中
     */
    const APPOINTMENT_ING = 1;

    /**
     * 医院预约：预约成功
     */
    const APPOINTMENT_SUCCESS = 2;

    /**
     * 医院预约：预约失败
     */
    const APPOINTMENT_LOSE = 3;

    /**
     * 权限角色：用户
     */
    const AUTH_ROLE_TYPE_USER = 1;

    /**
     * 医院账户：是
     */
    const HOSPITAL_YES = 1;

    /**
     * 医院账户：不是
     */
    const HOSPITAL_NO = 2;

    /**
     * 代理商账户：是
     */
    const AGENT_YES = 1;

    /**
     * 代理商账户：不是
     */
    const AGENT_NO = 2;

    /**
     * @var int KB字节数
     */
    const KB = 1024;
    /**
     * @var int MB字节数
     */
    const MB = 1048576;

    const HOSPITAL_CATEGORY = ['眼部塑美','鼻部塑美','胸部塑美','瘦身整形','面部整形','紧肤除皱','美肤祛斑','祛痘美肤','光滑脱毛','美白嫩肤','疤痕修复','活力齿科'];

    /**
     * 基础信息
     */
    const infoConfig = ['HOME_AI_INTRODUCE', 'HOSPITAL_SUBSCRIPTION', 'PICTURE_AI_AMOUNT', 'PRIMARY_DISTRIBUTION', 'SECONDARY_DISTRIBUTION'];

	/**
     * 通用返回
     * @param string[] $labels
     * @param null|int|string $key
     * @param string $defaultValue
     * @return string[]|string
     */
    public static function commonReturn(array $labels, $key = null, $defaultValue = "") {
        if ($key !== null) {
            return ArrayUtil::issetDefault($labels, $key, $defaultValue);
        }
        return $labels;
    }

    /**
     * 获取响应代码的标签
     * @param null|int $responseCode
     * @return array|string
     */
    public static function getReturnCodeLabel($responseCode = null) {
        $labels = [
            C::RESPONSE_CODE_DONE => "成功",
            C::RESPONSE_CODE_FAIL => "失败",
            C::RESPONSE_CODE_EXCEPTION => "程序异常",
            C::RESPONSE_CODE_NOT_LOGIN => "未登录",
        ];
        return C::commonReturn($labels, $responseCode);
    }

    /**
     * 是否医院账户的标签
     * @param null|int $isHospital
     * @return array|string
     */
    public static function isHospitalLabel($isHospital = null) {
        $labels = [
            C::HOSPITAL_YES => "是",
            C::HOSPITAL_NO => "否",
        ];
        return C::commonReturn($labels, $isHospital);
    }

    /**
     * 是否代理商账户的标签
     * @param null|int $isAgent
     * @return array|string
     */
    public static function isAgentLabel($isAgent = null) {
        $labels = [
            C::AGENT_YES => "是",
            C::AGENT_NO => "否",
        ];
        return C::commonReturn($labels, $isAgent);
    }
}
