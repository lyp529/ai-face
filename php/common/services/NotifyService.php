<?php
namespace common\services;

use common\classes\WeChat;
use common\constants\C;
use common\constants\F;
use common\models\db\Brokerage;
use common\models\db\FaceOrder;
use common\models\db\FaceReport;
use common\models\db\HospitalOrder;
use common\models\db\InfoConfig;
use common\models\db\UserMember;
use common\models\table\TableCfgMemberRelation;
use common\traits\InstanceTrait;
use common\utils\FaceAnalyzeUtil;
use Throwable;
use yii\db\StaleObjectException;

/**
 * Class NotifyService 通知服务
 * @package common\services
 */
class NotifyService extends BaseService
{
	use InstanceTrait;

    /**
     * 预约医院微信支付异步通知
     * @return string
     * @throws StaleObjectException
     * @throws Throwable
     */
	public function notifyHospital() {
        return (new WeChat())->getNotifyData('hospital');
    }

    /**
     * 医院订单异步通知
     * @param  $outTradeNo
     * @return bool
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function notifyHospitalOrder($outTradeNo) {
        $hospital = HospitalOrder::findOne(['order_number'=>$outTradeNo]);
        if (!$hospital || ($hospital->state != C::APPOINTMENT_ING)) return true;
        $checkOrder = [];
        $hospital->state == C::AUDIT_ING && $checkOrder = (new \common\classes\WeChat)->checkOrder($hospital->order_number);
        if (count($checkOrder) > 0) {
            if ($checkOrder['out_trade_no'] == $hospital->order_number && $checkOrder['total_fee'] == $hospital->amount) {
                $state = $checkOrder['trade_state'] == 'SUCCESS' ? C::APPOINTMENT_SUCCESS : C::APPOINTMENT_LOSE;
                $hospital->state = $state;
                $hospital->update();
            }
        }
        return true;
    }

    /**
     * 面向报告微信支付异步通知
     * @return string
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function notifyFace() {
        return (new WeChat())->getNotifyData('face');
    }

    /**
     * 面相报告微信支付异步通知
     * @param $outTradeNo
     * @return bool
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function notifyFaceOrder($outTradeNo) {
        $face = FaceOrder::findOne(['order_number'=>$outTradeNo]);
        if (!$face || ($face->state != C::APPOINTMENT_ING)) return true;
        $checkOrder = [];
        $face->state == C::AUDIT_ING && $checkOrder = (new \common\classes\WeChat)->checkOrder($face->order_number);
        if (count($checkOrder) > 0) {
            if ($checkOrder['out_trade_no'] == $face->order_number && $checkOrder['total_fee'] == $face->amount) {
                $state = $checkOrder['trade_state'] == 'SUCCESS' ? C::APPOINTMENT_SUCCESS : C::APPOINTMENT_LOSE;
                $face->state = $state;
                if ($face->update()) self::brokerage($face->member_id, $face->amount);
        //        $analyze = (new FaceAnalyzeUtil())->faceAnalyze($face->url);
        //        self::faceReport($analyze,$face->gender,$face->re_id);
            }
        }
        return true;
    }
//
//    /**
//     * 更新面相报告
//     * @param $analyze
//     * @param $gender
//     * @param $reId
//     * @throws StaleObjectException
//     * @throws Throwable
//     */
//    private static function faceReport($analyze,$gender,$reId) {
//        //脸宽度
//        $faceWidth = $analyze['FaceShapeSet'][0]['FaceProfile'][20]['X'] - $analyze['FaceShapeSet'][0]['FaceProfile'][0]['X'];
//        $overview = self::faceProfile($analyze['FaceShapeSet'][0]['FaceProfile'],$gender);         //面相概述
//        $eyeAnalysis = self::eyeAnalysis($analyze['FaceShapeSet'][0]['LeftEye'],$analyze['FaceShapeSet'][0]['RightEye'],$faceWidth);      //眼睛解析
//        $mouthAnalysis = self::mouthAnalysis($analyze['FaceShapeSet'][0]['Mouth'],$faceWidth,$gender);    //嘴相解析
//        $noseAnalysis = self::noseAnalysis($analyze['FaceShapeSet'][0]['Nose'],$faceWidth);     //鼻相解析
//
//        $faceReport = FaceReport::findOne($reId);
//        $faceReport->overview = $overview;
//        $faceReport->eye_analysis = $eyeAnalysis;
//        $faceReport->mouth_analysis = $mouthAnalysis;
//        $faceReport->nose_analysis = $noseAnalysis;
//        $faceReport->update();
//    }
//
//    /**
//     * 鼻相解析
//     * @param $nose
//     * @param $faceWidth
//     * @return array|string
//     */
//    private static function noseAnalysis($nose,$faceWidth) {
//        $noseWidth = max(array_column($nose,'X')) - min(array_column($nose,'X'));
//        if ($noseWidth * 2.5 <= $faceWidth && $noseWidth * 4 >= $faceWidth) {
//            return F::getNose(F::NOSE_STRAIGHT);
//        }
//        return '';
//    }
//    /**
//     * 嘴相解析
//     * @param $mouth
//     * @param $faceWidth
//     * @param $gender
//     */
//    private static function mouthAnalysis($mouth,$faceWidth,$gender) {
//        $str = '';
//        $mouthWidth = max(array_column($mouth,'X')) - min(array_column($mouth,'X'));
//        if ($mouthWidth/2 * 3 >= $faceWidth) {
//            $gender == 1 && $str .= F::getMouth(F::MOUTH_BIG_GIRL);
//            $gender == 2 && $str .= F::getMouth(F::MOUTH_BIG_MAN);
//        }
//        foreach ($mouth as $key=>$value) {
//            if ($key == count($mouth)-1) {
//                break;
//            }
//            if (abs($value['X']-$mouth[$key+1]['X'])>20) {
//                $str .= $gender == 1 ? F::getMouth(F::MOUTH_SKEW_MAN) : F::getMouth(F::MOUTH_SKEW_GIRL);
//            }
//        }
//    }
//    /**
//     * 面相概述
//     * @param $faceProfile
//     * @param $gender
//     * @return string
//     */
//    private static function faceProfile($faceProfile,$gender) {
//        $str = '';
//        $feature = F::FEATURE_SKEW;
//        $f0 = $faceProfile[0];
//        $f4 = $faceProfile[4];
//        $f5 = $faceProfile[5];
//        $f6 = $faceProfile[6];
//        $f7 = $faceProfile[7];
//        $f8 = $faceProfile[8];
//        $f9 = $faceProfile[9];
//        $f10 = $faceProfile[10];
//        $f11 = $faceProfile[11];
//        $f12 = $faceProfile[12];
//        $f13 = $faceProfile[13];
//        $f14 = $faceProfile[14];
//        $f15 = $faceProfile[15];
//        $f16 = $faceProfile[16];
//        $f20 = $faceProfile[20];
//        if(($f16['X'] - $f15['X']) < 20 && ($f15['X'] - $f14['X']) < 20 && ($f14['X'] - $f13['X']) < 20 && ($f13['X'] - $f12['X']) < 20 && ($f12['X'] - $f11['X']) < 20 && ($f11['X'] - $f10['X']) < 20 && ($f10['X'] - $f9['X']) < 20 && ($f9['X'] - $f8['X']) < 20 && ($f8['X'] - $f7['X']) < 20 && ($f7['X'] - $f6['X']) < 20 && ($f6['X'] - $f5['X']) < 20 && ($f5['X'] - $f4['X']) < 20) {
//            if(($f11['Y'] - $f12['Y']) < 20 && ($f12['Y'] - $f13['Y']) < 20 && ($f13['Y'] - $f12['Y']) < 20 && ($f12['Y'] - $f13['Y']) < 20 && ($f13['Y'] - $f14['Y']) < 20 && ($f14['Y'] - $f15['Y']) < 20 && ($f15['Y'] - $f16['Y']) < 20 && ($f10['Y'] - $f9['Y']) < 20 && ($f9['Y'] - $f8['Y']) < 20 && ($f8['Y'] - $f7['Y']) < 20 && ($f7['Y'] - $f6['Y']) < 20 && ($f6['Y'] - $f5['Y']) < 20) {
//                if (($f12['X'] - $f11['X']) < 5 && ($f11['X'] - $f10['X']) < 5 && ($f10['X'] - $f9['X']) < 5) {
//                    $feature = F::FEATURE_MA_TTS;
//                }else {
//                    $feature = F::FEATURE_WATER;
//                }
//            }
//        }
//        ($f20['X'] - $f0['X'])/2 >= ($f10['Y'] - (($f0['Y'] + $f20['Y'])/2)) && $feature = F::FEATURE_BY;
//        ($f20['X'] - $f0['X']) <= ($f10['Y'] - (($f0['Y'] + $f20['Y'])/2)) && $feature = F::FEATURE_WORD;
//
//
//        $str .= F::getFeatureLuck($feature);
//        $str .= F::getFeatureOffspring($feature);
//        $str .= F::getFeatureParents($feature);
//        $str .= F::getFeatureEducation($feature);
//        $gender == 1 && $str .= F::getFeatureMarriageGirl($feature);
//        $gender == 2 && $str .= F::getFeatureMarriageMan($feature);
//        return $str;
//    }
//
//    /**
//     * 眼睛解析
//     * @param $leftEye
//     * @param $rightEye
//     * @param $faceProfileX
//     * @return array|string
//     */
//    private static function eyeAnalysis($leftEye,$rightEye,$faceProfileX) {
//        $eye = F::EYE_MINOR;
//        if (((($leftEye[3]['X'] + $leftEye[4]['X'] + $leftEye[5]['X'])/3 - $leftEye[0]['X']) + $rightEye[0]['X'] - $rightEye[4]['X']) * 4 >= $faceProfileX) {
//            $eye = F::EYE_BIG;
//        }
//        return F::getEye($eye);
//    }
    /**
     * 分配佣金
     * @param $memberId
     * @param $amount
     * @return bool
     * @throws StaleObjectException
     * @throws Throwable
     */
    private static function brokerage($memberId, $amount) {
        $Relation = TableCfgMemberRelation::findOne(['member_id'=>$memberId]);
        if (!$Relation) return true;
        //一级分销
        $superiorId = $Relation->superior_id;
        $config = InfoConfig::findOne(C::infoConfig[3]);
        $superiorUser = UserMember::findOne($superiorId);
        if (!$superiorUser) return true;
        $superiorBro = (new Brokerage())->add($superiorId, $config->cfg_value*0.01*$amount, 1, 2, $superiorUser->icon, $superiorUser->username);
        if ($superiorBro) {
            $superiorUser->brokerage += $config->cfg_value*0.01*$amount;
            $superiorUser->update();
        }
        //二级分销
        $firstSuperiorId = self::getFirstSuperior($superiorUser->referrer_id);
        if (!$firstSuperiorId) return true;
        if ($firstSuperiorId) {
            $firstSuperiorUser = UserMember::findOne($firstSuperiorId);
            $firstConfig = InfoConfig::findOne(C::infoConfig[4]);
            $firstSuperiorBro = (new Brokerage())->add($firstSuperiorUser->id, $firstConfig->cfg_value*0.01*$amount, 1, 2, $firstSuperiorUser->icon, $firstSuperiorUser->username);
            if ($firstSuperiorBro) {
                $firstSuperiorUser->brokerage += $firstConfig->cfg_value*0.01*$amount;
                $firstSuperiorUser->update();
            }
        }
        return true;
    }

    /**
     * 获取是代理的上级
     * @param $superiorId
     * @return mixed
     */
    private static function getFirstSuperior($superiorId) {
        $Relation = UserMember::findOne($superiorId);
        if (!$Relation) return '';
        if ($Relation->is_agent == 1) return $Relation->id;
        if ($Relation->referrer_id == 0) return '';
        return self::getFirstSuperior($Relation->referrer_id);
    }
}
