<?php
namespace common\services;

use common\classes\WeChatOauth;
use common\constants\C;
use common\exceptions\ForeseeableException;
use common\exceptions\SystemException;
use common\models\db\Agent;
use common\models\db\Brokerage;
use common\models\db\FaceOrder;
use common\models\db\Hospital;
use common\models\db\HospitalCase;
use common\models\db\HospitalCategory;
use common\models\db\HospitalOrder;
use common\models\db\HospitalRequest;
use common\models\db\InfoConfig;
use common\models\db\MemberRelation;
use common\models\db\UserAdmin;
use common\models\db\UserMember;
use common\models\table\TableCfgHospitalRequest;
use common\models\table\TableCfgInfoConfig;
use common\models\table\TableTabAgent;
use common\models\table\TableTabHospital;
use common\traits\InstanceTrait;
use common\utils\YiiUtil;
use frontend\models\form\HospitalForm;
use frontend\models\form\HospitalRequestForm;
use Throwable;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Exception;
use yii\db\StaleObjectException;

/**
 * Class UserService 用户服务
 * @package common\services
 */
class UserService extends BaseService
{
	use InstanceTrait;

    /**
     * 管理员系统登录
     * @param $username
     * @param $password
     * @return UserAdmin|null
     * @throws ForeseeableException
     * @throws SystemException
     */
	public function adminLogin($username, $password) {
	    $admin = UserAdmin::findOne(['username'=>$username]);
	    if (!$admin || $admin->password != $password) {
            throw new ForeseeableException("密码不正确");
        }
	    $admin->last_login_ip = $admin->login_ip;
	    $admin->last_login_time = $admin->login_time;
	    $admin->login_ip = (string)YiiUtil::getUserIP();
	    $admin->login_time = time();
	    $admin->session_dead_time = time() + 86400;
        if (!$admin->save()) {
            throw new SystemException($admin->getFirstError());
        }
        YiiUtil::session(C::SESSION_KEY_ADMIN_ID, $admin->id);
        return $admin;
    }

    /**
     * 判断当前是否登录中
     * @return bool
     */
    public function isAdminLogin() {
        $adminId = $this->getAdminId();
        if (!$adminId) return false;
        $admin = UserAdmin::findOne($adminId);
        if (!$admin || $admin->session_dead_time < time()) return false;
        return true;
    }
    /**
     * 获取管理员id
     * @return mixed
     */
	public function getAdminId() {
	    return YiiUtil::session(C::SESSION_KEY_ADMIN_ID);
    }

    /**
     * 会员管理获取会员列表
     * @param $offset
     * @param $limit
     * @param $username
     * @return array
     */
    public function getUserList($offset, $limit,$username) {
        $query = UserMember::find();
        $username && $query->andWhere(["username" => $username]);
        $count = $query->count();
        $list = $query->orderBy("id DESC")->asArray()->offset($offset)->limit($limit)->all();
        foreach ($list as $k=>$v) {
            $list[$k]['is_hospital'] = C::isHospitalLabel($v['is_hospital']);
            $list[$k]['is_agent'] = C::isAgentLabel($v['is_agent']);
            $list[$k]['creation_time'] = date("Y-m-d H:i:s",$v['creation_time']);
            $superior = UserMember::findOne($v['referrer_id']);
            $list[$k]['superior'] = $superior ? $superior->username : '';
            $subordinate = UserMember::find()->Where(['referrer_id'=>$v['id']])->select('username')->asArray()->all();
            $list[$k]['subordinate'] = $subordinate ? implode(",", array_column($subordinate,'username')) : '';
        }
        return ["rows" => $list,"count" => $count];
    }
    /**
     * 获取管理员列表
     * @param $offset
     * @param $limit
     * @return array
     */
    public function getAdminList($offset, $limit) {
        $query = UserAdmin::find();
        $count=$query->count();
        $list = $query->orderBy("id DESC")->offset($offset)->limit($limit)->all();
        foreach ($list as $k=>$v) {
            $list[$k]->login_time = date("Y-m-d H:i:s",$v->login_time);
            $list[$k]->last_login_time = date("Y-m-d H:i:s",$v->last_login_time);
        }
        return ["rows" => $list,"count" => $count];
    }

    /**
     * 获取医院列表（管理员系统）
     * @param $offset
     * @param $limit
     * @param $name
     * @return array
     */
    public function adminGetHospitalList($offset, $limit,$name) {
        $query = Hospital::find();
        $name && $query->andWhere(["name" => $name]);
        $count = $query->count();
        $list = $query->orderBy("id DESC")->offset($offset)->limit($limit)->all();
        foreach ($list as $k=>$v) {
            $list[$k]->member_id = $this->getMember($v->member_id)->username;
            $list[$k]->category = $v->category;
            $list[$k]->creation_time = date("Y-m-d H:i:s",$v->creation_time);
        }
        return ["rows" => $list,"count" => $count];
    }

    /**
     * 删除医院
     * @param $id
     * @throws ForeseeableException
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function delHospital($id) {
        $hospital = Hospital::findOne($id);
        if (!$hospital) {
            throw new ForeseeableException("医院不存在！");
        }
        $request = HospitalRequest::findOne(['member_id'=>$hospital->member_id, 'state'=>C::AUDIT_SUCCESS]);
        if (!$request) {
            throw new ForeseeableException("医院不存在！");
        }
        $case = HospitalCase::find()->where(['h_id'=>$id])->all();
        if (!$case) {
            throw new ForeseeableException("医院不存在！");
        }
        $member = UserMember::findOne($hospital->member_id);
        if (!$member) {
            throw new ForeseeableException("用户错误，删除失败！");
        }
        $member->is_hospital = C::HOSPITAL_NO;
        if (!$member->update()) {
            throw new ForeseeableException($member->getFirstError());
        }
        @unlink($hospital->icon);
        @unlink($hospital->head_img);
        foreach ($case as $key=>$value) {
            @unlink($value->img_url);
        }
        if (!$hospital->delete()) {
            throw new ForeseeableException($hospital->getFirstError());
        }
        if (!$request->delete()) {
            throw new ForeseeableException($request->getFirstError());
        }
        if (!HospitalCase::deleteAll(["h_id" => $id])) {
            throw new StaleObjectException('删除失败！');
        }
    }

    /**
     * 获取医院申请列表
     * @param $offset
     * @param $limit
     * @param $state
     * @return array
     */
    public function getHospitalRequestList($offset, $limit, $state) {
        $query = HospitalRequest::find();
        $state && $query->andWhere(["state" => $state]);
        $count = $query->count();
        $list = $query->orderBy("id DESC")->offset($offset)->limit($limit)->all();
        foreach ($list as $k=>$v) {
            $list[$k]->member_id = $this->getMember($v->member_id)->username;
            $list[$k]->creation_time = date("Y-m-d H:i:s",$v->creation_time);
            $list[$k]->update_time = date("Y-m-d H:i:s",$v->update_time);
        }
        return ["rows" => $list,"count" => $count];
    }

    /**
     * 更新医院申请
     * @param $id
     * @param $state
     * @throws ForeseeableException
     * @throws StaleObjectException
     * @throws SystemException
     * @throws Throwable
     */
    public function updateHospitalRequest($id, $state) {
        $hospital = HospitalRequest::findOne($id);
        if (!$hospital) {
            throw new ForeseeableException("医院不存在！");
        }
        if ($hospital->state != C::AUDIT_ING) {
            throw new ForeseeableException("该申请已处理过！");
        }
        $hospital->state = (int)$state;
        if (!$hospital->update()) {
            throw new SystemException($hospital->getFirstError());
        }
    }

    /**
     * 获取医院预约订单
     * @param $offset
     * @param $limit
     * @param $state
     * @return array
     */
    public function getHospitalOrderList($offset, $limit, $state) {
        $query = HospitalOrder::find();
        $state && $query->Where(["state" => $state]);
        $count = $query->count();
        $list = $query->orderBy("id DESC")->offset($offset)->limit($limit)->all();
        foreach ($list as $k=>$v) {
            $hospital = $this->getHospital($v->hospital_id);
            $list[$k]->hospital_id = $hospital ? $hospital->name : '该医院已删除';
            $list[$k]->member_id = $this->getMember($v->member_id)->username;
            $list[$k]->services = $v->services;
            $list[$k]->appointment_time = date("Y-m-d H:i:s",$v->appointment_time);
            $list[$k]->creation_time = date("Y-m-d H:i:s",$v->creation_time);
            $list[$k]->update_time = date("Y-m-d H:i:s",$v->update_time);
        }
        return ["rows" => $list,"count" => $count];
    }
    /**
     * 更新医院预约订单
     * @param $id
     * @param $state
     * @throws ForeseeableException
     * @throws StaleObjectException
     * @throws SystemException
     * @throws Throwable
     */
    public function updateHospitalOrder($id, $state) {
        $order = HospitalOrder::findOne($id);
        if (!$order) {
            throw new ForeseeableException("该预约单不存在，或该医院已删除！");
        }
        if ($order->state != C::AUDIT_ING) {
            throw new ForeseeableException("该预约单已处理过！");
        }
        $order->state = (int)$state;
        if (!$order->update()) {
            throw new SystemException($order->getFirstError());
        }
    }

    /**
     * 获取代理商
     * @param $offset
     * @param $limit
     * @param $state
     * @param $username
     * @return array
     */
    public function getAgentList($offset, $limit, $state, $username) {
        $query = Agent::find();
        $state && $query->andWhere(["state" => $state]);
        $username && $query->andWhere(["username" => $username]);
        $count = $query->count();
        $list = $query->orderBy("id DESC")->offset($offset)->limit($limit)->all();
        foreach ($list as $k=>$v) {
            $list[$k]->member_id = $this->getMember($v->member_id)->username;
            $list[$k]->creation_time = date("Y-m-d H:i:s",$v->creation_time);
            $list[$k]->update_time = date("Y-m-d H:i:s",$v->update_time);
        }
        return ["rows" => $list,"count" => $count];
    }

    /**
     * 更新代理商
     * @param $id
     * @param $state
     * @throws ForeseeableException
     * @throws StaleObjectException
     * @throws SystemException
     * @throws Throwable
     */
    public function updateAgent($id, $state) {
        $agent = Agent::findOne($id);
        if (!$agent) {
            throw new ForeseeableException("该代理商申请不存在！");
        }
        if ($agent->state != C::AUDIT_ING) {
            throw new ForeseeableException("该代理商申请已处理过！");
        }
        $agent->state = (int)$state;
        if (!$agent->update()) {
            throw new SystemException($agent->getFirstError());
        }
        if ($state == C::AUDIT_SUCCESS) {
            $member = UserMember::findOne($agent->member_id);
            $member->is_agent = C::AGENT_YES;
            if (!$member->update()) {
                throw new SystemException($member->getFirstError());
            }
        }
    }

    /**
     * 更新配置
     * @param $cfgCode
     * @param $cfgValue
     * @param $cfgContent
     * @throws ForeseeableException
     * @throws StaleObjectException
     * @throws SystemException
     * @throws Throwable
     */
    public function updateInfoConfig($cfgCode, $cfgValue, $cfgContent) {
        $config = TableCfgInfoConfig::findOne(['cfg_code'=> $cfgCode]);
        if (!$config) {
            throw new ForeseeableException("该代理商申请不存在！");
        }
        $config->cfg_value = $cfgValue;
        $config->cfg_content = $cfgContent;
        if (!$config->update()) {
            throw new SystemException($config->getFirstError());
        }
    }




    /**
     * 获取医院
     * @param int $id
     * @return Hospital|null
     */
    public function getHospital($id = 0) {
        $where = $id;
        $id == 0 && $where = ['member_id'=>self::getMemberId()];
        return Hospital::findOne($where);
    }
    /**
     * 获取会员
     * @param $memberId
     * @return UserMember|null
     */
    public function getMember($memberId = 0) {
        $memberId === 0 &&  $memberId = $this->getMemberId();
        return UserMember::findOne($memberId);
    }
    /**
     * 获取用户id
     * @return mixed
     */
	public function getMemberId() {
	    return YiiUtil::session(C::SESSION_KEY_USER_ID);
    }
    /**
     * 获取用户经度
     * @return mixed
     */
	private static function getAddressX() {
	    return YiiUtil::session(C::SESSION_KEY_ADDRESS_X);
    }
    /**
     * 获取用户纬度
     * @return mixed
     */
	private static function getAddressY() {
	    return YiiUtil::session(C::SESSION_KEY_ADDRESS_Y);
    }

    /**
     * 获取首页信息
     * @return array
     */
    public function getHomeInfo() {
        //首页AI简单介绍
        $config = InfoConfig::findOne(C::infoConfig[0]);
        //面相报告
        $faceOrder = FaceOrder::find()->select(['member_id as username'])->asArray()->orderBy("id DESC")->all();
        //最新的十名获取报告用户
        $userReport = array_slice($faceOrder, 0,10);
        foreach ($userReport as $k=>$v) {
            $user = UserMember::findOne($v['username']);
            $userReport[$k]['username'] = $user->username;
            $userReport[$k]['url'] = $user->icon;
        }
        $data = ['ai_introduce'=>$config->cfg_content,'report_count'=>count($faceOrder),'user_report'=>$userReport];
        return ["code"=>200,"msg"=>"获取成功！","data"=>$data];
    }
    /**
     * 注册
     * @param $code
     * @param int $referrerId 推荐人ID
     * @return array
     */
	public function register($code,$referrerId = 0) {
	    $weChatOauth = new WeChatOauth();
        $data = $weChatOauth->wxLogin($code);
        if ($data['code'] === 200) {
            $member = UserMember::findOne(['openid' => $data['openid']]);
            if (!$member) {
                $this->addUserMember($data['openid'],$referrerId);
            } else {
                YiiUtil::session(C::SESSION_KEY_USER_ID, $member->id);
                return ["code"=>200,"msg"=>"成功登录！","data"=>['user_id'=>$member->id,'token'=>$member->token]];
            }

        }else {
            return $data;
        }
    }

    /**
     * 新增用户
     * @param $openid   string   授权用户唯一标识
     * @param $referrerId   int   推荐人
     * @return array
     */
	public function addUserMember($openid,$referrerId) {
	    $data = ["code"=>400,"msg"=>"创建用户失败！"];
	    $member = new UserMember();
        $member->referrer_id = (int)$referrerId;
	    $member->token = self::getAccessToken();
	    $member->openid = $openid;
	    $member->is_hospital = 2;
	    $member->is_agent = 2;
	    $member->creation_time = time();
	    $member->token_time = 30 * 86400 + time();
	    if ($member->save()) {
            $relation = new MemberRelation();
            $relation->superior_id = $referrerId;
            $relation->member_id = $member->id;
            if ($relation->save()) {
                YiiUtil::session(C::SESSION_KEY_USER_ID, $member->id);
                $data = ["code"=>200,"msg"=>"成功登录！","data"=>['user_id'=>$member->id,'token'=>$member->token]];
            }
        }
	    return $data;
    }

    /**
     * 生成token
     * @return string
     */
    private static function getAccessToken() {
	    return strtoupper(md5(substr(str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890'), 0, 6).rand(10000, 99999).time()));
    }

    /**
     * 医院入驻申请表
     * @param HospitalRequestForm $form
     * @return array
     */
    public function requestHospital(HospitalRequestForm $form) {
        $memberId = $this->getMemberId();
        $hospital = TableCfgHospitalRequest::findOne(['member_id'=>$memberId]);
        if ($hospital && $hospital->state == 1) {
            return ["code"=>200,"msg"=>"您已申请，等待审核！"];
        }
        if ($hospital && $hospital->state == 2) {
            return ["code"=>200,"msg"=>"您已申请成功！"];
        }
        $request = new TableCfgHospitalRequest();
        $request->member_id = $memberId;
        $request->logo_url = $form->logo_url;
        $request->name = $form->name;
        $request->address = $form->address;
        $request->contact_person = $form->contact_person;
        $request->contact_way = $form->contact_way;
        $request->business_license = $form->business_license;
        $request->id_card_f = $form->id_card_f;
        $request->id_card_r = $form->id_card_r;
        $request->state = C::AUDIT_ING;
        $request->creation_time = time();
        if ($request->save()) {
            return ["code"=>200,"msg"=>"申请成功，等待审核！"];
        }
        return ["code"=>400,"msg"=>"提交申请失败！"];
    }

    /**
     * 新增医院
     * @param HospitalForm $form
     * @return array
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function addHospital(HospitalForm $form) {
        $data = ["code"=>400,"msg"=>"提交失败！"];
        $memberId = $this->getMemberId();
        $hospitalName = HospitalRequest::findOne(['member_id'=>$memberId]);
        if (!$hospitalName) {
            return $data;
        }
        $hospital = new Hospital();
        $hospital->member_id = $memberId;
        $hospital->category = $form->category;
        $hospital->name = $hospitalName->name;
        $hospital->icon = $form->icon;
        $hospital->head_img = $form->head_img;
        $hospital->contact_way = $form->contact_way;
        $hospital->address = $form->address;
        $hospital->brief = $form->brief;
        $hospital->address_x = $form->address_x;
        $hospital->address_y = $form->address_y;
        $hospital->creation_time = time();
        if ($hospital->save()) {
            if (self::addHospitalCase($hospital->id, $form->img_url)) {
                $userMember = UserMember::findOne($memberId);
                $userMember->is_hospital = C::HOSPITAL_YES;
                $userMember->update();
                return ["code"=>200,"msg"=>"提交成功！"];
            }
        }
        return $data;
    }

    /**
     * 新增医院成功案例
     * @param $hId
     * @param $imgUrl
     * @return bool
     * @throws Exception
     */
    public static function addHospitalCase($hId, $imgUrl) {
        $imgUrl = explode(',',$imgUrl);
        $rows = [];
        foreach ($imgUrl as $k=>$v) {
            $rows[] = [$hId, $k, $v];
        }
        return Yii::$app->db->createCommand()->batchInsert('cfg_hospital_case', ['h_id', 'sort', 'img_url'], $rows)->execute();
    }

    /**
     * 获取医院列表
     * @param string $addressX
     * @param string $addressY
     * @param string $name
     * @param $offset
     * @param $limit
     * @return array
     */
    public function getHospitalList($addressX, $addressY, $offset, $limit, $name='') {
        if ($addressX==='' || $addressY==='') return ["code"=>400,"msg"=>"获取用户地址失败！"];
        $query = TableTabHospital::find();
        $name && $query->andWhere(['like', 'name', $name]);
        $hospital = $query->all();
        $list = [];
        foreach ($hospital as $key=>$value) {
            $distance = self::getDistance($value->address_x, $value->address_y, (float)$addressX, (float)$addressY);
            $list[$key]['id'] = $value->id;
            $list[$key]['icon'] = $value->icon;
            $list[$key]['name'] = $value->name;
            $list[$key]['brief'] = mb_substr($value->brief, 0, 25).'...';
            $list[$key]['distance'] = '距离'.$distance.'公里';
        }
        $count = count($list);
        ksort($list);
        $list = array_slice((array)$list,$offset,$limit);
        return ["code"=>200,"msg"=>"获取成功！","data"=>['list'=>$list,'count'=>$count]];
    }

    /**
     * 获取医院详情
     * @param $id
     * @param $addressX
     * @param $addressY
     * @return array
     */
    public function getHospitalDetails($id, $addressX, $addressY) {
        $hospital = Hospital::findOne($id);
        if (!$hospital) {
            return ["code"=>400,"msg"=>"获取医院详情失败！"];
        }
        $form = new HospitalForm();
        $form->getHospitalDetails($hospital, $addressX, $addressY);
        return ["code"=>200,"msg"=>"获取成功！","data"=>$form];
    }
    /**
     * 经纬度测距
     * @param $lng1 float 经度
     * @param $lng2 float 经度
     * @param $lat1 float 纬度
     * @param $lat2 float 纬度
     * @return float 距离，单位米
     */
    public static function getDistance($lng1,$lat1,$lng2,$lat2) {
        //将角度转为狐度
        $radLat1=deg2rad($lat1);
        $radLat2=deg2rad($lat2);
        $radLng1=deg2rad($lng1);
        $radLng2=deg2rad($lng2);
        $a=$radLat1-$radLat2;//两纬度之差,纬度<90
        $b=$radLng1-$radLng2;//两经度之差纬度<180
        return round(2*asin(sqrt(pow(sin($a/2),2)+cos($radLat1)*cos($radLat2)*pow(sin($b/2),2)))*6378.137,2);
    }

    /**
     * 申请代理商
     * @param $username
     * @param $phone
     * @return array
     */
    public function requestAgent($username, $phone) {
        $memberId = $this->getMemberId();
        $isRequest = Agent::findOne(['member_id'=>$memberId]);
        if ($isRequest) {
            if ($isRequest->state == C::AUDIT_ING)  return ["code"=>400,"msg"=>"你已申请代理商，请等待审核！"];
            if ($isRequest->state == C::AUDIT_SUCCESS)  return ["code"=>400,"msg"=>"该账户已是代理商！"];
        }
        $agent = new TableTabAgent();
        $agent->member_id = $memberId;
        $agent->username = $username;
        $agent->contact_phone = $phone;
        $agent->state = C::AUDIT_ING;
        if ($agent->save()) {
            return ["code"=>200,"msg"=>"提交成功！"];
        }
        return ["code"=>400,"msg"=>"提交失败！"];
    }

    /**
     * 获取两级下线会员
     * @return array
     */
    public function extendUser() {
        $memberId = $this->getMemberId();
        $relations = [];
        $relation = self::memberRelation([$memberId]);
        if (count($relation) == 0) return ["code"=>200,"msg"=>"成功！","data"=>['list'=>'','count'=>0]];
        if (count($relation) > 0) $relations = self::memberRelation($relation);
        $relation = array_unique(array_merge($relations,$relation));
        $member = UserMember::find()->Where(['in', 'id', $relation])->select(['icon','username','creation_time'])->all();
        foreach ($member as $key=>$value) {
            $value->icon == null && $member[$key]->icon = C::DEFAULT_MEMBER_ICON;
            $value->username == null && $member[$key]->username = C::DEFAULT_MEMBER_WE_CHAT_NAME;
            $member[$key]->creation_time = date("Y/m/d H:i",$value->creation_time);
        }
        return ["code"=>200,"msg"=>"成功！","data"=>['list'=>$member,'count'=>count($member)]];
    }

    private static function memberRelation($memberId) {
        $relation = MemberRelation::find()->Where(['in', 'superior_id', $memberId])->select('member_id')->asArray()->all();
        return array_column($relation,'member_id');
    }

    /**
     * 个人中心
     * @param string $username
     * @return array
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function myCenter($username = '') {
        $member = UserMember::findOne(self::getMemberId());
        if($username) {
            $member->username = $username;
            $member->update();
        }
        //面相报告 return array
        $facesReport = [];
        $faceOrder = FaceOrder::find()->where(['member_id'=>$member->id])->orderBy("id DESC")->all();
        if ($faceOrder) {
            foreach ($faceOrder as $key=>$value) {
                $facesReport[$key]['icon'] = $value->url;
                $facesReport[$key]['id'] = $value->id;
                $facesReport[$key]['state'] = $value->state;
            }
        }
        //是否是代理商 return bool
        $isAgent = $member->is_agent == C::AGENT_YES;
        //如果是医院账户，return 医院ID
        $hospitalId = 0;
        $isHospitalRequest = false; //是否医院申请成功
        if ($member->is_hospital == C::HOSPITAL_YES) {
            $hospital = self::getHospital();
            $hospital && $hospitalId = (int)$hospital->id;
        }else {
            $hospitalRequest = (new HospitalRequest())->getHospitalRequest(self::getMemberId());
            $isHospitalRequest = ($hospitalRequest && $hospitalRequest->state == C::AUDIT_SUCCESS) ? true : false;
        }
        return ["code"=>200,"msg"=>"成功！","data"=>['icon'=>$member->icon,'username'=>$member->username,'brokerage'=>$member->brokerage,'facesReport'=>$facesReport,'isAgent'=>$isAgent,'hospitalId'=>$hospitalId,'isHospitalRequest'=>$isHospitalRequest]];
    }

    /**
     * 佣金版面
     * @return array
     */
    public function brokeragePlate() {
        $member = UserMember::findOne(self::getMemberId());
        if (!$member) {
            return ["code"=>400,"msg"=>"用户错误！"];
        }
        $data['brokerage'] = $member->brokerage;
        $data['list'] = [];
        $brokerage = Brokerage::find()->where(['member_id'=>$member->id])->orderBy("id DESC")->all();
        for ($i=0;$i<count($brokerage);$i++) {
            if ($i==3) {
                break;
            }
            $type = $brokerage[$i]->br_type == 1 ? '+' : '-';
            $data['list'][$i]['username'] = $brokerage[$i]->username;
            $data['list'][$i]['amount'] = $type.$brokerage[$i]->amount;
            $data['list'][$i]['time'] = date("Y/m/d H:i",$brokerage[$i]->creation_time);
        }
        return ["code"=>200,"msg"=>"成功！","data"=>$data];
    }

    /**
     * 申请提现
     * @param $number
     * @param $username
     * @param $phone
     * @param $amount
     * @return array
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function withdraw($number,$username,$phone,$amount) {
        if ($amount == 0) {
            return ["code"=>400,"msg"=>"提现额不能为零！"];
        }
        $member = UserMember::findOne(self::getMemberId());
        if (!$member) {
            return ["code"=>400,"msg"=>"用户错误！"];
        }
        $brokerage = new Brokerage();
        $brokerage->member_id = $member->id;
        $brokerage->icon = $member->icon;
        $brokerage->username = $member->username;
        $brokerage->amount = $amount;
        $brokerage->br_type = 2;
        $brokerage->state = 1;
        $brokerage->bank_number = $number;
        $brokerage->bank_username = $username;
        $brokerage->phone = $phone;
        if (!$brokerage->save()) {
            return ["code"=>400,"msg"=>"申请提现失败！"];
        }
        $member->brokerage -= $amount;
        if (!$member->update()) {
            return ["code"=>400,"msg"=>"申请提现失败！"];
        }
        return ["code"=>200,"msg"=>"申请提现成功！"];
    }

    /**
     * 提现记录
     * @return array
     */
    public function getWithdrawLog() {
        $data['list'] = [];
        $log = Brokerage::find()->where(['member_id'=>self::getMemberId(),'br_type'=>2])->orderBy("id DESC")->all();
        foreach ($log as $key=>$value) {
            $data['list'][$key]['username'] = $value->username;
            $data['list'][$key]['amount'] = '+'.$value->amount;
            $data['list'][$key]['time'] = date("Y/m/d H:i",$value->creation_time);
        }
        $data['all_withdraw'] = array_sum(array_column($log,'amount'));
        return ["code"=>200,"msg"=>"成功！","data"=>$data];
    }
}
