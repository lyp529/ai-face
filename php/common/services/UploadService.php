<?php


namespace common\services;

use common\traits\InstanceTrait;

/**
 * Class UploadService 上传服务
 * @package common\services
 */
class UploadService extends BaseService
{
    use InstanceTrait;

    protected $upload_addr;

    /**
     * 上传图片
     * @param $imagesBase64
     * @param $fileName
     * @return bool|string
     */
    public function uploadImageBase64($imagesBase64, $fileName){
        if (!isset($imagesBase64['name']) || !$imagesBase64["tmp_name"]) {
            return false;
        }
        $newUrl = 'https://' . $_SERVER['SERVER_NAME'] . '/img/'.$fileName.'/';
        $this->upload_addr = $_SERVER['DOCUMENT_ROOT'] . '/img/'.$fileName.'/';

        if (!is_dir($this->upload_addr)){
            //如果不存在就创建该目录
            mkdir($this->upload_addr,0777,true);
        }
        $imgName = time() . '.' . substr(strrchr($imagesBase64['name'],'.'),1);
        $info = move_uploaded_file($imagesBase64["tmp_name"],$this->upload_addr . $imgName);

        if ($info) {
            return $newUrl . $imgName;
        } else {
            return false;
        }
    }

    /**
     * 获取小程序传来的图片
     * @param $fileName int 用户ID
     * @return array
     */
    public function uploadImage($fileName){
        $code = $_FILES['file'];//获取小程序传来的图片
        $this->upload_addr = $_SERVER['DOCUMENT_ROOT'] . '/img/'.$fileName.'/';
        $newUrl = 'https://' . $_SERVER['SERVER_NAME'] . '/img/'.$fileName.'/';
        if (!is_dir($this->upload_addr)){
            //如果不存在就创建该目录
            mkdir($this->upload_addr,0777,true);
        }
        $imgName = time().rand(1,1000)."-".date("Y-m-d").substr($code['name'],strrpos($code['name'],"."));
        $info = move_uploaded_file($code["tmp_name"],$this->upload_addr . $imgName);
        if ($info) {
            return ["code"=>200,"msg"=>"上传成功！","data"=>$newUrl . $imgName];
        } else {
            return ["code"=>400,"msg"=>"上传失败！"];
        }
    }

    public function imgToBase64($img_file) {
        $img_file = $_SERVER['DOCUMENT_ROOT'] . parse_url($img_file)['path'];
        $img_base64 = '';
        if (file_exists($img_file)) {
            $app_img_file = $img_file; // 图片路径
            $img_info = getimagesize($app_img_file); // 取得图片的大小，类型等

            //echo '<pre>' . print_r($img_info, true) . '</pre><br>';
            $fp = fopen($app_img_file, "r"); // 图片是否可读权限

            if ($fp) {
                $filesize = filesize($app_img_file);
                $content = fread($fp, $filesize);
                $file_content = chunk_split(base64_encode($content)); // base64编码
                switch ($img_info[2]) {           //判读图片类型
                    case 1: $img_type = "gif";
                        break;
                    case 2: $img_type = "jpg";
                        break;
                    case 3: $img_type = "png";
                        break;
                }

                $img_base64 = 'data:image/' . $img_type . ';base64,' . $file_content;//合成图片的base64编码

            }
            fclose($fp);
        }

        return $img_base64; //返回图片的base64
    }
}
