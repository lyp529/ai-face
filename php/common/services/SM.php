<?php


namespace common\services;


/**
 * Class SM Services Manager 服务管理器
 * @package common\services
 */
class SM
{
    /**
     * @return UserService
     */
    public static function getUserService() {
        return UserService::getIns();
    }

    /**
     * @return UploadService
     */
    public static function getUploadService() {
        return UploadService::getIns();
    }

    /**
     * @return LogService
     */
    public static function getLogService() {
        return LogService::getIns();
    }

    /**
     * @return OrderService
     */
    public static function getOrderService() {
        return OrderService::getIns();
    }

    /**
     * @return NotifyService
     */
    public static function getNotifyService() {
        return NotifyService::getIns();
    }
}
