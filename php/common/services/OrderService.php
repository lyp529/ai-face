<?php
namespace common\services;

use common\classes\WeChatPay;
use common\constants\C;
use common\constants\F;
use common\exceptions\ForeseeableException;
use common\exceptions\SystemException;
use common\models\db\Brokerage;
use common\models\db\FaceOrder;
use common\models\db\FaceReport;
use common\models\db\Hospital;
use common\models\db\HospitalOrder;
use common\models\db\InfoConfig;
use common\models\db\UserMember;
use common\models\table\TableCfgFaceReport;
use common\models\table\TableTabFaceOrder;
use common\models\table\TableTabHospital;
use common\traits\InstanceTrait;
use common\utils\FaceAnalyzeUtil;
use common\utils\FaceDetectUtil;
use Throwable;
use yii\db\StaleObjectException;

/**
 * Class OrderService 订单服务
 * @package common\services
 */
class OrderService extends BaseService
{
    use InstanceTrait;

    /**
     * 获取预约订单列
     * @return array
     */
    public function getHospitalOrderList() {
        $hospital = HospitalOrder::find()->Where(['member_id'=>SM::getUserService()->getMemberId()])->select(['id', 'hospital_id', 'services'])->all();
        if (!$hospital) return ["code"=>200,"msg"=>"没有预约订单！"];
        $list = [];
        foreach ($hospital as $key=>$value) {
            $list[$key]['id'] = $value->id;
            $list[$key]['hospital_id'] = $value->hospital_id;
            $list[$key]['services'] = $value->services;
        }
        return ["code"=>200,"msg"=>"获取成功！","data"=>$list];
    }

    /**
     * 获取医院订单详情
     * @param $id
     * @param $addressX
     * @param $addressY
     * @return array
     */
    public function getHospitalOrderDetails($id, $addressX, $addressY) {
        $hospitalOrder = HospitalOrder::findOne($id);
        if (!$hospitalOrder) {
            return ["code"=>400,"msg"=>"该预约订单不存在！"];
        }
        $hospital = Hospital::findOne($hospitalOrder->hospital_id);
        if (!$hospital || $addressX==='' || $addressY==='') return ["code"=>400,"msg"=>"查看详情失败！"];

        $distance = SM::getUserService()->getDistance($hospital->address_x, $hospital->address_y, (float)$addressX, (float)$addressY);
        $details['id'] = $hospital->id;
        $details['name'] = $hospital->name;
        $details['icon'] = $hospital->icon;
        $details['distance'] = '距离'.$distance.'公里';
        $details['brief'] = mb_substr($hospital->brief, 0, 25).'...';;
        $details['services'] = $hospitalOrder->services;
        $details['username'] = $hospitalOrder->username;
        $details['contact_phone'] = $hospitalOrder->contact_phone;
        $details['appointment_time'] = date("Y/m/d H:i",$hospitalOrder->appointment_time);
        return ["code"=>200,"msg"=>"查看详情成功！","data"=>$details];
    }

    /**
     * （医院）统一下单
     * @param $hospitalId int 医院ID
     * @param $services int 服务项目
     * @param $username string 姓名
     * @param $contactPhone string 联系电话
     * @param $appointmentTime string 预约时间
     * @return array
     */
    public function hospitalAppointment($hospitalId, $services, $username, $contactPhone, $appointmentTime) {
        $hospital = Hospital::findOne($hospitalId);
        if(!$hospital) {
            return ["code"=>400,"msg"=>"医院不存在！"];
        }
        $memberId = SM::getUserService()->getMemberId();
        $member = UserMember::findOne($memberId);
        if(!$memberId || !$member) {
            return ["code"=>400,"msg"=>"用户出错！"];
        }
        $config = InfoConfig::findOne(C::infoConfig[1]);
        $hospitalOrder = new HospitalOrder();
        $hospitalOrder->member_id = $memberId;
        $hospitalOrder->order_number = self::randOrderNumber();
        $hospitalOrder->amount = $config->cfg_value;
        $hospitalOrder->services = $services;
        $hospitalOrder->hospital_id = $hospitalId;
        $hospitalOrder->username = $username;
        $hospitalOrder->contact_phone = $contactPhone;
        $hospitalOrder->appointment_time = $appointmentTime;
        $hospitalOrder->state = C::APPOINTMENT_ING;
        $hospitalOrder->creation_time = time();
        $hospitalOrder->update_time = time();
        if(!$hospitalOrder->save()) {
            return ["code"=>400,"msg"=>"下单失败！"];
        }
        $notifyUrl = 'https://' . $_SERVER['SERVER_NAME'] . '/hospital.php';
        return (new WeChatPay())->unifiedOrder($member->openid, $hospitalOrder->order_number, $hospitalOrder->amount, '预约医院', $notifyUrl);
    }

    private static function randOrderNumber() {
        $orderNumber = (int)date('His').mt_rand(1000,9999);
        $order = HospitalOrder::findOne(['order_number'=>$orderNumber]);
        if ($order) {
            return self::randOrderNumber();
        }else {
            return $orderNumber;
        }
    }
    /**
     * 人脸检测与分析
     * @param $url
     * @return array
     */
    public function faceDetect($url) {
        if(!$url) {
            return ["code"=>400,"msg"=>"图片为空！"];
        }
        $detect = (new FaceDetectUtil())->faceDetect($url);
        if (isset($detect['Error']) && isset($detect['Error']['Code'])) {
            return ["code"=>400,"msg"=>$detect['Error']['Message']];
        }
        //性别[0~49]为女性，[50，100]为男性
        $gender = $detect['FaceInfos'][0]['FaceAttributesInfo']['Gender'];
        $isEyeOpen = $detect['FaceInfos'][0]['FaceAttributesInfo']['EyeOpen'];
        $isGlass = $detect['FaceInfos'][0]['FaceAttributesInfo']['Glass'];
        $orderId = 0;
        if ($isEyeOpen && !$isGlass) {
            $report = new TableCfgFaceReport();
            $report->overview = '';
            if(!$report->save()) {
                return ["code"=>400,"msg"=>"人脸检测与分析失败！"];
            }
            $config = InfoConfig::findOne(C::infoConfig[2]);
            $order = new TableTabFaceOrder();
            $order->member_id = SM::getUserService()->getMemberId();
            $order->re_id = $report->id;
            $order->order_number = self::randOrderNumber();
            $order->amount = $config->cfg_value;
            $order->url = $url;
            $order->gender = $gender < 50 ? 1 : 2;
            $order->age = $detect['FaceInfos'][0]['FaceAttributesInfo']['Age'];
            $order->state = C::APPOINTMENT_ING;
            if(!$order->save()) {
                return ["code"=>400,"msg"=>"人脸检测与分析失败！"];
            }
            $orderId = $order->id;
        }

        $roll = $detect['FaceInfos'][0]['FaceAttributesInfo']['Roll'];  //平面旋转[-180,180]

        //双眼是否睁开
        $eyeOpen = '闭眼，';
        $isEyeOpen && $eyeOpen = '睁眼，';

        //是否有眼镜
        $glass = '未戴眼镜';
        $isGlass && $glass = '戴眼镜';

        //质量分: [0,100]是否适合人脸识别；[0,40]较差，[40,60] 一般，[60,80]较好，[80,100]很好
        $score = $detect['FaceInfos'][0]['FaceQualityInfo']['Score'];

        $data['head_gesture']['label'] = $roll > 0 ? '右倾斜'.$roll.'°' : ($roll < 0 ? '左倾斜'.abs($roll).'°' : '无倾斜');
        $data['head_gesture']['state'] = true;
        $data['eye_states']['label'] = $eyeOpen.$glass;
        $data['eye_states']['state'] = ($isEyeOpen && !$isGlass) ? true : false;
        $data['face_recognition']['label'] = $score < 41 ? '较差' : ($score < 61 ? '一般' : ($score < 81 ? '较好' : '很好'));
        $data['face_recognition']['state'] = true;
        $data['url'] = $url;
        $data['order_id'] = $orderId;
        return ["code"=>200,"msg"=>"查看详情成功！","data"=>$data];
    }

    /**
     * 提交面相报告
     * @param $orderId
     * @return array
     */
    public function faceAnalyze($orderId) {
        $order = FaceOrder::findOne($orderId);
        if(!$order) {
            return ["code"=>400,"msg"=>"提交失败！"];
        }
        $config = InfoConfig::findOne(C::infoConfig[2]);
        return ["code"=>200,"msg"=>"报告已生成！", "data"=>['label'=>'支付'.$config->cfg_value.'元领取报告', 'order_id'=>$orderId]];
    }

    /**
     * 面相报告支付
     * @param $orderId
     * @return array
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function faceAnalyzePay($orderId) {
        $order = FaceOrder::findOne(['id'=>$orderId, 'state'=>C::APPOINTMENT_ING]);
        if(!$order) {
            return ["code"=>400,"msg"=>"该报告不存在！"];
        }
        $member = UserMember::findOne($order->member_id);
        if(!$member) {
            return ["code"=>400,"msg"=>"用户错误！"];
        }
        $analyze = (new FaceAnalyzeUtil())->faceAnalyze($order->url);
        self::faceReport($analyze,$order->gender,$order->re_id);
        $notifyUrl = 'https://' . $_SERVER['SERVER_NAME'] . '/face.php';
        return (new WeChatPay())->unifiedOrder($member->openid, $order->order_number, $order->amount, '领取面相报告', $notifyUrl);
        //查询支付是否成功

    }

    /**
     * 更新面相报告
     * @param $analyze
     * @param $gender
     * @param $reId
     * @throws StaleObjectException
     * @throws Throwable
     */
    private static function faceReport($analyze,$gender,$reId) {
        //脸宽度
        $faceWidth = $analyze['FaceShapeSet'][0]['FaceProfile'][20]['X'] - $analyze['FaceShapeSet'][0]['FaceProfile'][0]['X'];

        //眉心
        $eyebrows = ((int)$analyze['FaceShapeSet'][0]['LeftEyeBrow'][6]['Y'] + (int)$analyze['FaceShapeSet'][0]['RightEyeBrow'][6]['Y'])/2;
        //额头最高点
        $top = (int)max(array_column($analyze['FaceShapeSet'][0]['FaceProfile'],'Y'));
        //鼻子最低点
        $noseBottom = (int)min(array_column($analyze['FaceShapeSet'][0]['Nose'],'Y'));

        $overview = self::faceProfile($analyze['FaceShapeSet'][0]['FaceProfile'],$gender); //面相概述
        //额头高低
        if (($top - $eyebrows) >= ($eyebrows - $noseBottom)) {
            $overview .= F::FORE_HAD_HIGH;
        }else {
            $overview .= F::FORE_HAD_COMMON;
        }
        $eyeAnalysis = self::eyeAnalysis($analyze['FaceShapeSet'][0],$faceWidth);//眼睛解析
        $mouthAnalysis = self::mouthAnalysis($analyze['FaceShapeSet'][0]['Mouth'],$faceWidth,$gender);//嘴相解析
        $noseAnalysis = self::noseAnalysis($analyze['FaceShapeSet'][0]['Nose'],$faceWidth);     //鼻相解析

        $brow = array_merge(array_column($analyze['FaceShapeSet'][0]['LeftEyeBrow'],'Y'),array_column($analyze['FaceShapeSet'][0]['RightEyeBrow'],'Y'));
        $eye = array_merge(array_column($analyze['FaceShapeSet'][0]['LeftEye'],'Y'),array_column($analyze['FaceShapeSet'][0]['RightEye'],'Y'));
        $mouth = array_column($analyze['FaceShapeSet'][0]['Mouth'],'Y');
        $nose = array_column($analyze['FaceShapeSet'][0]['Nose'],'Y');

        $faceReport = FaceReport::findOne($reId);
        $faceReport->overview = $overview;
        $faceReport->eye_analysis = $eyeAnalysis;
        $faceReport->mouth_analysis = $mouthAnalysis;
        $faceReport->nose_analysis = $noseAnalysis;
        $faceReport->eye_top = (int)min($brow);
        $faceReport->eye_bottom = (int)$analyze['ImageHeight'] - (int)max($eye);
        $faceReport->mouth_top = (int)min($mouth);
        $faceReport->mouth_bottom = (int)$analyze['ImageHeight'] - (int)max($mouth);
        $faceReport->nose_top = (int)min($nose);
        $faceReport->nose_bottom = (int)$analyze['ImageHeight'] - (int)max($nose);
        $faceReport->update();
    }

    /**
     * 鼻相解析
     * @param $nose
     * @param $faceWidth
     * @return array|string
     */
    private static function noseAnalysis($nose,$faceWidth) {
        $noseWidth = max(array_column($nose,'X')) - min(array_column($nose,'X'));
        if ($noseWidth * 2.5 <= $faceWidth && $noseWidth * 4 >= $faceWidth) {
            return F::getNose(F::NOSE_STRAIGHT);
        }elseif ($noseWidth * 2.5 > $faceWidth) {
            return F::getNose(F::NOSE_BIG);
        }elseif ($noseWidth * 4 < $faceWidth) {
            return F::getNose(F::NOSE_MINOR);
        }
        return '';
    }

    /**
     * 嘴相解析
     * @param $mouth
     * @param $faceWidth
     * @param $gender
     * @return string
     */
    private static function mouthAnalysis($mouth,$faceWidth,$gender) {
        $str = '';
        $mouthWidth = max(array_column($mouth,'X')) - min(array_column($mouth,'X'));
        if ($mouthWidth/2 * 3 >= $faceWidth) {
            $gender == 1 && $str .= F::getMouth(F::MOUTH_BIG_GIRL);
            $gender == 2 && $str .= F::getMouth(F::MOUTH_BIG_MAN);
        }elseif ($mouthWidth/2 * 3 < $faceWidth && $mouthWidth > $faceWidth/2) {
            $str .= F::getMouth(F::MOUTH_COMMON);
        }else {
            $str .= F::getMouth(F::MOUTH_SMALL);
        }

        $str .= $gender == 1 ? F::getMouth(F::MOUTH_SKEW_MAN) : F::getMouth(F::MOUTH_SKEW_GIRL);
        return $str;
    }
    /**
     * 面相概述
     * @param $faceProfile
     * @param $gender
     * @return string
     */
    private static function faceProfile($faceProfile,$gender) {
        $str = '';
        $feature = F::FEATURE_SKEW;
        $f0 = $faceProfile[0];
        $f4 = $faceProfile[4];
        $f5 = $faceProfile[5];
        $f6 = $faceProfile[6];
        $f7 = $faceProfile[7];
        $f8 = $faceProfile[8];
        $f9 = $faceProfile[9];
        $f10 = $faceProfile[10];
        $f11 = $faceProfile[11];
        $f12 = $faceProfile[12];
        $f13 = $faceProfile[13];
        $f14 = $faceProfile[14];
        $f15 = $faceProfile[15];
        $f16 = $faceProfile[16];
        $f20 = $faceProfile[20];
        if(($f16['X'] - $f15['X']) < 20 && ($f15['X'] - $f14['X']) < 20 && ($f14['X'] - $f13['X']) < 20 && ($f13['X'] - $f12['X']) < 20 && ($f12['X'] - $f11['X']) < 20 && ($f11['X'] - $f10['X']) < 20 && ($f10['X'] - $f9['X']) < 20 && ($f9['X'] - $f8['X']) < 20 && ($f8['X'] - $f7['X']) < 20 && ($f7['X'] - $f6['X']) < 20 && ($f6['X'] - $f5['X']) < 20 && ($f5['X'] - $f4['X']) < 20) {
            if(($f11['Y'] - $f12['Y']) < 20 && ($f12['Y'] - $f13['Y']) < 20 && ($f13['Y'] - $f12['Y']) < 20 && ($f12['Y'] - $f13['Y']) < 20 && ($f13['Y'] - $f14['Y']) < 20 && ($f14['Y'] - $f15['Y']) < 20 && ($f15['Y'] - $f16['Y']) < 20 && ($f10['Y'] - $f9['Y']) < 20 && ($f9['Y'] - $f8['Y']) < 20 && ($f8['Y'] - $f7['Y']) < 20 && ($f7['Y'] - $f6['Y']) < 20 && ($f6['Y'] - $f5['Y']) < 20) {
                if (($f12['X'] - $f11['X']) < 5 && ($f11['X'] - $f10['X']) < 5 && ($f10['X'] - $f9['X']) < 5) {
                    $feature = F::FEATURE_MA_TTS;
                }else {
                    $feature = F::FEATURE_WATER;
                }
            }
        }
        ($f20['X'] - $f0['X'])/2 >= ($f10['Y'] - (($f0['Y'] + $f20['Y'])/2)) && $feature = F::FEATURE_BY;
        ($f20['X'] - $f0['X']) <= ($f10['Y'] - (($f0['Y'] + $f20['Y'])/2)) && $feature = F::FEATURE_WORD;


        $str .= F::getFeatureLuck($feature);
        $str .= F::getFeatureOffspring($feature);
        $str .= F::getFeatureParents($feature);
        $str .= F::getFeatureEducation($feature);
        $gender == 1 && $str .= F::getFeatureMarriageGirl($feature);
        $gender == 2 && $str .= F::getFeatureMarriageMan($feature);
        return $str;
    }

    /**
     * 眼睛解析
     * @param $faceShapeSet
     * @param $faceProfileX
     * @return array|string
     */
    private static function eyeAnalysis($faceShapeSet,$faceProfileX) {
        $leftEye = $faceShapeSet['LeftEye'];
        $rightEye = $faceShapeSet['RightEye'];
        $leftEyeBrow = $faceShapeSet['LeftEyeBrow'];
        $rightEyeBrow = $faceShapeSet['RightEyeBrow'];
        $str = '';
        $eye = F::EYE_MINOR;
        if (((($leftEye[3]['X'] + $leftEye[4]['X'] + $leftEye[5]['X'])/3 - $leftEye[0]['X']) + $rightEye[0]['X'] - $rightEye[4]['X']) * 4 >= $faceProfileX) {
            $eye = F::EYE_BIG;
        }
        $str .= F::getEye($eye);

        //眼睛平均长度
        $leftEyeX = (int)max(array_column($leftEye,'X')) - (int)min(array_column($leftEye,'X'));
        $rightEyeX = (int)max(array_column($rightEye,'X')) - (int)min(array_column($rightEye,'X'));
        $eyeX= ($leftEyeX + $rightEyeX)/2;

        //眉毛平均长度
        $leftEyeBrowX = (int)max(array_column($leftEyeBrow,'X')) - (int)min(array_column($leftEyeBrow,'X'));
        $rightEyeBrowX = (int)max(array_column($rightEyeBrow,'X')) - (int)min(array_column($rightEyeBrow,'X'));
        $browX= ($leftEyeBrowX + $rightEyeBrowX)/2;

        if (($browX - $eyeX) >= 3) {
            $str .= F::getBrow(F::BROW_LONG);
        }elseif ($browX <= $eyeX) {
            $str .= F::getBrow(F::BROW_SHORT);
        }else {
            $str .= F::getBrow(F::BROW_COMMON);
        }

        if (abs($leftEyeBrow[1]['Y'] - $leftEyeBrow[3]['Y']) >= 3 || abs($leftEyeBrow[5]['Y'] - $leftEyeBrow[3]['Y']) >= 3) {
            if (abs($rightEyeBrow[1]['Y'] - $rightEyeBrow[3]['Y']) >= 3 || abs($rightEyeBrow[5]['Y'] - $rightEyeBrow[3]['Y']) >= 3) {
                $str .= F::getBrow(F::BROW_BEND);
            }
        }

        if (abs($leftEyeBrow[0]['Y'] - $leftEyeBrow[2]['Y']) >= 5 || abs($leftEyeBrow[4]['Y'] - $leftEyeBrow[2]['Y']) >= 5) {
            if (abs($rightEyeBrow[0]['Y'] - $rightEyeBrow[2]['Y']) >= 5 || abs($rightEyeBrow[4]['Y'] - $rightEyeBrow[2]['Y']) >= 5) {
                $str .= F::getBrow(F::BROW_ROUGH);
            }
        }
        return $str;
    }

    /**
     * 获取面向报告
     * @param $orderId
     * @return array
     */
    public function getFaceReport($orderId) {
        $order = FaceOrder::findOne(['id'=>$orderId]);
        if(!$order) {
            return ["code"=>400,"msg"=>"该报告不存在！"];
        }
        if($order->state == C::APPOINTMENT_ING) {
            return ["code"=>400,"msg"=>"该报告未付款！"];
        }
        $report = FaceReport::findOne($order->re_id);
        if(!$report) {
            return ["code"=>400,"msg"=>"该报告不存在！"];
        }
        $data['overview'] = $report->overview;
        $data['eye_analysis'] = $report->eye_analysis;
        $data['mouth_analysis'] = $report->mouth_analysis;
        $data['nose_analysis'] = $report->nose_analysis;
        $data['remark'] = $order->remark;
        $data['url'] = $order->url;
        $data['eye_top'] = $report->eye_top;
        $data['eye_bottom'] = $report->eye_bottom;
        $data['mouth_top'] = $report->mouth_top;
        $data['mouth_bottom'] = $report->mouth_bottom;
        $data['nose_top'] = $report->nose_top;
        $data['nose_bottom'] = $report->nose_bottom;
        $data['hospital_list'] = self::getHospitalListRandom();
        return ["code"=>200,"msg"=>"获取成功！", "data"=>$data];
    }

    /**
     * 获取医院推荐列表
     * @return array
     */
    private static function getHospitalListRandom() {
        $hospitalList = TableTabHospital::find()->all();
        $list = [];
        foreach ($hospitalList as $key=>$value) {
            $list[$key]['id'] = $value->id;
            $list[$key]['icon'] = $value->icon;
            $list[$key]['name'] = $value->name;
            $list[$key]['brief'] = mb_substr($value->brief, 0, 25).'...';
        }
        if (count($list) <= 3) {
            return $list;
        }
        $rand = array_rand($list,3);
        return [$list[$rand[0]],$list[$rand[1]],$list[$rand[2]]];
    }

    /**
     * 面相报告留言
     * @param $orderId
     * @param $remark
     * @return array
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function faceOrderRemark($orderId,$remark) {
        if($remark == '') {
            return ["code"=>400,"msg"=>"留言不能为空！"];
        }
        $order = FaceOrder::findOne($orderId);
        if(!$order) {
            return ["code"=>400,"msg"=>"留言失败！"];
        }
        $order->remark = $remark;
        if (!$order->update()) {
            return ["code"=>400,"msg"=>"留言失败！"];
        }
        return ["code"=>200,"msg"=>"留言成功！"];
    }

    /**
     * 医院账户获取预约订单详情
     * @param $id
     * @return array
     */
    public function getReportOrderDetails($id) {
        $list = HospitalOrder::findOne($id);
        if (!$list) {
            return ["code"=>400,"msg"=>"获取预约订单详情失败！"];
        }
        $order['username'] = $list->username;
        $order['project'] = $list->services;
        $order['contact_phone'] = $list->contact_phone;
        $order['appointment_time'] = date("Y-m-d H:i:s",$list->appointment_time);
        $order['creation_time'] = date("Y-m-d H:i:s",$list->creation_time);
        return ["code"=>200,"msg"=>"获取成功！","data"=>$order];
    }

    /**
     * 医院账户获取预约订单列
     * @return array
     */
    public function getReportOrderList() {
        $member = SM::getUserService()->getMember();
        if ($member->is_hospital != 1) {
            return ["code"=>400,"msg"=>"您还不是医院账户！"];
        }
        $hospital = Hospital::findOne(['member_id'=>$member->id]);
        if (!$hospital) {
            return ["code"=>400,"msg"=>"获取医院信息失败！"];
        }
        $list = HospitalOrder::find()->Where(['hospital_id'=>$hospital->id])->select(['id','services'])->all();
        if (!$list) return ["code"=>200,"msg"=>"没有预约订单！"];
        $lists = [];
        foreach ($list as $key=>$value) {
            $lists[$key]['id'] = $value->id;
            $lists[$key]['services'] = $value->services;
        }
        return ["code"=>200,"msg"=>"获取成功！","data"=>$lists];
    }
    /**
     * 面相订单表
     * @param $offset
     * @param $limit
     * @param $state
     * @return array
     */
    public function getFaceOrderList($offset, $limit, $state) {
        $query = TableTabFaceOrder::find();
        $state && $query->andWhere(["state" => $state]);
        $count = $query->count();
        $list = $query->orderBy("id DESC")->offset($offset)->limit($limit)->asArray()->all();
        if ($list) {
            foreach ($list as $k=>$v) {
                $list[$k]['username'] = UserMember::findOne($v['member_id'])->username;
                $list[$k]['face_report'] = FaceReport::findOne($v['re_id']);
                $list[$k]['state_label'] = $v['state'] == 1 ? '未付款' : ($v['state'] == 2 ? '已付款' : '付款失败');
                $list[$k]['creation_time'] = date("Y-m-d H:i:s",$v['creation_time']);
                $list[$k]['update_time'] = date("Y-m-d H:i:s",$v['update_time']);
            }
        }
        return ["rows" => $list,"count" => $count];
    }

    /**
     * 更新面相数据
     * @param $id
     * @param $overview
     * @param $eye
     * @param $mouth
     * @param $nose
     * @throws ForeseeableException
     * @throws SystemException
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function updateFaceReport($id, $overview, $eye, $mouth, $nose) {
        $report = FaceReport::findOne($id);
        if (!$report) {
            throw new ForeseeableException("该面相不存在！");
        }
        $overview && $report->overview = $overview;
        $eye && $report->eye_analysis = $eye;
        $mouth && $report->mouth_analysis = $mouth;
        $nose && $report->nose_analysis = $nose;
        if (!$report->update()) {
            throw new SystemException($report->getFirstError());
        }
    }

    /**
     * 获取佣金列
     * @param $offset
     * @param $limit
     * @param $brType
     * @param $state
     * @return array
     */
    public function getBrokerageList($offset, $limit, $brType, $state) {
        $query = Brokerage::find();
        $state && $query->andWhere(["state" => $state]);
        $brType && $query->andWhere(["br_type" => $brType]);
        $count = $query->count();
        $list = $query->orderBy("id DESC")->offset($offset)->limit($limit)->asArray()->all();
        if ($list) {
            foreach ($list as $k=>$v) {
                $list[$k]['state_label'] = $v['state'] == 1 ? '申请中' : ($v['state'] == 2 ? '已完成' : '失败');
                $list[$k]['type_label'] = $v['br_type'] == 1 ? '入账佣金' : '提现佣金';
                $list[$k]['creation_time'] = date("Y-m-d H:i:s",$v['creation_time']);
                $list[$k]['update_time'] = date("Y-m-d H:i:s",$v['update_time']);
                $list[$k]['showColumn'] = ($v['br_type'] == 2 && $v['state'] == 1) ? true : false;
            }
        }
        return ["rows" => $list,"count" => $count];
    }

    /**
     * 更新提现申请
     * @param $id
     * @param $state
     * @throws ForeseeableException
     * @throws StaleObjectException
     * @throws SystemException
     * @throws Throwable
     */
    public function updateWithdraw($id, $state) {
        $brokerage = Brokerage::findOne(['id'=>$id, 'br_type'=>2, 'state'=>1]);
        if (!$brokerage) {
            throw new ForeseeableException("该提现申请不存在！");
        }
        $brokerage->state = (int)$state;
        if (!$brokerage->update()) {
            throw new SystemException($brokerage->getFirstError());
        }
        if ($state == 3) {
            $member = UserMember::findOne($brokerage->member_id);
            if (!$member) {
                throw new ForeseeableException("用户不存在！");
            }
            $member->brokerage += $brokerage->amount;
            if (!$member->update()) {
                throw new SystemException($member->getFirstError());
            }
        }
    }
}
