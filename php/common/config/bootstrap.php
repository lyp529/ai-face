<?php
error_reporting(E_ALL);
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@api', dirname(dirname(__DIR__)) . '/api');
Yii::setAlias('@payment', dirname(dirname(__DIR__)) . '/payment');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
// 小微商户第三方库
Yii::setAlias('@wei', dirname(dirname(__DIR__)) . '/common/3rd/yandy/wechat-xiaowei/src');


include_once __DIR__ . '/define.php';
include_once __DIR__ . '/functions.php';
