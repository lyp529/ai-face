<?php


namespace common\exceptions;


use yii\base\UserException;

/**
 * Class SystemException 系统异常。返回消息给客户端时，只提示：系统错误
 * @package common\exceptions
 */
class SystemException extends UserException {

}