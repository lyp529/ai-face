<?php
namespace common\utils;

use common\services\SM;
use TencentCloud\Common\Credential;
use TencentCloud\Common\Profile\ClientProfile;
use TencentCloud\Common\Profile\HttpProfile;
use TencentCloud\Iai\V20200303\IaiClient;
use TencentCloud\Iai\V20200303\Models\AnalyzeFaceRequest;

/**
 * Class FaceAnalyzeUtil 五官定位
 * @package common\utils
 */
class FaceAnalyzeUtil
{
    public static function faceAnalyze($url) {
        $img = SM::getUploadService()->imgToBase64($url);
        require_once '../../vendor/autoload.php';
        $cred = new Credential("AKIDcNrNHvu0Ujio35406MNimZ7A0ldFsgof", "qPhVBdpDMFqQkofqUtwiPwaEdXv2seBV");
        $httpProfile = new HttpProfile();
        $httpProfile->setEndpoint("iai.tencentcloudapi.com");

        $clientProfile = new ClientProfile();
        $clientProfile->setHttpProfile($httpProfile);
        $client = new IaiClient($cred, "ap-guangzhou", $clientProfile);
        $req = new AnalyzeFaceRequest();
        $req->fromJsonString(json_encode(["NeedRotateDetection" => 1, "Image" => $img]));
        $resp = $client->AnalyzeFace($req);
        if (is_array($resp) && isset($resp['Error'])) return $resp;
        return json_decode($resp->toJsonString(),true);
    }

    public static function faceAnalyzeUrl($url) {
        require_once '../../vendor/autoload.php';
        $cred = new Credential("AKIDcNrNHvu0Ujio35406MNimZ7A0ldFsgof", "qPhVBdpDMFqQkofqUtwiPwaEdXv2seBV");
        $httpProfile = new HttpProfile();
        $httpProfile->setEndpoint("iai.tencentcloudapi.com");

        $clientProfile = new ClientProfile();
        $clientProfile->setHttpProfile($httpProfile);
        $client = new IaiClient($cred, "ap-guangzhou", $clientProfile);
        $req = new AnalyzeFaceRequest();
        $req->fromJsonString(json_encode(["NeedRotateDetection" => 1, "Url" => $url]));
        $resp = $client->AnalyzeFace($req);
        if (is_array($resp) && isset($resp['Error'])) return $resp;
        return json_decode($resp->toJsonString(),true);
    }

    public static function faceAnalyzeUrlBase64($url) {
        $img = SM::getUploadService()->imgToBase64($url);
        require_once '../../vendor/autoload.php';
        $cred = new Credential("AKIDcNrNHvu0Ujio35406MNimZ7A0ldFsgof", "qPhVBdpDMFqQkofqUtwiPwaEdXv2seBV");
        $httpProfile = new HttpProfile();
        $httpProfile->setEndpoint("iai.tencentcloudapi.com");

        $clientProfile = new ClientProfile();
        $clientProfile->setHttpProfile($httpProfile);
        $client = new IaiClient($cred, "ap-guangzhou", $clientProfile);
        $req = new AnalyzeFaceRequest();
        $req->fromJsonString(json_encode(["NeedRotateDetection" => 1, "Image" => $img]));
        $resp = $client->AnalyzeFace($req);
        if (is_array($resp) && isset($resp['Error'])) return $resp;
        return json_decode($resp->toJsonString(),true);
    }
}
