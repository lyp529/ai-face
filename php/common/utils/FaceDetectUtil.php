<?php
namespace common\utils;

use common\services\SM;
use TencentCloud\Common\Credential;
use TencentCloud\Common\Profile\ClientProfile;
use TencentCloud\Common\Profile\HttpProfile;
use TencentCloud\Iai\V20200303\IaiClient;
use TencentCloud\Iai\V20200303\Models\DetectFaceRequest;

/**
 * Class FaceDetectUtil 人脸检测与分析
 * @package common\utils
 */
class FaceDetectUtil
{
    public static function faceDetect($url) {
        require_once '../../vendor/autoload.php';
        $cred = new Credential("AKIDcNrNHvu0Ujio35406MNimZ7A0ldFsgof", "qPhVBdpDMFqQkofqUtwiPwaEdXv2seBV");
        $httpProfile = new HttpProfile();
        $httpProfile->setEndpoint("iai.tencentcloudapi.com");

        $clientProfile = new ClientProfile();
        $clientProfile->setHttpProfile($httpProfile);
        $client = new IaiClient($cred, "ap-guangzhou", $clientProfile);

        $req = new DetectFaceRequest();

        $img = SM::getUploadService()->imgToBase64($url);
        $req->fromJsonString(json_encode(["NeedFaceAttributes" => 1, "NeedRotateDetection" => 1, "Image" => $img]));

        $resp = $client->DetectFace($req);
        if (is_array($resp) && isset($resp['Error'])) return $resp;
        return json_decode($resp->toJsonString(),true);
    }
}
