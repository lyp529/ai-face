<?php
namespace common\utils;

use Closure;
use common\exceptions\SystemException;

/**
 * Class DBUtil 数据库工具
 * @package common\utils
 */
class DBUtil {

    /**
     * 在数据库事务下运行该方法（简化事务的写法。并且可以处理多重事务导致逻辑错乱的问题）
     *
     * @param Closure $run
     * @param Closure|null $err 出现异常时执行的方法
     * @throws SystemException
     */
    public static function runOnTransaction(Closure $run, Closure $err = null) {
        $transaction = \Yii::$app->db->beginTransaction();

        try {
            $run();
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            if ($err !== null) {
                $err();
            }
            throw new SystemException($e->getMessage()); // 以 erp 模块可捕捉的异常抛出
        }
    }
}