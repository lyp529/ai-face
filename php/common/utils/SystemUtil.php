<?php


namespace common\utils;


use common\constants\C;

/**
 * Class SystemUtil 系统、环境工具
 * @package common\utils
 */
class SystemUtil
{
    /**
     * 判断当前运行环境是不是windows
     * @return bool
     */
    public static function isWin() {
        return strtoupper(substr(PHP_OS, 0, 3)) === 'WIN';
    }

}
