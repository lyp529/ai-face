<?php
/**
 * Created by PhpStorm.
 * User: ewing
 * Date: 2019-08-07
 * Time: 15:03
 */

namespace common\utils;


class CurlUtil
{

    /**
     * 只获取内容
     * @param $url
     * @return mixed
     */
    public static function getContent($url) {
        $arr = self::httpsRequest($url);
        return $arr[0];
    }

    /**
     * httpsRequest  https请求（支持GET和POST）
     * @param $url
     * @param string $data
     * @param array $headers
     * @param int $timeout
     * @param bool $followLocation 是否接受重定向
     * @return array
     */
    public static function httpsRequest($url, $data = '', array $headers = [], $timeout = 30, $followLocation = false) {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);

        $data && curl_setopt($curl, CURLOPT_POST, 1); // POST
        $data && curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

        curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);   // 设置超时

        if (substr($url, 0, 5) == 'https') {
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // 信任任何证书
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2); // 检查证书中是否设置域名
        }
        // 使用sock5代理模式
//        curl_setopt($curl,CURLOPT_PROXY, '127.0.0.1');
//        curl_setopt($curl,CURLOPT_PROXYPORT, '1081');
//        curl_setopt($curl,CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);

        if (!empty($headers)) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            // curl_setopt($curl, CURLINFO_HEADER_OUT, true); //TRUE 时追踪句柄的请求字符串，从 PHP 5.1.3 开始可用。这个很关键，就是允许你查看请求header
            // $headers = curl_getinfo($curl, CURLINFO_HEADER_OUT); //官方文档描述是“发送请求的字符串”，其实就是请求的header。这个就是直接查看请求header，因为上面允许查看
        }
        curl_setopt($curl, CURLOPT_HEADER, true);    // 是否需要响应 header
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        if ($followLocation) {
            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        }

        $output = curl_exec($curl);
        $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);    // 获得响应结果里的：头大小
        $response_header = substr($output, 0, $header_size);    // 根据头大小去获取头信息内容
        $http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);    // 获取响应状态码
        $response_body = substr($output, $header_size);
        $error = curl_error($curl);
        curl_close($curl);


        return [$response_body, $http_code, $response_header, $error];
    }
}
