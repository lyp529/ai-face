<?php


namespace common\utils;


use yii\helpers\StringHelper;

/**
 * Class ValueUtil 数值工具
 * @package common\utils
 */
class ValueUtil extends StringHelper {
    /**
     * 将数据转为 bool 类型
     * @param string|int|bool $data
     * @return bool
     */
    public static function toBool($data) {
        if (is_bool($data)) {
            return $data;
        } else if (is_numeric($data)) {
            return $data !== 0;
        } else if (is_string($data)) {
            return $data === "true";
        }

        return !empty($data);
    }

    /**
     * 在数字前面补0
     * @param int|float $num 数字
     * @param int $digits 需要补充多少位数字
     * @return int|string
     */
    public static function fillZero($num, $digits = 4) {
        for ($i = 1; $i <= $digits; ++$i) {
            if ($num < pow(10, $i - 1)) {
                $num = "0" . $num;
            }
        }

        return $num;
    }
}