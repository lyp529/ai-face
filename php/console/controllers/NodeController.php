<?php


namespace console\controllers;


use common\utils\FileUtil;
use yii\helpers\Console;
use yii\helpers\Json;

/**
 * Class NodeController nodejs控制器。负责处理node代码生成等工作
 * @package console\controllers
 */
class NodeController extends BaseConsoleController {
    /**
     * 有 vue 的模块
     * @var string[]
     */
    private $modules = ["backend", "frontend"];
    /**
     * 排除不被删除的文件
     * @var string[]
     */
    private $excludeFilenames = [".", "..", "assets", "index.php", "index-test.php"];

    /**
     * 初始化 nodejs 项目
     */
    public function actionInit() {
        // 使用阿里的镜像
        system("npm config set registry https://registry.npm.taobao.org");
        // 全局安装 vue-cli 客户端工具
        system("npm install -g @vue/cli@3.12.0");

        chdir(FileUtil::getPhpDir());
        system("php yii node/update-all");
    }

    /**
     * 更新项目依赖库（安装）
     */
    public function actionUpdate() {
        $modules = func_get_args();
        $this->validModule($modules);

        foreach ($modules as $module) {
            $moduleDir = FileUtil::getVueCliDir() . "/" . $module;
            chdir($moduleDir);
            system("npm install");
        }
    }

    /**
     * 更新所有项目
     */
    public function actionUpdateAll() {
        $argv = implode(" ", $this->modules);
        chdir(FileUtil::getPhpDir());
        system("php yii node/update " . $argv);
    }

    /**
     * 生成 nodejs 项目
     */
    public function actionBuild() {
        $modules = func_get_args();
        $this->validModule($modules);

        foreach ($modules as $module) {
            // 生成项目
            $moduleDir = FileUtil::getVueCliDir() . "/" . $module;
            chdir($moduleDir);
            system("npm run-script build");
            $this->removeFile($module);
            $this->copyFile($module);
            Console::output("模块：" . $module . "生成完成");
        }
    }

    /**
     * 生成所有项目
     */
    public function actionBuildAll() {
        $argv = implode(" ", $this->modules);
        chdir(FileUtil::getPhpDir());
        system("php yii node/build " . $argv);
    }

    /**
     * 移除web目录下文件（排除部分文件）
     * @param $module
     */
    private function removeFile($module) {
        $webDir = FileUtil::getPhpDir() . "/" . $module . "/web";
        $filenames = scandir($webDir);
        foreach ($filenames as $filename) {
            if (in_array($filename, $this->excludeFilenames)) {
                continue;
            }
            FileUtil::delete($webDir . "/" . $filename);
        }
    }

    /**
     * 复制文件到web目录
     * @param $module
     */
    private function copyFile($module) {
        $distDir = FileUtil::getVueCliDir() . "/" . $module . "/dist";
        $webDir = FileUtil::getPhpDir() . "/" . $module . "/web";
        $filenames = scandir($distDir);
        foreach ($filenames as $filename) {
            if (in_array($filename, $this->excludeFilenames)) {
                continue;
            }
            $distFilename = $distDir . "/" . $filename;
            $webFilename = $webDir . "/" . $filename;
            FileUtil::copy($distFilename, $webFilename);
        }
    }

    /**
     * 验证模块是否合法。验证不通过将终止运行
     * @param string[] $modules 模块
     * @return void
     */
    private function validModule($modules) {
        $isValid = true;
        if (empty($modules)) {
            $isValid = false;
        }
        foreach ($modules as $module) {
            if (!in_array($module, $this->modules)) {
                $isValid = false;
            }
        }

        if (!$isValid) {
            Console::output("非法参数：" . implode(" ", $modules));
            Console::output("可选合法参数：" . implode(" ", $this->modules));
            exit;
        }
    }
}