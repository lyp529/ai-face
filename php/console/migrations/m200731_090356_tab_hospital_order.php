<?php

use yii\db\Migration;

/**
 * Class m200731_090356_tab_hospital_order
 */
class m200731_090356_tab_hospital_order extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE TABLE `ai-face`.`tab_hospital_order`(  
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '订单ID',
  `member_id` INT(11) NOT NULL COMMENT '用户ID',
  `order_number` INT(11) NOT NULL COMMENT '订单号',
  `amount` DECIMAL(12,2) NOT NULL COMMENT '金额',
  `services` VARCHAR(500) NOT NULL COMMENT '服务项目',
  `hospital_id` INT(11) NOT NULL COMMENT '医院ID',
  `username` VARCHAR(100) NOT NULL COMMENT '姓名',
  `contact_phone` VARCHAR(100) NOT NULL COMMENT '联系电话',
  `appointment_time` INT(11) NOT NULL COMMENT '预约时间',
  `state` TINYINT(1) NOT NULL COMMENT '订单状态（1预约中、2预约成功、3预约失败）',
  `remark` text NULL COMMENT '备注',
  `creation_time` INT(11) NOT NULL COMMENT '创建时间',
  `update_time` INT(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=INNODB CHARSET=utf8mb4 COLLATE=utf8mb4_german2_ci
COMMENT='医院预约订单';
");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200731_090356_tab_hospital_order cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200731_090356_tab_hospital_order cannot be reverted.\n";

        return false;
    }
    */
}
