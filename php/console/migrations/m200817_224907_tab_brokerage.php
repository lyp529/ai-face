<?php

use yii\db\Migration;

/**
 * Class m200817_224907_tab_brokerage
 */
class m200817_224907_tab_brokerage extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE TABLE `ai-face`.`tab_brokerage`(  
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `member_id` INT(11) NOT NULL COMMENT '用户id',
  `icon` VARCHAR(200) COMMENT '头像',
  `username` VARCHAR(100) COMMENT '用户名',
  `amount` DECIMAL(12,2) NOT NULL COMMENT '金额',
  `br_type` TINYINT(1) NOT NULL COMMENT '变动类型（1入账佣金 2提现佣金）',
  `state` TINYINT(1) NOT NULL COMMENT '状态（1申请中、2完成、3失败）',
  `bank_number` VARCHAR(100) COMMENT '银行卡号',
  `bank_username` VARCHAR(100) COMMENT '银行卡姓名',
  `phone` VARCHAR(100) COMMENT '手机',
  `creation_time` INT(11) NOT NULL COMMENT '创建时间',
  `update_time` INT(11) COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=INNODB CHARSET=utf8mb4 COLLATE=utf8mb4_german2_ci
COMMENT='佣金表';
");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200817_224907_tab_brokerage cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200817_224907_tab_brokerage cannot be reverted.\n";

        return false;
    }
    */
}
