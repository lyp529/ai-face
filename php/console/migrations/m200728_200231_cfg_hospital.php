<?php

use yii\db\Migration;

/**
 * Class m200728_200231_cfg_hospital
 */
class m200728_200231_cfg_hospital extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE TABLE `ai-face`.`tab_hospital`(  
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '医院ID',
  `member_id` int(11) NOT NULL COMMENT '用户ID',
  `category` VARCHAR(500) NOT NULL COMMENT '医院服务类目',
  `name` varchar(100) COLLATE utf8mb4_german2_ci NOT NULL COMMENT '医院名称',
  `icon` VARCHAR(200) NOT NULL COMMENT '医院头像',
  `head_img` VARCHAR(200) NOT NULL COMMENT '头部图片',
  `contact_way` VARCHAR(100) NOT NULL COMMENT '联系方式',
  `address` VARCHAR(100) NOT NULL COMMENT '门店详细地址',
  `brief` TEXT NOT NULL COMMENT '门店简介',
  `address_x` DECIMAL(12,6) NOT NULL COMMENT '经度',
  `address_y` DECIMAL(12,6) NOT NULL COMMENT '纬度',
  `creation_time` INT(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=INNODB CHARSET=utf8mb4 COLLATE=utf8mb4_german2_ci
COMMENT='医院表';
");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200728_200231_cfg_hospital cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200728_200231_cfg_hospital cannot be reverted.\n";

        return false;
    }
    */
}
