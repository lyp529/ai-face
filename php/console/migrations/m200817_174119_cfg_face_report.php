<?php

use yii\db\Migration;

/**
 * Class m200817_174119_cfg_face_report
 */
class m200817_174119_cfg_face_report extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE TABLE `ai-face`.`cfg_face_report`(  
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '报告ID',
  `overview` TEXT COMMENT '面相概述',
  `eye_analysis` TEXT COMMENT '眼睛解析',
  `mouth_analysis` TEXT COMMENT '嘴相解析',
  `nose_analysis` TEXT COMMENT '鼻相解析',
  `eye_top` int(11) DEFAULT NULL COMMENT '眼睛上面的距离',
  `eye_bottom` int(11) DEFAULT NULL COMMENT '眼睛下面的距离',
  `mouth_top` int(11) DEFAULT NULL COMMENT '嘴巴上面的距离',
  `mouth_bottom` int(11) DEFAULT NULL COMMENT '嘴巴下面的距离',
  `nose_top` int(11) DEFAULT NULL COMMENT '鼻子上面的距离',
  `nose_bottom` int(11) DEFAULT NULL COMMENT '鼻子下面的距离',
  PRIMARY KEY (`id`)
) ENGINE=MYISAM CHARSET=utf8mb4 COLLATE=utf8mb4_german2_ci
COMMENT='面相报告表';
");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200817_174119_cfg_face_report cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200817_174119_cfg_face_report cannot be reverted.\n";

        return false;
    }
    */
}
