<?php

use yii\db\Migration;

/**
 * Class m200728_194908_cfg_hospital_case
 */
class m200728_194908_cfg_hospital_case extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE TABLE `ai-face`.`cfg_hospital_case`(  
  `h_id` INT(11) NOT NULL COMMENT '医院ID',
  `sort` TINYINT(1) NOT NULL COMMENT '排序',
  `img_url` VARCHAR(200) NOT NULL COMMENT '图片路径'
) ENGINE=INNODB CHARSET=utf8mb4 COLLATE=utf8mb4_german2_ci
COMMENT='医院成功案例表';");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200728_194908_cfg_hospital_case cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200728_194908_cfg_hospital_case cannot be reverted.\n";

        return false;
    }
    */
}
