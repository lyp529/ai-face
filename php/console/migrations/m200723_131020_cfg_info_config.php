<?php

use yii\db\Migration;

/**
 * Class m200723_131020_cfg_info_config
 */
class m200723_131020_cfg_info_config extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE TABLE `ai-face`.`cfg_info_config`(  
  `cfg_code` VARCHAR(100) NOT NULL COMMENT '配置代码',
  `cfg_name` varchar(100) NOT NULL COMMENT '配置名',
  `cfg_value` INT(11) DEFAULT 0 COMMENT '所需值',
  `cfg_content` TEXT COMMENT '多文字使用（例如首页AI文案）',
  PRIMARY KEY (`cfg_code`)
) ENGINE=INNODB CHARSET=utf8mb4 COLLATE=utf8mb4_german2_ci
COMMENT='配置表';
");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200723_131020_cfg_info_config cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200723_131020_cfg_info_config cannot be reverted.\n";

        return false;
    }
    */
}
