<?php

use yii\db\Migration;

/**
 * Class m200721_133135_cfg_member_relation
 */
class m200721_133135_cfg_member_relation extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE TABLE `cfg_member_relation` (
  `superior_id` int(11) NOT NULL COMMENT '上级ID',
  `member_id` int(11) NOT NULL COMMENT '用户ID',
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_german2_ci COMMENT='用户关系表'
;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200721_133135_cfg_member_relation cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200721_133135_cfg_member_relation cannot be reverted.\n";

        return false;
    }
    */
}
