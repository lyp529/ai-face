<?php

use yii\db\Migration;

/**
 * Class m200729_082536_tab_agent
 */
class m200729_082536_tab_agent extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE TABLE `tab_agent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL COMMENT '用户ID',
  `username` varchar(100) COLLATE utf8mb4_german2_ci NOT NULL COMMENT '姓名',
  `contact_phone` int(11) NOT NULL COMMENT '联系电话',
  `state` tinyint(1) DEFAULT 1 COMMENT '状态',
  `creation_time` int(11) NOT NULL COMMENT '创建时间',
  `update_time` int(11) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_german2_ci COMMENT='代理商（申请）表';
");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200729_082536_tab_agent cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200729_082536_tab_agent cannot be reverted.\n";

        return false;
    }
    */
}
