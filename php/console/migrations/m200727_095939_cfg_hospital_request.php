<?php

use yii\db\Migration;

/**
 * Class m200727_095939_cfg_hospital_request
 */
class m200727_095939_cfg_hospital_request extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE TABLE `cfg_hospital_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL COMMENT '用户ID',
  `logo_url` varchar(200) COLLATE utf8mb4_german2_ci NOT NULL COMMENT '医院logo-url',
  `name` varchar(100) COLLATE utf8mb4_german2_ci NOT NULL COMMENT '医院名称',
  `address` varchar(100) COLLATE utf8mb4_german2_ci NOT NULL COMMENT '详细地址',
  `contact_person` varchar(100) COLLATE utf8mb4_german2_ci NOT NULL COMMENT '联系人',
  `contact_way` varchar(100) COLLATE utf8mb4_german2_ci NOT NULL COMMENT '联系方式（手机、座机）',
  `business_license` varchar(200) COLLATE utf8mb4_german2_ci NOT NULL COMMENT '营业执照url',
  `id_card_f` varchar(200) COLLATE utf8mb4_german2_ci NOT NULL COMMENT '法人正面身份证url',
  `id_card_r` varchar(200) COLLATE utf8mb4_german2_ci NOT NULL COMMENT '法人反面身份证url',
  `state` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态（1审核中、2审核成功、3审核失败）',
  `creation_time` int(11) NOT NULL COMMENT '创建时间',
  `update_time` int(11) NOT NULL COMMENT '审核时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_german2_ci COMMENT='医院申请表';");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200727_095939_cfg_hospital_request cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200727_095939_cfg_hospital_request cannot be reverted.\n";

        return false;
    }
    */
}
