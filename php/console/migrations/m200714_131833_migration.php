<?php

use yii\db\Migration;

/**
 * Class m200714_131833_migration
 */
class m200714_131833_migration extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) COLLATE utf8mb4_german2_ci NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_german2_ci");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200714_131833_migration cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200714_131833_migration cannot be reverted.\n";

        return false;
    }
    */
}
