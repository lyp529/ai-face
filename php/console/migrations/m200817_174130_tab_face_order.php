<?php

use yii\db\Migration;

/**
 * Class m200817_174130_tab_face_order
 */
class m200817_174130_tab_face_order extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE TABLE `tab_face_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '订单ID',
  `member_id` int(11) NOT NULL COMMENT '用户ID',
  `re_id` int(11) NOT NULL COMMENT '报告ID',
  `order_number` int(11) NOT NULL COMMENT '订单号',
  `amount` decimal(12,2) NOT NULL COMMENT '金额',
  `url` varchar(200) COLLATE utf8mb4_german2_ci NOT NULL COMMENT '图片url',
  `state` tinyint(1) NOT NULL DEFAULT 1 COMMENT '订单状态（1未付款、2付款成功、3付款失败）',
  `gender` tinyint(1) NOT NULL COMMENT '1女性，2男性',
  `age` tinyint(2) NOT NULL COMMENT '年龄',
  `remark` text COLLATE utf8mb4_german2_ci DEFAULT NULL COMMENT '备注',
  `creation_time` int(11) NOT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_german2_ci COMMENT='面相订单表'
;
");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200817_174130_tab_face_order cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200817_174130_tab_face_order cannot be reverted.\n";

        return false;
    }
    */
}
