<?php

use yii\db\Migration;

/**
 * Class m200727_094945_user_admin
 */
class m200727_094945_user_admin extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE TABLE `user_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '管理员ID',
  `username` varchar(100) COLLATE utf8mb4_german2_ci NOT NULL COMMENT '管理员登录名',
  `password` varchar(200) COLLATE utf8mb4_german2_ci NOT NULL COMMENT '密码',
  `login_time` int(11) DEFAULT NULL COMMENT '最近登录时间',
  `login_ip` varchar(100) COLLATE utf8mb4_german2_ci DEFAULT NULL COMMENT '最近登录ip',
  `last_login_time` int(11) DEFAULT NULL COMMENT '上次登录时间',
  `last_login_ip` varchar(100) COLLATE utf8mb4_german2_ci DEFAULT NULL COMMENT '上次登录IP',
  `session_dead_time` int(11) DEFAULT NULL COMMENT 'session有效截止时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_german2_ci COMMENT='管理员表';
");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200727_094945_user_admin cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200727_094945_user_admin cannot be reverted.\n";

        return false;
    }
    */
}
