<?php

use yii\db\Migration;

/**
 * Class m200721_130706_user_member
 */
class m200721_130706_user_member extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE TABLE `user_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `brokerage` decimal(12,2) DEFAULT 0.00 COMMENT '佣金',
  `referrer_id` int(11) DEFAULT 0 COMMENT '推荐人',
  `icon` varchar(200) COLLATE utf8mb4_german2_ci DEFAULT NULL COMMENT '头像',
  `username` varchar(100) COLLATE utf8mb4_german2_ci DEFAULT NULL COMMENT '微信名',
  `token` int(10) NOT NULL,
  `openid` char(50) COLLATE utf8mb4_german2_ci NOT NULL COMMENT '授权用户唯一标识',
  `is_hospital` tinyint(1) DEFAULT 2 COMMENT '医院账户（1 是，2 否）',
  `is_agent` tinyint(1) DEFAULT 2 COMMENT '代理商账户（1 是，2 否）',
  `creation_time` int(11) NOT NULL COMMENT '创建时间',
  `token_time` int(11) NOT NULL COMMENT 'token过期时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `token` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_german2_ci COMMENT='用户表';");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200721_130706_user_member cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200721_130706_user_member cannot be reverted.\n";

        return false;
    }
    */
}
