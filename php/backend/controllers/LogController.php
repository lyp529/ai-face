<?php

namespace backend\controllers;

use common\services\SM;


/**
 * Class LogController  日志控制器
 * @package backend\LogController
 */
class LogController extends BaseBackendController
{
    /**
     * 登录日志
     * @return string
     */
    public function actionGetLoginLog()
    {
        $page = $this->param("page", 1);
        $size = $this->param("size", 10);
        $endTime = $this->param("endTime");
        $startTime = $this->param("startTime");
        $username = $this->param("username");
        return $this->done(SM::getLogService()->getLoginLog($startTime, $endTime, ($page - 1) * $size, $size, $username));
    }
}
