<?php


namespace backend\controllers;


use Yii;
use yii\web\Controller;

/**
 * Class BaseController 控制器基类
 * @package code\controllers
 */
class BaseController extends Controller {
    /**
     * 错误处理
     */
    public function actionError() {
        $exception = Yii::$app->errorHandler->exception;
        if ($exception !== null) {
            Yii::$app->response->statusCode = 200;
            return $exception->getMessage() . ' ' . $exception->getTraceAsString();
        }

        return "404";
    }
}