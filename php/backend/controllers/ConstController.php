<?php

namespace backend\controllers;
use common\constants\C;

/**
 * Class OptionsController 常量、配置
 * @package backend\controllers
 */
class ConstController extends BaseBackendController
{

    /**
     * 获取系统通用的常量列表
     */
    public function actionGet() {
        return $this->done([

        ]);
    }
}
