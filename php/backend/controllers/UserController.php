<?php

namespace backend\controllers;


use common\constants\C;
use common\exceptions\ForeseeableException;
use common\exceptions\SystemException;
use common\models\db\InfoConfig;
use common\models\vo\user\InfoVo;
use Throwable;
use common\services\SM;
use yii\db\StaleObjectException;


/**
 * Class UserController 用户控制器
 * @package backend\BaseBackendController
 */
class UserController extends BaseBackendController
{
    /**
     * 登录系统
     * @return string
     * @throws ForeseeableException
     * @throws SystemException
     * @throws Throwable
     */
    public function actionLogin()
    {
        $username = $this->param('username');
        $password = $this->param('password');
        $admin = SM::getUserService()->adminLogin($username, $password);
        return $this->done(["userId" => $admin->id]);
    }

    /**
     * 退出登录
     * @throws Throwable
     */
    public function actionLogout()
    {
        $this->destroySession();
        return $this->done();
    }

    /**
     * 获取管理员基础数据
     */
    public function actionGetUserInfoVo()
    {
        $userInfoVo = new InfoVo();
        $userInfoVo->initByUserModel(SM::getUserService()->getAdminId());
        return $this->done(["userInfoVo" => $userInfoVo]);
    }
    /**
     * 会员管理获取会员列表
     * @return string
     */
    public function actionGetUserList() {
        $page = $this->param("page",0);
        $rows = $this->param("size",10);
        $username = $this->param("username");
        return $this->done(SM::getUserService()->getUserList(($page - 1) * $rows,$rows,$username));
    }
    /**
     * 获取管理员列表
     * @return string
     */
    public function actionGetAdminList() {
        $page = $this->param("page");
        $rows = $this->param("size");
        return $this->done(SM::getUserService()->getAdminList(($page - 1) * $rows,$rows));
    }

    /**
     * 获取医院列表
     * @return string
     */
    public function actionAdminGetHospitalList() {
        $page = $this->param("page");
        $rows = $this->param("size");
        $name = $this->param("name");
        return $this->done(SM::getUserService()->adminGetHospitalList(($page - 1) * $rows,$rows,$name));
    }

    /**
     * 删除医院
     * @return string
     * @throws ForeseeableException
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function actionDelHospital() {
        $id = $this->param("id");
        $this->beginTransaction();
        return $this->done(SM::getUserService()->delHospital($id));
    }
    /**
     * 获取医院申请列表
     * @return string
     */
    public function actionGetHospitalRequestList() {
        $page = $this->param("page");
        $rows = $this->param("size");
        $state = $this->param("state");
        return $this->done(SM::getUserService()->getHospitalRequestList(($page - 1) * $rows,$rows,$state));
    }

    /**
     * 更新医院申请
     * @return string
     * @throws ForeseeableException
     * @throws SystemException
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionUpdateHospitalRequest() {
        $id = $this->param("id");
        $state = $this->param("state");
        return $this->done(SM::getUserService()->updateHospitalRequest($id, $state));
    }

    /**
     * 获取医院预约订单
     * @return string
     */
    public function actionGetHospitalOrderList() {
        $page = $this->param("page");
        $rows = $this->param("size");
        $state = $this->param("state");
        return $this->done(SM::getUserService()->getHospitalOrderList(($page - 1) * $rows,$rows,$state));
    }
    /**
     * 更新医院预约订单
     * @return string
     * @throws ForeseeableException
     * @throws SystemException
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionUpdateHospitalOrder() {
        $id = $this->param("id");
        $state = $this->param("state");
        return $this->done(SM::getUserService()->updateHospitalOrder($id, $state));
    }

    /**
     * 代理商表
     * @return string
     */
    public function actionGetAgentList() {
        $page = $this->param("page");
        $rows = $this->param("size");
        $state = $this->param("state");
        $username = $this->param("username");
        return $this->done(SM::getUserService()->getAgentList(($page - 1) * $rows, $rows, $state,$username));
    }

    /**
     * 更新代理商
     * @return string
     * @throws ForeseeableException
     * @throws StaleObjectException
     * @throws SystemException
     * @throws Throwable
     */
    public function actionUpdateAgent() {
        $id = $this->param("id");
        $state = $this->param("state");
        return $this->done(SM::getUserService()->updateAgent($id, $state));
    }

    /**
     * 获取配置表列
     * @return string
     */
    public function actionGetInfoConfigList() {
        return $this->done((new InfoConfig())->getInfoConfigList());
    }

    /**
     * 更新配置
     * @return string
     * @throws ForeseeableException
     * @throws StaleObjectException
     * @throws SystemException
     * @throws Throwable
     */
    public function actionUpdateInfoConfig() {
        $cfgCode = $this->param("cfg_code");
        $cfgValue = $this->param("cfg_value");
        $cfgContent = $this->param("cfg_content");
        return $this->done(SM::getUserService()->updateInfoConfig($cfgCode, $cfgValue, $cfgContent));
    }

    /**
     * 面相订单表
     * @return string
     */
    public function actionGetFaceOrderList() {
        $page = $this->param("page");
        $rows = $this->param("size");
        $state = $this->param("state");
        return $this->done(SM::getOrderService()->getFaceOrderList(($page - 1) * $rows, $rows, $state));
    }

    /**
     * 更新面相数据
     * @return string
     * @throws ForeseeableException
     * @throws StaleObjectException
     * @throws SystemException
     * @throws Throwable
     */
    public function actionUpdateFaceReport() {
        $id = $this->param("id");
        $overview = $this->param("overview");
        $eye = $this->param("eye_analysis");
        $mouth = $this->param("mouth_analysis");
        $nose = $this->param("nose_analysis");
        return $this->done(SM::getOrderService()->updateFaceReport($id, $overview, $eye, $mouth, $nose));
    }

    /**
     * 获取佣金列
     * @return string
     */
    public function actionGetBrokerageList() {
        $page = $this->param("page");
        $rows = $this->param("size");
        $state = $this->param("state");
        $brType = $this->param("br_type");
        return $this->done(SM::getOrderService()->getBrokerageList(($page - 1) * $rows, $rows, $brType, $state));
    }

    /**
     * 更新提现申请
     * @return string
     * @throws ForeseeableException
     * @throws StaleObjectException
     * @throws SystemException
     * @throws Throwable
     */
    public function actionUpdateWithdraw() {
        $id = $this->param("id");
        $state = $this->param("state");
        return $this->done(SM::getOrderService()->updateWithdraw($id, $state));
    }
}
