<?php


namespace backend\models\form;


use common\base\BaseForm;
use common\models\UserModel;

/**
 * Class UserForm 新增、修改用户表单的模型
 * @package models\form
 */
class UserForm extends BaseForm {
    /**
     * @var int 用户id
     */
    public $id;
    /**
     * @var int 用户类型
     */
    public $userType;
    /**
     * @var string 登录账号
     */
    public $username;
    /**
     * @var string 登录密码 sha512 后的值
     */
    public $password;
    /**
     * @var string 用户名
     */
    public $name;
    /**
     * @var string 备注
     */
    public $remark;


    /**
     * @return array
     */
    public function rules() {
        return array_merge(parent::rules(), [
            [["username", "name", "userType"], "required"],
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels() {
        return [
            "id" => "ID",
            "userType" => "用户类型",
            "username" => "登录账号",
            "password" => "登录密码",
            "name" => "用户名"
        ];
    }

    /**
     * @param array $values
     * @param bool $safeOnly
     */
    public function setAttributes($values, $safeOnly = true) {
        parent::setAttributes($values, $safeOnly);
        $this->id = (int)$this->id;
        $this->userType = (int)$this->userType;
    }
}
