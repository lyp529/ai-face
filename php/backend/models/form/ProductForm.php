<?php


namespace backend\models\form;


use common\base\BaseForm;
use common\constants\C;
use common\models\db\Img;

/**
 * Class ProductForm 产品的模型
 * @package models\form
 */
class ProductForm extends BaseForm {
    /**
     * @var int id
     */
    public $id;
    /**
     * @var int 类型
     */
    public $type;
    /**
     * @var string 类型 labels
     */
    public $typeLabels;
    /**
     * @var string 标题
     */
    public $title;
    /**
     * @var int 排序
     */
    public $img_sort;
    /**
     * @var string 图片路径
     */
    public $img_src;
    /**
     * @var string 链接
     */
    public $img_link;
    /**
     * @var string 图片内容
     */
    public $content;
    /**
     * @var int 0显示 1不显示
     */
    public $state;


    /**
     * @param Img $list
     */
    public function initByProduct(Img $list) {
        $this->id = $list->id;
        $this->type = $list->type;
        $this->typeLabels = C::getImgTypeLabel($list->type);
        $this->title = $list->title;
        $this->img_sort = $list->img_sort;
        $this->img_src = $list->img_src;
        $this->img_link = $list->img_link;
        $this->content = $list->content;
        $this->state = (string)$list->state;
    }
}
