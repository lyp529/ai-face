import { UserStore } from '@/store/modules/UserStore';
export const permission = {
    inserted(el, binding) {
        const { value } = binding;
        const roles = UserStore.roles;
        if (value && value instanceof Array && value.length > 0) {
            const permissionRoles = value;
            const hasPermission = roles.some(role => {
                return permissionRoles.includes(role);
            });
            if (!hasPermission) {
                el.parentNode && el.parentNode.removeChild(el);
            }
        }
        else {
            throw new Error(`need roles! Like v-permission="['admin','editor']"`);
        }
    }
};
//# sourceMappingURL=index.js.map