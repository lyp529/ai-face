// You can customize below settings :)
const settings = {
    title: '后台管理',
    showSettings: true,
    showTagsView: true,
    fixedHeader: false,
    showSidebarLogo: false,
    errorLog: ['production'],
    sidebarTextTheme: true,
    devServerPort: 9527,
    mockServerPort: 9528
};
export default settings;
//# sourceMappingURL=settings.js.map
