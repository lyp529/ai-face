import * as tslib_1 from "tslib";
import { Component, Vue, Watch } from 'vue-property-decorator';
import { UserStore } from '@/store/modules/UserStore';
import LangSelect from '@/components/LangSelect/index.vue';
import SocialSign from './components/SocialSignin.vue';
import UserApi from "@/api/UserApi";
import EncryptUtil from "@/utils/EncryptUtil";
import SIdentify from './components/SIdentify.vue';
let default_1 = class default_1 extends Vue {
    constructor() {
        super(...arguments);
        this.validateAccount = (rule, value, callback) => {
            callback();
            //if (value.length < 3 || value.length > 100 || value.search(/^[a-zA-Z|_][0-9a-zA-Z|_]+/) === -1) {
            //    callback(new Error('Please enter the correct user name'))
            //} else {
            //    callback()
            //}
        };
        this.validatePassword = (rule, value, callback) => {
            callback();
            //if (value.length < 6) {
            //    callback(new Error('The password can not be less than 6 digits'))
            //} else {
            //    callback()
            //}
        };
        this.validateEnsurePassword = (rule, value, callback) => {
            if (!this.isInitAdmin && value !== this.loginForm.password) {
                callback(new Error('two passwords not same!'));
            }
            else {
                callback();
            }
        };
        this.loginForm = {
            username: '',
            password: '',
            ensurePassword: '',
            // verifyCode:'',
            codeNum: ''
        };
        this.loginRules = {
            username: [{ validator: this.validateAccount, trigger: 'blur' }],
            password: [{ validator: this.validatePassword, trigger: 'blur' }],
            ensurePassword: [{ validator: this.validateEnsurePassword, trigger: 'blur' }],
        };
        this.passwordType = 'password';
        this.loading = false;
        this.showDialog = false;
        this.otherQuery = {};
        this.identifyCode = '0';
        /**
         * 是否初始化了超级管理员
         */
        this.isInitAdmin = true;
    }
    validateVerifyCode(rule, value, callback) {
        if (value === '') {
            callback(new Error('请输入验证码'));
        }
        else {
            callback();
        }
    }
    /**
     * 对表单数据进行修改，以适应后台的数据
     */
    get loginFormParams() {
        let username = this.loginForm.username.trim();
        let password = this.loginForm.password.trim();
        // let verifyCode = this.loginForm.verifyCode.trim()
        return {
            username: username,
            password: EncryptUtil.sha256(password)
            // verifyCode:verifyCode
        };
    }
    onRouteChange(route) {
        // TODO: remove the "as Dictionary<string>" hack after v4 release for vue-router
        // See https://github.com/vuejs/vue-router/pull/2050 for details
        const query = route.query;
        if (query) {
            this.redirect = query.redirect;
            this.otherQuery = this.getOtherQuery(query);
        }
    }
    mounted() {
        if (this.loginForm.username === '') {
            this.$refs.username.focus();
        }
        else if (this.loginForm.password === '') {
            this.$refs.password.focus();
        }
        else if (this.loginForm.ensurePassword === '') {
            this.$refs.ensurePassword.focus();
        }
        // else if (this.loginForm.verifyCode === '') {
        //     (this.$refs.verifyCode as Input).focus()
        // }
        this.makeCode();
        this.requestIsInitAdmin();
    }
    /**
     * 请求是否创建了超级管理员
     */
    async requestIsInitAdmin() {
        this.isInitAdmin = await UserApi.isInitAdmin();
    }
    showPwd() {
        if (this.passwordType === 'password') {
            this.passwordType = '';
        }
        else {
            this.passwordType = 'password';
        }
        this.$nextTick(() => {
            this.$refs.password.focus();
        });
    }
    handleLogin() {
        this.$refs.loginForm.validate(async (valid) => {
            if (valid) {
                this.loading = true;
                if (this.isInitAdmin) {
                    // 进行登录
                    await UserStore.Login(this.loginFormParams);
                    this.$router.push({
                        path: this.redirect || '/',
                        query: this.otherQuery
                    });
                }
                else {
                    // 初始化系统管理员
                    let isSuccess = await UserApi.initAdmin(this.loginFormParams);
                    if (isSuccess) {
                        this.isInitAdmin = true;
                    }
                }
                this.loading = false;
            }
            else {
                return false;
            }
        });
    }
    getOtherQuery(query) {
        return Object.keys(query).reduce((acc, cur) => {
            if (cur !== 'redirect') {
                acc[cur] = query[cur];
            }
            return acc;
        }, {});
    }
    // 生成随机数
    randomNum(min, max) {
        return Math.floor(Math.random() * (max - min) + min);
    }
    // 生成四位随机验证码
    async makeCode() {
        this.identifyCode = await UserApi.getCode();
        console.log(this.identifyCode);
    }
    // 切换验证码
    refreshCode() {
        this.makeCode();
    }
};
tslib_1.__decorate([
    Watch('$route', { immediate: true })
], default_1.prototype, "onRouteChange", null);
default_1 = tslib_1.__decorate([
    Component({
        name: 'Login',
        components: {
            LangSelect,
            SocialSign,
            SIdentify
        }
    })
], default_1);
export default default_1;
//# sourceMappingURL=index.vue.js.map