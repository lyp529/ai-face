const req = require.context('../../icons/svg', false, /\.svg$/);
const re = /\.\/(.*)\.svg/;
const requireAll = (requireContext) => requireContext.keys();
const icons = requireAll(req).map((str) => {
    return str.match(re)[1];
});
export default icons;
//# sourceMappingURL=svg-icons.js.map