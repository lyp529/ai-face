import * as tslib_1 from "tslib";
import { VuexModule, Module, Mutation, Action, getModule } from 'vuex-module-decorators';
import store from '@/store';
let TagsView = class TagsView extends VuexModule {
    constructor() {
        super(...arguments);
        this.visitedViews = [];
        this.cachedViews = [];
    }
    ADD_VISITED_VIEW(view) {
        if (this.visitedViews.some(v => v.path === view.path))
            return;
        this.visitedViews.push(Object.assign({}, view, {
            title: view.meta.title || 'no-name'
        }));
    }
    ADD_CACHED_VIEW(view) {
        if (this.cachedViews.includes(view.name))
            return;
        if (!view.meta.noCache) {
            this.cachedViews.push(view.name);
        }
    }
    DEL_VISITED_VIEW(view) {
        for (const [i, v] of this.visitedViews.entries()) {
            if (v.path === view.path) {
                this.visitedViews.splice(i, 1);
                break;
            }
        }
    }
    DEL_CACHED_VIEW(view) {
        for (const [i, v] of this.cachedViews.entries()) {
            if (v === view.name) {
                this.cachedViews.splice(i, 1);
                break;
            }
        }
    }
    DEL_OTHERS_VISITED_VIEWS(view) {
        this.visitedViews = this.visitedViews.filter(v => {
            return v.meta.affix || v.path === view.path;
        });
    }
    DEL_OTHERS_CACHED_VIEWS(view) {
        for (const [i, v] of this.cachedViews.entries()) {
            if (v === view.name) {
                this.cachedViews = this.cachedViews.slice(i, i + 1);
                break;
            }
        }
    }
    DEL_ALL_VISITED_VIEWS() {
        // keep affix tags
        const affixTags = this.visitedViews.filter(tag => tag.meta.affix);
        this.visitedViews = affixTags;
    }
    DEL_ALL_CACHED_VIEWS() {
        this.cachedViews = [];
    }
    UPDATE_VISITED_VIEW(view) {
        for (let v of this.visitedViews) {
            if (v.path === view.path) {
                v = Object.assign(v, view);
                break;
            }
        }
    }
    addView(view) {
        this.ADD_VISITED_VIEW(view);
        this.ADD_CACHED_VIEW(view);
    }
    addVisitedView(view) {
        this.ADD_VISITED_VIEW(view);
    }
    delView(view) {
        this.DEL_VISITED_VIEW(view);
        this.DEL_CACHED_VIEW(view);
    }
    delCachedView(view) {
        this.DEL_CACHED_VIEW(view);
    }
    delOthersViews(view) {
        this.DEL_OTHERS_VISITED_VIEWS(view);
        this.DEL_OTHERS_CACHED_VIEWS(view);
    }
    delAllViews() {
        this.DEL_ALL_VISITED_VIEWS();
        this.DEL_ALL_CACHED_VIEWS();
    }
    delAllCachedViews() {
        this.DEL_ALL_CACHED_VIEWS();
    }
    updateVisitedView(view) {
        this.UPDATE_VISITED_VIEW(view);
    }
};
tslib_1.__decorate([
    Mutation
], TagsView.prototype, "ADD_VISITED_VIEW", null);
tslib_1.__decorate([
    Mutation
], TagsView.prototype, "ADD_CACHED_VIEW", null);
tslib_1.__decorate([
    Mutation
], TagsView.prototype, "DEL_VISITED_VIEW", null);
tslib_1.__decorate([
    Mutation
], TagsView.prototype, "DEL_CACHED_VIEW", null);
tslib_1.__decorate([
    Mutation
], TagsView.prototype, "DEL_OTHERS_VISITED_VIEWS", null);
tslib_1.__decorate([
    Mutation
], TagsView.prototype, "DEL_OTHERS_CACHED_VIEWS", null);
tslib_1.__decorate([
    Mutation
], TagsView.prototype, "DEL_ALL_VISITED_VIEWS", null);
tslib_1.__decorate([
    Mutation
], TagsView.prototype, "DEL_ALL_CACHED_VIEWS", null);
tslib_1.__decorate([
    Mutation
], TagsView.prototype, "UPDATE_VISITED_VIEW", null);
tslib_1.__decorate([
    Action
], TagsView.prototype, "addView", null);
tslib_1.__decorate([
    Action
], TagsView.prototype, "addVisitedView", null);
tslib_1.__decorate([
    Action
], TagsView.prototype, "delView", null);
tslib_1.__decorate([
    Action
], TagsView.prototype, "delCachedView", null);
tslib_1.__decorate([
    Action
], TagsView.prototype, "delOthersViews", null);
tslib_1.__decorate([
    Action
], TagsView.prototype, "delAllViews", null);
tslib_1.__decorate([
    Action
], TagsView.prototype, "delAllCachedViews", null);
tslib_1.__decorate([
    Action
], TagsView.prototype, "updateVisitedView", null);
TagsView = tslib_1.__decorate([
    Module({ dynamic: true, store, name: 'tagsView' })
], TagsView);
export const TagsViewModule = getModule(TagsView);
//# sourceMappingURL=tags-view.js.map