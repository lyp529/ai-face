import * as tslib_1 from "tslib";
import { VuexModule, Module, Action, Mutation, getModule } from 'vuex-module-decorators';
import { getToken, setToken, removeToken } from '@/utils/cookies';
import router, { resetRouter } from '@/router';
import { PermissionModule } from './permission';
import { TagsViewModule } from './tags-view';
import store from '@/store';
import UserApi from "@/api/UserApi";
import { Message } from "element-ui";
import WindowUtil from "@/utils/WindowUtil";
import OptVo from "@/models/vos/OptVo";
import { ConstStore } from "@/store/modules/ConstStore";
import C from "@/constants/C";
import HomeVo from "@/models/vos/HomeVo";
let userStore = class userStore extends VuexModule {
    constructor() {
        super(...arguments);
        this.token = getToken() || '';
        this.name = '';
        this.avatar = '';
        this.introduction = '';
        this.roles = [];
        this.email = '';
        this.isRemoveVirtual = 0;
        /**
         * 用户id
         */
        this.id = 0;
        /**
         * 用户选项数据
         */
        this.opts = {};
        /**
         * 首页数据
         */
        this.homeVo = new HomeVo();
    }
    /**
     * 经过筛选的用户选项数据（只能获取比自己低级或同级的用户）
     */
    get filterUserTypeOpts() {
        let userTypeOpts = ConstStore.userTypeOpts;
        let retUserTypeOpts = [];
        for (let i in userTypeOpts) {
            let optVo = userTypeOpts[i];
            if (optVo.value < this.userType && optVo.value !== C.USER_TYPE_ADMIN) {
                retUserTypeOpts.push(optVo);
            }
        }
        return retUserTypeOpts;
    }
    /**
     * 是否是管理员
     */
    get isAdmin() {
        return this.userType === C.USER_TYPE_ADMIN;
    }
    /**
     * 获取用户id
     */
    get userId() {
        return this.id;
    }
    /**
     * 判断当前是否登录
     */
    get isLogin() {
        return this.token !== '';
    }
    SET_TOKEN(token) {
        this.token = token;
    }
    SET_NAME(name) {
        this.name = name;
    }
    SET_AVATAR(avatar) {
        this.avatar = avatar;
    }
    SET_INTRODUCTION(introduction) {
        this.introduction = introduction;
    }
    SET_ROLES(roles) {
        this.roles = roles;
    }
    SET_EMAIL(email) {
        this.email = email;
    }
    SET_ID(id) {
        this.id = id;
    }
    SET_REMOVE_STATE(isRemoveVirtual) {
        this.isRemoveVirtual = isRemoveVirtual;
    }
    /**
     * 设置选项数据
     * @param opts
     */
    setOpts(opts) {
        this.opts = opts;
    }
    /**
     * 设置首页数据
     * @param homeVo
     */
    setHomeVo(homeVo) {
        this.homeVo.load(homeVo);
    }
    /**
     * 启动检测登录的循环
     */
    startCheckLoginLoop() {
        if (this.checkLoginLoopTimer !== undefined) {
            clearInterval(this.checkLoginLoopTimer);
        }
        this.checkLoginLoopTimer = setInterval(async () => {
            if (!UserStore.isLogin) {
                return;
            }
            await UserApi.checkLogin();
        }, 10000);
    }
    /**
     * 停止检测登录的循环
     */
    stopCheckLoginLoop() {
        if (this.checkLoginLoopTimer !== undefined) {
            clearInterval(this.checkLoginLoopTimer);
        }
    }
    async Login(userInfo) {
        const vo = await UserApi.login(userInfo);
        if (!vo.isDone()) {
            Message.error(vo.message);
            return;
        }
        setToken(vo.data("userId"));
        this.SET_TOKEN(vo.data("userId"));
    }
    ResetToken() {
        removeToken();
        this.SET_TOKEN('');
        this.SET_ROLES([]);
    }
    async GetUserInfo() {
        if (this.token === '') {
            throw Error('GetUserInfo: token is undefined!');
        }
        const userInfoVo = await UserApi.getGetUserInfoVo();
        if (userInfoVo === null) {
            WindowUtil.messageError("获取用户信息失败");
            this.ResetToken();
            return;
        }
        // roles must be a non-empty array
        if (!userInfoVo.roles || userInfoVo.roles.length <= 0) {
            throw Error('GetUserInfo: roles must be a non-null array!');
        }
        // 设置用户数据
        this.SET_ROLES(userInfoVo.roles);
        this.SET_NAME(userInfoVo.name);
        this.SET_AVATAR(userInfoVo.avatar);
        this.SET_INTRODUCTION('');
        this.SET_EMAIL('');
        this.SET_ID(userInfoVo.id);
        this.SET_REMOVE_STATE(userInfoVo.isRemoveVirtual);
        this.setHomeVo(userInfoVo.homeVo);
    }
    async ChangeRoles(role) {
        // Dynamically modify permissions
        const token = role + '-token';
        this.SET_TOKEN(token);
        setToken(token);
        await this.GetUserInfo();
        resetRouter();
        // Generate dynamic accessible routes based on roles
        PermissionModule.GenerateRoutes(this.roles);
        // Add generated routes
        router.addRoutes(PermissionModule.dynamicRoutes);
        // Reset visited views and cached views
        TagsViewModule.delAllViews();
    }
    async LogOut() {
        if (this.token === '') {
            throw Error('LogOut: token is undefined!');
        }
        removeToken();
        resetRouter();
        this.SET_TOKEN('');
        this.SET_ROLES([]);
        this.stopCheckLoginLoop(); //移到上面来 by lizr
        let isLogoutDone = await UserApi.logout();
        if (isLogoutDone) {
            // this.stopCheckLoginLoop();
            await router.push(`/login?redirect=${router.currentRoute.fullPath}`);
        }
    }
};
tslib_1.__decorate([
    Mutation
], userStore.prototype, "SET_TOKEN", null);
tslib_1.__decorate([
    Mutation
], userStore.prototype, "SET_NAME", null);
tslib_1.__decorate([
    Mutation
], userStore.prototype, "SET_AVATAR", null);
tslib_1.__decorate([
    Mutation
], userStore.prototype, "SET_INTRODUCTION", null);
tslib_1.__decorate([
    Mutation
], userStore.prototype, "SET_ROLES", null);
tslib_1.__decorate([
    Mutation
], userStore.prototype, "SET_EMAIL", null);
tslib_1.__decorate([
    Mutation
], userStore.prototype, "SET_ID", null);
tslib_1.__decorate([
    Mutation
], userStore.prototype, "SET_REMOVE_STATE", null);
tslib_1.__decorate([
    Mutation
], userStore.prototype, "setOpts", null);
tslib_1.__decorate([
    Mutation
], userStore.prototype, "setHomeVo", null);
tslib_1.__decorate([
    Mutation
], userStore.prototype, "startCheckLoginLoop", null);
tslib_1.__decorate([
    Mutation
], userStore.prototype, "stopCheckLoginLoop", null);
tslib_1.__decorate([
    Action
], userStore.prototype, "Login", null);
tslib_1.__decorate([
    Action
], userStore.prototype, "ResetToken", null);
tslib_1.__decorate([
    Action
], userStore.prototype, "GetUserInfo", null);
tslib_1.__decorate([
    Action
], userStore.prototype, "ChangeRoles", null);
tslib_1.__decorate([
    Action
], userStore.prototype, "LogOut", null);
userStore = tslib_1.__decorate([
    Module({ dynamic: true, store, name: 'user' })
], userStore);
export const UserStore = getModule(userStore);
//# sourceMappingURL=UserStore.js.map
