import * as tslib_1 from "tslib";
import { VuexModule, Module, Action, Mutation, getModule } from 'vuex-module-decorators';
import store from '@/store';
import ConstApi from "@/api/ConstApi";
import OptVo from "@/models/vos/OptVo";
import C from "@/constants/C";
let constStore = class constStore extends VuexModule {
    constructor() {
        super(...arguments);
        /**
         * 获取所有常量配置
         */
        this.constMap = {};
    }
    /**
     * 获取用户类型labels
     */
    get userTypeLabels() {
        return this.constMap.hasOwnProperty("userTypeLabels") ? this.constMap["userTypeLabels"] : {};
    }
    /**
     * 管理员
     */
    get manageTypeOpts() {
        return OptVo.loadListByLabels(this.userTypeLabels, C.OPT_VALUE_TYPE_NUMBER);
    }

    /**
     * 获取img类型
     */
    get getImgLabels() {
        return this.constMap.hasOwnProperty('getImgLabels') ? this.constMap['getImgLabels'] : {}
    }
    /**
     * 设置常量map
     * @param constMap
     */
    async setConstMap(constMap) {
        this.constMap = constMap;
    }
    /**
     * 请求
     */
    async requestConstMap() {
        const constMap = await ConstApi.get();
        this.setConstMap(constMap);
    }
};
tslib_1.__decorate([
    Mutation
], constStore.prototype, "setConstMap", null);
tslib_1.__decorate([
    Action
], constStore.prototype, "requestConstMap", null);
constStore = tslib_1.__decorate([
    Module({ dynamic: true, store, name: 'const' })
], constStore);
export const ConstStore = getModule(constStore);
//# sourceMappingURL=ConstStore.js.map
