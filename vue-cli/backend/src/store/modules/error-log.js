import * as tslib_1 from "tslib";
import { VuexModule, Module, Mutation, Action, getModule } from 'vuex-module-decorators';
import store from '@/store';
let ErrorLog = class ErrorLog extends VuexModule {
    constructor() {
        super(...arguments);
        this.logs = [];
    }
    ADD_ERROR_LOG(log) {
        this.logs.push(log);
    }
    CLEAR_ERROR_LOG() {
        this.logs.splice(0);
    }
    AddErrorLog(log) {
        this.ADD_ERROR_LOG(log);
    }
    ClearErrorLog() {
        this.CLEAR_ERROR_LOG();
    }
};
tslib_1.__decorate([
    Mutation
], ErrorLog.prototype, "ADD_ERROR_LOG", null);
tslib_1.__decorate([
    Mutation
], ErrorLog.prototype, "CLEAR_ERROR_LOG", null);
tslib_1.__decorate([
    Action
], ErrorLog.prototype, "AddErrorLog", null);
tslib_1.__decorate([
    Action
], ErrorLog.prototype, "ClearErrorLog", null);
ErrorLog = tslib_1.__decorate([
    Module({ dynamic: true, store, name: 'errorLog' })
], ErrorLog);
export const ErrorLogModule = getModule(ErrorLog);
//# sourceMappingURL=error-log.js.map