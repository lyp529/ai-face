import { VuexModule, Module, Action, Mutation, getModule } from 'vuex-module-decorators'
import { login, logout, getUserInfo } from '@/api/users'
import { getToken, setToken, removeToken } from '@/utils/cookies'
import router, { resetRouter } from '@/router'
import { PermissionModule } from './permission'
import { TagsViewModule } from './tags-view'
import store from '@/store'
import UserApi from '@/api/UserApi'
import { Message } from 'element-ui'
import WindowUtil from '@/utils/WindowUtil'
import OptVo from '@/models/vos/OptVo'
import { ConstStore } from '@/store/modules/ConstStore'
import C from '@/constants/C'
import HomeVo from '@/models/vos/HomeVo'

export interface IUserState {
    token: string
    name: string
    introduction: string
    roles: string[]
    email: string
    userType: number
    id: number
    opts: { superHolderOpts: OptVo[], holderOpts: OptVo[], vendorOpts: OptVo[], agentOpts: OptVo[] },
    homeVo: HomeVo
    checkLoginLoopTimer?: NodeJS.Timeout;
}

@Module({ dynamic: true, store, name: 'user' })
class userStore extends VuexModule implements IUserState {
    public token = getToken() || ''
    public name = ''
    public introduction = ''
    public roles: string[] = []
    public email = ''
    /**
     * 用户类型
     */
    public userType: number = 0;
    /**
     * 用户id
     */
    public id: number = 0;
    /**
     * 用户选项数据
     */
    public opts: {
        superHolderOpts: OptVo[]
        holderOpts: OptVo[]
        vendorOpts: OptVo[]
        agentOpts: OptVo[]
    } = {
      superHolderOpts: [],
      holderOpts: [],
      vendorOpts: [],
      agentOpts: []
    };
    /**
     * 首页数据
     */
    public homeVo: HomeVo = new HomeVo();

    /**
     * 检测是否登录的定时器
     */
    public checkLoginLoopTimer?: NodeJS.Timeout;

    /**
     * 是否是管理员
     */
    public get isAdmin(): boolean {
      return this.userType === C.USER_TYPE_ADMIN
    }

    /**
     * 是否是会员
     */
    public get isMember(): boolean {
      return this.userType === C.USER_TYPE_MEMBER
    }

    /**
     * 获取用户id
     */
    public get userId(): number {
      return this.id
    }

    /**
     * 判断当前是否登录
     */
    public get isLogin(): boolean {
      return this.token !== ''
    }

    @Mutation
    private SET_TOKEN(token: string) {
      this.token = token
    }

    @Mutation
    private SET_NAME(name: string) {
      this.name = name
    }

    @Mutation
    private SET_INTRODUCTION(introduction: string) {
      this.introduction = introduction
    }

    @Mutation
    private SET_ROLES(roles: string[]) {
      this.roles = roles
    }

    @Mutation
    private SET_EMAIL(email: string) {
      this.email = email
    }

    @Mutation
    private SET_ID(id: number) {
      this.id = id
    }

    /**
     * 设置选项数据
     * @param opts
     */
    @Mutation
    private setOpts(opts: { superHolderOpts: OptVo[], holderOpts: OptVo[], vendorOpts: OptVo[], agentOpts: OptVo[] }) {
      this.opts = opts
    }

    /**
     * 设置首页数据
     * @param homeVo
     */
    @Mutation
    private setHomeVo(homeVo: any) {
      this.homeVo.load(homeVo)
    }

    /**
     * 启动检测登录的循环
     */
    @Mutation
    public startCheckLoginLoop() {
      if (this.checkLoginLoopTimer !== undefined) {
        clearInterval(this.checkLoginLoopTimer)
      }
      this.checkLoginLoopTimer = setInterval(async() => {
        if (!UserStore.isLogin) {
          return
        }

        await UserApi.checkLogin()
      }, 10000)
    }

    /**
     * 停止检测登录的循环
     */
    @Mutation
    public stopCheckLoginLoop() {
      if (this.checkLoginLoopTimer !== undefined) {
        clearInterval(this.checkLoginLoopTimer)
      }
    }

    @Action
    public async Login(userInfo: { username: string, password: string }) {
      const vo = await UserApi.login(userInfo)
      if (!vo.isDone()) {
        Message.error(vo.message)
        return
      }
      setToken(vo.data('userId'))
      this.SET_TOKEN(vo.data('userId'))
    }

    @Action
    public ResetToken() {
      removeToken()
      this.SET_TOKEN('')
      this.SET_ROLES([])
    }

    @Action
    public async GetUserInfo() {
      if (this.token === '') {
        throw Error('GetUserInfo: token is undefined!')
      }
      const userInfoVo = await UserApi.getGetUserInfoVo()
      if (userInfoVo === null) {
        WindowUtil.messageError('获取用户信息失败')
        this.ResetToken()
        return
      }
      // roles must be a non-empty array
      if (!userInfoVo.roles || userInfoVo.roles.length <= 0) {
        throw Error('GetUserInfo: roles must be a non-null array!')
      }
      // 设置用户数据
      this.SET_ROLES(userInfoVo.roles)
      this.SET_NAME(userInfoVo.name)
      this.SET_INTRODUCTION('')
      this.SET_EMAIL('')
      this.SET_ID(userInfoVo.id)
      this.setHomeVo(userInfoVo.homeVo)
    }

    @Action
    public async ChangeRoles(role: string) {
      // Dynamically modify permissions
      const token = role + '-token'
      this.SET_TOKEN(token)
      setToken(token)
      await this.GetUserInfo()
      resetRouter()
      // Generate dynamic accessible routes based on roles
      PermissionModule.GenerateRoutes(this.roles)
      // Add generated routes
      router.addRoutes(PermissionModule.dynamicRoutes)
      // Reset visited views and cached views
      TagsViewModule.delAllViews()
    }

    @Action
    public async LogOut() {
      if (this.token === '') {
        throw Error('LogOut: token is undefined!')
      }
      removeToken()
      resetRouter()
      this.SET_TOKEN('')
      this.SET_ROLES([])
      this.stopCheckLoginLoop() // 移到上面来 by lizr
      let isLogoutDone = await UserApi.logout()
      if (isLogoutDone) {
        // this.stopCheckLoginLoop();
        await router.push(`/login?redirect=${router.currentRoute.fullPath}`)
      }
    }
}

export const UserStore = getModule(userStore)
