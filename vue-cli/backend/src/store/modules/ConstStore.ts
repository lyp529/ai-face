import { VuexModule, Module, Action, Mutation, getModule } from 'vuex-module-decorators'
import store from '@/store'
import ConstApi from '@/api/ConstApi'
import OptVo from '@/models/vos/OptVo'
import C from '@/constants/C'

export interface IConstState {
    constMap: { [key: string]: any }
}

@Module({ dynamic: true, store, name: 'const' })
class constStore extends VuexModule implements IConstState {
    /**
     * 获取所有常量配置
     */
    public constMap: { [key: string]: any } = {};

    /**
     * 获取用户类型labels
     */
    public get userTypeLabels(): { [key: number]: string } {
      // eslint-disable-next-line no-prototype-builtins
      return this.constMap.hasOwnProperty('userTypeLabels') ? this.constMap['userTypeLabels'] : {}
    }

    /**
     * 管理员
     */
    public get manageTypeOpts(): OptVo[] {
      return OptVo.loadListByLabels(this.userTypeLabels, C.OPT_VALUE_TYPE_NUMBER)
    }

    /**
     * 获取img类型
     */
    public get getImgLabels(): { [key: number]: string } {
      return this.constMap.hasOwnProperty('getImgLabels') ? this.constMap['getImgLabels'] : {}
    }

    /**
     * 设置常量map
     * @param constMap
     */
    @Mutation
    private async setConstMap(constMap: { [key: string]: any }) {
      this.constMap = constMap
    }

    /**
     * 请求
     */
    @Action
    public async requestConstMap() {
      const constMap = await ConstApi.get()
      this.setConstMap(constMap)
    }
}

export const ConstStore = getModule(constStore)
