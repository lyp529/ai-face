import * as tslib_1 from "tslib";
import { Component, Vue } from 'vue-property-decorator';
import ServiceWorkerUpdatePopup from '@/pwa/components/ServiceWorkerUpdatePopup.vue';
let default_1 = class extends Vue {
};
default_1 = tslib_1.__decorate([
    Component({
        name: 'App',
        components: {
            ServiceWorkerUpdatePopup
        }
    })
], default_1);
export default default_1;
//# sourceMappingURL=App.vue.js.map