export default class TableDataVo {
    constructor() {
        this.rows = [];
        this.type = [];
        this.count = 0;
    }
    /**
     * 使用json数据加载对象
     * @param values
     */
    load(values) {
        this.rows = values.rows;
        this.type = values.type;
        this.count = Number(values.count);
        return this;
    }
}
//# sourceMappingURL=TableDataVo.js.map