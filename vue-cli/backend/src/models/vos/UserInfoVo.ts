import BaseVo from '@/models/vos/BaseVo'
import JsonInstantiable from '@/models/JsonInitabled'
import HomeVo from '@/models/vos/HomeVo'

/**
 * 用户vo
 */
export default class UserInfoVo extends BaseVo implements JsonInstantiable<UserInfoVo> {
    /**
     * @var int 用户id
     */
    public id: number = 0;
    /**
     * @var string 用户名字
     */
    public name: string = '';
    /**
     * @var string[] 角色权限
     */
    public roles: string[] = [];
    /**
     * 首页数据
     */
    public homeVo: HomeVo = new HomeVo();

    /**
     *
     * @param values
     */
    public load(values: { [key: string]: any }): UserInfoVo {
      this.id = Number(values.id)
      this.name = values.name
      this.roles = values.roles
      this.homeVo.load(values.homeVo)
      return this
    }
}
