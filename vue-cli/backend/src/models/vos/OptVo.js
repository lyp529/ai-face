import C from "@/constants/C";
/**
 * 选项数据（可以用 labels 初始化）
 */
export default class OptVo {
    constructor() {
        /**
         * 选项数据
         */
        this.value = "";
        /**
         * 标签数据
         */
        this.label = "";
    }
    /**
     * 使用数据初始化
     * @param values
     */
    load(values) {
        this.value = values.value;
        this.label = values.label;
        return this;
    }
    /**
     * 使用 labels 初始化一个列表
     * @param labels
     * @param valueType
     */
    static loadListByLabels(labels, valueType = C.OPT_VALUE_TYPE_DEFAULT) {
        let list = [];
        for (let i in labels) {
            let label = labels[i];
            let value = "";
            if (valueType === C.OPT_VALUE_TYPE_NUMBER) {
                value = Number(i);
            }
            else if (valueType === C.OPT_VALUE_TYPE_STRING) {
                value = i + "";
            }
            else {
                value = i;
            }
            const optVo = new OptVo().load({ value, label });
            list.push(optVo);
        }
        return list;
    }
    /**
     * 使用 labels 初始化一个map
     * @param labels
     * @param valueType
     */
    static loadMapByLabels(labels, valueType = C.OPT_VALUE_TYPE_DEFAULT) {
        let list = OptVo.loadListByLabels(labels, valueType);
        let map = {};
        list.forEach((optVo, index) => {
            map[optVo.value] = optVo;
        });
        return map;
    }
}
//# sourceMappingURL=OptVo.js.map