import BaseVo from "@/models/vos/BaseVo";
/**
 * 首页数据
 */
export default class HomeVo extends BaseVo {
    constructor() {
        super(...arguments);
        /**
         * 本次登录时间
         */
        this.loginTime = "";
        /**
         * 本次登录IP
         */
        this.loginIp = "";
        /**
         * 上次登录时间
         */
        this.lastLoginTime = "";
        /**
         * 上次登录IP
         */
        this.lastLoginIp = "";
    }
    /**
     * json 加载对象数据
     * @param values
     */
    load(values) {
        this.loginTime = values.loginTime;
        this.loginIp = values.loginIp;
        this.lastLoginTime = values.lastLoginTime;
        this.lastLoginIp = values.lastLoginIp;
        return this;
    }
}
//# sourceMappingURL=HomeVo.js.map