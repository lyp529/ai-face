import { Message, MessageBox, Notification } from "element-ui";
import C from "../constants/C";
/**
 * 窗口工具。用于弹窗、消息提醒、通知等
 */
export default class WindowUtil {
    /**
     * 基础消息
     * @param message 消息内容
     * @param type 消息类型
     * @param seconds 显示秒数
     */
    static message(message, type = C.MESSAGE_TYPE_INFO, seconds = 5) {
        Message({
            message: message,
            type: type,
            duration: seconds * 1000,
        });
    }
    /**
     * 成功消息
     * @param message 消息内容
     * @param seconds 显示秒数
     */
    static messageSuccess(message, seconds = 5) {
        WindowUtil.message(message, C.MESSAGE_TYPE_SUCCESS, seconds);
    }
    /**
     * 警告消息
     * @param message 消息内容
     * @param seconds 显示秒数
     */
    static messageWarning(message, seconds = 5) {
        WindowUtil.message(message, C.MESSAGE_TYPE_WARNING, seconds);
    }
    /**
     * 错误消息
     * @param message 消息内容
     * @param seconds 显示秒数
     */
    static messageError(message, seconds = 5) {
        WindowUtil.message(message, C.MESSAGE_TYPE_ERROR, seconds);
    }
    /**
     * 等待警告消息
     * @param seconds 显示秒数
     */
    static messageWarningWait(seconds = 5) {
        WindowUtil.messageWarning('正在处理，请稍后...', seconds);
    }
    /**
     * 关闭所有消息
     */
    static messageCloseAll() {
        // @ts-ignore 这个是隐藏方法，忽略IDE报错
        Message.closeAll();
    }
    // -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    /**
     * 基础通知
     * @param message 消息
     * @param title 标题
     * @param type 类型
     * @param seconds 显示秒数
     * @param position 弹出位置
     */
    static notify(message, title, type = C.MESSAGE_TYPE_INFO, seconds = 5, position = C.NOTIFICATION_POSITION_TOP_RIGHT) {
        Notification({
            title: title,
            message: message,
            type: type,
            duration: seconds * 1000,
            position: position,
        });
    }
    /**
     * 成功通知
     * @param message
     * @param title
     * @param seconds
     * @param position
     */
    static notifySuccess(message, title = "成功", seconds = 5, position = C.NOTIFICATION_POSITION_TOP_RIGHT) {
        WindowUtil.notify(message, title, C.MESSAGE_TYPE_SUCCESS, seconds, position);
    }
    /**
     * 警告通知
     * @param message
     * @param title
     * @param seconds
     * @param position
     */
    static notifyWarning(message, title = "警告", seconds = 5, position = C.NOTIFICATION_POSITION_TOP_RIGHT) {
        WindowUtil.notify(message, title, C.MESSAGE_TYPE_WARNING, seconds, position);
    }
    /**
     * 错误通知
     * @param message
     * @param title
     * @param seconds
     * @param position
     */
    static notifyError(message, title = "错误", seconds = 5, position = C.NOTIFICATION_POSITION_TOP_RIGHT) {
        WindowUtil.notify(message, title, C.MESSAGE_TYPE_ERROR, seconds, position);
    }
    /**
     * 关闭所有通知
     */
    static notifyCloseAll() {
        // @ts-ignore 这个是隐藏方法，忽略IDE报错
        Notification.closeAll();
    }
    // -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    /**
     * 确认弹窗
     * @param message
     * @param title
     */
    static async confirm(message, title = "请确认") {
        return new Promise((resolve) => {
            MessageBox.confirm(message, title, {}).then(() => {
                resolve(true);
            }).catch(() => {
                resolve(false);
            });
        });
    }
    /**
     * 弹窗提示
     * @param message
     * @param title
     */
    static async dialog(message, title = "提示") {
        return new Promise(resolve => {
            MessageBox({
                message: message,
                title: title,
            }).then(() => {
                resolve();
            }).catch(() => {
                resolve();
            });
        });
    }
    /**
     * 在浏览器新页签中打开链接
     */
    static async openNewTab(url) {
        window.open(url, '_blank', '');
    }
}
//# sourceMappingURL=WindowUtil.js.map