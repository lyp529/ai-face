// Parse the time to string
export const parseTime = (time, cFormat) => {
    if (time === undefined) {
        return null;
    }
    const format = cFormat || '{y}-{m}-{d} {h}:{i}:{s}';
    let date;
    if (typeof time === 'object') {
        date = time;
    }
    else {
        if (typeof time === 'string' && /^[0-9]+$/.test(time)) {
            time = parseInt(time);
        }
        if (typeof time === 'number' && time.toString().length === 10) {
            time = time * 1000;
        }
        date = new Date(time);
    }
    const formatObj = {
        y: date.getFullYear(),
        m: date.getMonth() + 1,
        d: date.getDate(),
        h: date.getHours(),
        i: date.getMinutes(),
        s: date.getSeconds(),
        a: date.getDay()
    };
    const timeStr = format.replace(/{(y|m|d|h|i|s|a)+}/g, (result, key) => {
        let value = formatObj[key];
        // Note: getDay() returns 0 on Sunday
        if (key === 'a') {
            return ['日', '一', '二', '三', '四', '五', '六'][value];
        }
        if (result.length > 0 && value < 10) {
            return '0' + value;
        }
        return String(value) || '0';
    });
    return timeStr;
};
// Format and filter json data using filterKeys array
export const formatJson = (filterKeys, jsonData) => jsonData.map((data) => filterKeys.map((key) => {
    if (key === 'timestamp') {
        return parseTime(data[key]);
    }
    else {
        return data[key];
    }
}));
// Check if an element has a class
export const hasClass = (ele, className) => {
    return !!ele.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'));
};
// Add class to element
export const addClass = (ele, className) => {
    if (!hasClass(ele, className))
        ele.className += ' ' + className;
};
// Remove class from element
export const removeClass = (ele, className) => {
    if (hasClass(ele, className)) {
        const reg = new RegExp('(\\s|^)' + className + '(\\s|$)');
        ele.className = ele.className.replace(reg, ' ');
    }
};
// Toggle class for the selected element
export const toggleClass = (ele, className) => {
    if (!ele || !className) {
        return;
    }
    let classString = ele.className;
    const nameIndex = classString.indexOf(className);
    if (nameIndex === -1) {
        classString += '' + className;
    }
    else {
        classString =
            classString.substr(0, nameIndex) +
                classString.substr(nameIndex + className.length);
    }
    ele.className = classString;
};
//# sourceMappingURL=index.js.map