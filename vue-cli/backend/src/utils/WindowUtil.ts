
import {Message, MessageBox, Notification} from "element-ui";
import C from "../constants/C";
import {MessageType} from "element-ui/types/message";
import {NotificationPosition} from "element-ui/types/notification";

/**
 * 窗口工具。用于弹窗、消息提醒、通知等
 */
export default class WindowUtil {
    /**
     * 基础消息
     * @param message 消息内容
     * @param type 消息类型
     * @param seconds 显示秒数
     */
    public static message(message: string, type: MessageType = C.MESSAGE_TYPE_INFO, seconds: number = 5) {
        Message({
            message: message,
            type: type,
            duration: seconds * 1000,
        });
    }

    /**
     * 成功消息
     * @param message 消息内容
     * @param seconds 显示秒数
     */
    public static messageSuccess(message: string, seconds: number = 5) {
        WindowUtil.message(message, C.MESSAGE_TYPE_SUCCESS, seconds);
    }

    /**
     * 警告消息
     * @param message 消息内容
     * @param seconds 显示秒数
     */
    public static messageWarning(message: string, seconds: number = 5) {
        WindowUtil.message(message, C.MESSAGE_TYPE_WARNING, seconds);
    }

    /**
     * 错误消息
     * @param message 消息内容
     * @param seconds 显示秒数
     */
    public static messageError(message: string, seconds: number = 5) {
        WindowUtil.message(message, C.MESSAGE_TYPE_ERROR, seconds);
    }

    /**
     * 等待警告消息
     * @param seconds 显示秒数
     */
    public static messageWarningWait(seconds: number = 5) {
        WindowUtil.messageWarning('正在处理，请稍后...', seconds);
    }

    /**
     * 关闭所有消息
     */
    public static messageCloseAll() {
        // @ts-ignore 这个是隐藏方法，忽略IDE报错
        Message.closeAll();
    }

    // -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * 基础通知
     * @param message 消息
     * @param title 标题
     * @param type 类型
     * @param seconds 显示秒数
     * @param position 弹出位置
     */
    public static notify(message: string, title: string, type: MessageType = C.MESSAGE_TYPE_INFO, seconds: number = 5, position: NotificationPosition = C.NOTIFICATION_POSITION_TOP_RIGHT) {
        Notification({
            title: title,
            message: message,
            type: type,
            duration: seconds * 1000,
            position: position,
        });
    }

    /**
     * 成功通知
     * @param message
     * @param title
     * @param seconds
     * @param position
     */
    public static notifySuccess(message: string, title: string = "成功", seconds: number = 5, position: NotificationPosition = C.NOTIFICATION_POSITION_TOP_RIGHT) {
        WindowUtil.notify(message, title, C.MESSAGE_TYPE_SUCCESS, seconds, position);
    }

    /**
     * 警告通知
     * @param message
     * @param title
     * @param seconds
     * @param position
     */
    public static notifyWarning(message: string, title: string = "警告", seconds: number = 5, position: NotificationPosition = C.NOTIFICATION_POSITION_TOP_RIGHT) {
        WindowUtil.notify(message, title, C.MESSAGE_TYPE_WARNING, seconds, position);
    }

    /**
     * 错误通知
     * @param message
     * @param title
     * @param seconds
     * @param position
     */
    public static notifyError(message: string, title: string = "错误", seconds: number = 5, position: NotificationPosition = C.NOTIFICATION_POSITION_TOP_RIGHT) {
        WindowUtil.notify(message, title, C.MESSAGE_TYPE_ERROR, seconds, position);
    }

    /**
     * 关闭所有通知
     */
    public static notifyCloseAll() {
        // @ts-ignore 这个是隐藏方法，忽略IDE报错
        Notification.closeAll();
    }

    // -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    // -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


    /**
     * 确认弹窗
     * @param message
     * @param title
     */
    public static async confirm(message: string, title: string = "请确认"): Promise<boolean> {
        return new Promise<boolean>((resolve) => {
            MessageBox.confirm(message, title, {}).then(() => {
                resolve(true)
            }).catch(() => {
                resolve(false);
            });
        })
    }

    /**
     * 弹窗提示
     * @param message
     * @param title
     */
    public static async dialog(message: string, title: string = "提示"): Promise<void> {
        return new Promise<void>(resolve => {
            MessageBox({
                message: message,
                title: title,
            }).then(() => {
                resolve();
            }).catch(() => {
                resolve();
            })
        });
    }


    /**
     * 在浏览器新页签中打开链接
     */
    public static async openNewTab(url: string) {
        window.open(url, '_blank', '');
    }
}
