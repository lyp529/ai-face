/**
 * 加密工具
 */
// @ts-ignore
import Base64 from 'crypto-js/enc-base64';
import * as CryptoJS from "crypto-js";

/**
 * 加密工具
 */
export default class EncryptUtil {
    /**
     * sha256加密（即md5）
     * @param str
     */
    public static sha256(str: string): string {
        return Base64.stringify(CryptoJS.SHA256(str));
    }
}
