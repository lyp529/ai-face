import CommonUtil from "../utils/CommonUtil";
import OptVo from "@/models/vos/OptVo";
export default class C {
}
/**
 * 表单类型：添加
 */
C.FORM_TYPE_ADD = 1;
/**
 * 表单类型：修改
 */
C.FORM_TYPE_MOD = 2;
/**
 * 表单类型：初始化
 */
C.FORM_TYPE_INIT = 3;
/**
 * 表单类型：登录
 */
C.FORM_TYPE_LOGIN = 4;
/**
 * 表单类型：保存
 */
C.FORM_TYPE_SAVE = 5;
/**
 * 表单类型：提交（确认提交数据）
 */
C.FORM_TYPE_SUBMIT = 6;
/**
 * 消息类型：成功
 */
C.MESSAGE_TYPE_SUCCESS = "success";
/**
 * 消息类型：警告
 */
C.MESSAGE_TYPE_WARNING = "warning";
/**
 * 消息类型：信息（默认）
 */
C.MESSAGE_TYPE_INFO = "info";
/**
 * 消息类型：错误
 */
C.MESSAGE_TYPE_ERROR = "error";
/**
 * 通知位置：上右
 */
C.NOTIFICATION_POSITION_TOP_RIGHT = "top-right";
/**
 * 通知位置：上左
 */
C.NOTIFICATION_POSITION_TOP_LEFT = "top-left";
/**
 * 通知位置：下右
 */
C.NOTIFICATION_POSITION_BOTTOM_RIGHT = "bottom-right";
/**
 * 通知位置：下左
 */
C.NOTIFICATION_POSITION_BOTTOM_LEFT = "bottom-left";
/**
 * 用户类型：会员
 */
C.USER_TYPE_MEMBER = 1;
/**
 * 用户类型：管理员
 */
C.USER_TYPE_ADMIN = 99;
/**
 * 选项数据类型：数字
 */
C.OPT_VALUE_TYPE_NUMBER = 1;
/**
 * 选项数据类型：字符串
 */
C.OPT_VALUE_TYPE_STRING = 2;
/**
 * 选项数据类型：默认（原来是什么就是什么）
 */
C.OPT_VALUE_TYPE_DEFAULT = 3;
/**
 * 响应代码：未登录
 */
C.RESPONSE_CODE_NOT_LOGIN = -3;
/**
 * 标签数据map
 */
C.constantMap = null;
//# sourceMappingURL=C.js.map
