/**
 * 前端通用常量定义
 */
import { MessageType } from 'element-ui/types/message'
import { NotificationPosition } from 'element-ui/types/notification'
import CommonUtil from '../utils/CommonUtil'
import OptVo from '@/models/vos/OptVo'

export default class C {
    /**
     * 表单类型：添加
     */
    public static readonly FORM_TYPE_ADD: number = 1;
    /**
     * 表单类型：修改
     */
    public static readonly FORM_TYPE_MOD: number = 2;
    /**
     * 表单类型：初始化
     */
    public static readonly FORM_TYPE_INIT: number = 3;
    /**
     * 表单类型：登录
     */
    public static readonly FORM_TYPE_LOGIN: number = 4;
    /**
     * 表单类型：保存
     */
    public static readonly FORM_TYPE_SAVE: number = 5;
    /**
     * 表单类型：提交（确认提交数据）
     */
    public static readonly FORM_TYPE_SUBMIT: number = 6;

    /**
     * 消息类型：成功
     */
    public static readonly MESSAGE_TYPE_SUCCESS: MessageType = 'success';
    /**
     * 消息类型：警告
     */
    public static readonly MESSAGE_TYPE_WARNING: MessageType = 'warning';
    /**
     * 消息类型：信息（默认）
     */
    public static readonly MESSAGE_TYPE_INFO: MessageType = 'info';
    /**
     * 消息类型：错误
     */
    public static readonly MESSAGE_TYPE_ERROR: MessageType = 'error';

    /**
     * 通知位置：上右
     */
    public static readonly NOTIFICATION_POSITION_TOP_RIGHT: NotificationPosition = 'top-right';
    /**
     * 通知位置：上左
     */
    public static readonly NOTIFICATION_POSITION_TOP_LEFT: NotificationPosition = 'top-left';
    /**
     * 通知位置：下右
     */
    public static readonly NOTIFICATION_POSITION_BOTTOM_RIGHT: NotificationPosition = 'bottom-right';
    /**
     * 通知位置：下左
     */
    public static readonly NOTIFICATION_POSITION_BOTTOM_LEFT: NotificationPosition = 'bottom-left';

    /**
     * 用户类型：会员
     */
    public static readonly USER_TYPE_MEMBER = 1;
    /**
     * 用户类型：管理员
     */
    public static readonly USER_TYPE_ADMIN = 99;

    /**
     * 选项数据类型：数字
     */
    public static readonly OPT_VALUE_TYPE_NUMBER = 1;
    /**
     * 选项数据类型：字符串
     */
    public static readonly OPT_VALUE_TYPE_STRING = 2;
    /**
     * 选项数据类型：默认（原来是什么就是什么）
     */
    public static readonly OPT_VALUE_TYPE_DEFAULT = 3;

    /**
     * 响应代码：未登录
     */
    public static readonly RESPONSE_CODE_NOT_LOGIN = -3;

    /**
     * 标签数据map
     */
    private static constantMap: { [key: string]: any } | null = null;
}
