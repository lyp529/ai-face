/**
 * 日志API
 * */
import HttpUtil from '@/utils/HttpUtil'
import TableDataVo from '@/models/vos/TableDataVo'

export default class LogApi {
  /**
     * 获取登录日志
     * @param params
     */
  public static async getLoginLog(params: any): Promise<TableDataVo> {
    let vo = await HttpUtil.post('log/get-login-log', params)
    if (!vo.isDone()) {
      return new TableDataVo()
    }
    return new TableDataVo().load(vo.d)
  }
}
