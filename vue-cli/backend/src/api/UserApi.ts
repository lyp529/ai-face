/**
 * 用户api
 */
import ResponseVo from '@/models/vos/ResponseVo'
import HttpUtil from '@/utils/HttpUtil'
import WindowUtil from '@/utils/WindowUtil'
import UserInfoVo from '@/models/vos/UserInfoVo'
import TableDataVo from '@/models/vos/TableDataVo'
import { emptyTypeAnnotation } from '@babel/types'
import { UserStore } from '@/store/modules/UserStore'

export default class UserApi {
  /**
     * 用户登录
     * @param params
     */
  public static async login(params: {username: string, password: string}): Promise<ResponseVo> {
    // eslint-disable-next-line no-return-await
    return await HttpUtil.post('user/login', params)
  }

  /**
     * 登出接口
     */
  public static async logout(): Promise<boolean> {
    let vo = await HttpUtil.post('user/logout')
    if (!vo.isDone()) {
      WindowUtil.messageError(vo.message)
      return false
    }
    return true
  }
  /**
     * 获取当前登录账号的的信息
     */
  public static async getGetUserInfoVo(): Promise<UserInfoVo|null> {
    let vo = await HttpUtil.post('user/getUserInfoVo')
    if (!vo.isDone()) {
      return null
    }
    return new UserInfoVo().load(vo.data('userInfoVo'))
  }
  /**
     *
     * @param userId
     * @param active
     */
  public static async modActive(userId: number, active: boolean): Promise<boolean> {
    let vo = await HttpUtil.post('user/modActive', { userId, active })
    return vo.commonHandler()
  }

  /**
     * 检测是否登录
     */
  public static async checkLogin(): Promise<boolean> {
    let vo = await HttpUtil.post('user/checkLogin')
    return vo.isDone()
  }

  /**
     * 获取用户列表
     * @param params
     */
  public static async getUserList(params: any): Promise<TableDataVo> {
    let vo = await HttpUtil.post('user/getUserList', params)
    if (!vo.isDone()) {
      return new TableDataVo()
    }
    return new TableDataVo().load(vo.d)
  }

  /**
     * 获取管理员列表
     * @param params
     */
  public static async getAdminList(params: any): Promise<TableDataVo> {
    let vo = await HttpUtil.post('user/get-admin-list', params)
    if (!vo.isDone()) {
      return new TableDataVo()
    }
    return new TableDataVo().load(vo.d)
  }

  /**
     * 获取医院列表
     * @param params
     */
  public static async getHospitalList(params: any): Promise<TableDataVo> {
    let vo = await HttpUtil.post('user/admin-get-hospital-list', params)
    if (!vo.isDone()) {
      return new TableDataVo()
    }
    return new TableDataVo().load(vo.d)
  }

  /**
     * 获取医院申请列表
     * @param params
     */
  public static async getHospitalRequestList(params: any): Promise<TableDataVo> {
    let vo = await HttpUtil.post('user/get-hospital-request-list', params)
    if (!vo.isDone()) {
      return new TableDataVo()
    }
    return new TableDataVo().load(vo.d)
  }
  /**
     * 更新医院申请
     * @param params
     */
  public static async updateHospitalRequest(params: any) {
    let vo = await HttpUtil.post('user/update-hospital-request', params)
    return vo.commonHandler()
  }
  /**
     * 删除医院
     * @param params
     */
  public static async delHospital(params: any) {
    let vo = await HttpUtil.post('user/del-hospital', params)
    return vo.commonHandler()
  }

  /**
     * 获取预约订单列表
     * @param params
     */
  public static async getHospitalOrderList(params: any): Promise<TableDataVo> {
    let vo = await HttpUtil.post('user/get-hospital-order-list', params)
    if (!vo.isDone()) {
      return new TableDataVo()
    }
    return new TableDataVo().load(vo.d)
  }

  /**
     * 更新预约订单
     * @param params
     */
  public static async updateHospitalOrder(params: any): Promise<TableDataVo> {
    let vo = await HttpUtil.post('user/update-hospital-order', params)
    if (!vo.isDone()) {
      return new TableDataVo()
    }
    return new TableDataVo().load(vo.d)
  }

  /**
     * 获取代理商列表
     * @param params
     */
  public static async getAgentList(params: any): Promise<TableDataVo> {
    let vo = await HttpUtil.post('user/get-agent-list', params)
    if (!vo.isDone()) {
      return new TableDataVo()
    }
    return new TableDataVo().load(vo.d)
  }

  /**
     * 更新代理商申请
     * @param params
     */
  public static async updateAgent(params: any): Promise<TableDataVo> {
    let vo = await HttpUtil.post('user/update-agent', params)
    if (!vo.isDone()) {
      return new TableDataVo()
    }
    return new TableDataVo().load(vo.d)
  }

  /**
     * 面相订单表
     * @param params
     */
  public static async getFaceOrderList(params: any): Promise<TableDataVo> {
    let vo = await HttpUtil.post('user/get-face-order-list', params)
    if (!vo.isDone()) {
      return new TableDataVo()
    }
    return new TableDataVo().load(vo.d)
  }

  /**
     * 更新面相概述
     * @param params
     */
  public static async updateFaceReport(params: any): Promise<TableDataVo> {
    let vo = await HttpUtil.post('user/update-face-report', params)
    if (!vo.isDone()) {
      return new TableDataVo()
    }
    return new TableDataVo().load(vo.d)
  }

  /**
     * 获取佣金列
     * @param params
     */
  public static async getBrokerageList(params: any): Promise<TableDataVo> {
    let vo = await HttpUtil.post('user/get-brokerage-list', params)
    if (!vo.isDone()) {
      return new TableDataVo()
    }
    return new TableDataVo().load(vo.d)
  }

  /**
     * 更新提现申请
     * @param params
     */
  public static async updateWithdraw(params: any): Promise<TableDataVo> {
    let vo = await HttpUtil.post('user/update-withdraw', params)
    if (!vo.isDone()) {
      return new TableDataVo()
    }
    return new TableDataVo().load(vo.d)
  }
}
