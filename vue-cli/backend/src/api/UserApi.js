import HttpUtil from "@/utils/HttpUtil";
import WindowUtil from "@/utils/WindowUtil";
import UserInfoVo from "@/models/vos/UserInfoVo";
import TableDataVo from "@/models/vos/TableDataVo";
export default class UserApi {
    /**
     * 用户登录
     * @param params
     */
    static async login(params) {
        return await HttpUtil.post("user/login", params);
    }
    /**
     * 登出接口
     */
    static async logout() {
        let vo = await HttpUtil.post("user/logout");
        if (!vo.isDone()) {
            WindowUtil.messageError(vo.message);
            return false;
        }
        return true;
    }
    static async getProductImgList(params) {
        let vo = await HttpUtil.post('user/get-product-img-list', params)
        if (!vo.isDone()) {
            return new TableDataVo()
        }
        return new TableDataVo().load(vo.d)
    }
    /**
     * 获取当前登录账号的的信息
     */
    static async getGetUserInfoVo() {
        let vo = await HttpUtil.post("user/getUserInfoVo");
        if (!vo.isDone()) {
            return null;
        }
        return new UserInfoVo().load(vo.data("userInfoVo"));
    }
    /**
     *
     * @param userId
     * @param active
     */
    static async modActive(userId, active) {
        let vo = await HttpUtil.post("user/modActive", { userId, active });
        return vo.commonHandler();
    }
    /**
     * 检测是否登录
     */
    static async checkLogin() {
        let vo = await HttpUtil.post("user/checkLogin");
        return vo.isDone();
    }
    /**
     * 获取用户列表
     * @param params
     */
    static async getUserList(params) {
        let vo = await HttpUtil.post('user/getUserList', params)
        if (!vo.isDone()) {
            return new TableDataVo()
        }
        return new TableDataVo().load(vo.d)
    }

    /**
     * 获取管理员列表
     * @param params
     */
    static async getAdminList(params) {
        let vo = await HttpUtil.post('user/get-admin-list', params)
        if (!vo.isDone()) {
            return new TableDataVo()
        }
        return new TableDataVo().load(vo.d)
    }

    /**
     * 获取医院列表
     * @param params
     */
    static async getHospitalList(params) {
        let vo = await HttpUtil.post('user/admin-get-hospital-list', params)
        if (!vo.isDone()) {
            return new TableDataVo()
        }
        return new TableDataVo().load(vo.d)
    }

    /**
     * 获取医院申请列表
     * @param params
     */
    static async getHospitalRequestList(params) {
        let vo = await HttpUtil.post('user/get-hospital-request-list', params)
        if (!vo.isDone()) {
            return new TableDataVo()
        }
        return new TableDataVo().load(vo.d)
    }
    /**
     * 更新医院申请
     * @param params
     */
    static async updateHospitalRequest(params) {
        let vo = await HttpUtil.post('user/update-hospital-request', params)
        return vo.commonHandler()
    }
    /**
     * 删除医院
     * @param params
     */
    static async delHospital(params) {
        let vo = await HttpUtil.post('user/del-hospital', params)
        return vo.commonHandler()
    }

    /**
     * 获取预约订单列表
     * @param params
     */
    static async getHospitalOrderList(params) {
        let vo = await HttpUtil.post('user/get-hospital-order-list', params)
        if (!vo.isDone()) {
            return new TableDataVo()
        }
        return new TableDataVo().load(vo.d)
    }
    /**
     * 更新预约订单
     * @param params
     */
    static async updateHospitalOrder(params) {
        let vo = await HttpUtil.post('user/update-hospital-order', params)
        if (!vo.isDone()) {
            return new TableDataVo()
        }
        return new TableDataVo().load(vo.d)
    }

    /**
     * 获取代理商列表
     * @param params
     */
    static async getAgentList(params) {
        let vo = await HttpUtil.post('user/get-agent-list', params)
        if (!vo.isDone()) {
            return new TableDataVo()
        }
        return new TableDataVo().load(vo.d)
    }

    /**
     * 更新代理商申请
     * @param params
     */
    static async updateAgent(params) {
        let vo = await HttpUtil.post('user/update-agent', params)
        if (!vo.isDone()) {
            return new TableDataVo()
        }
        return new TableDataVo().load(vo.d)
    }

    /**
     * 面相订单表
     * @param params
     */
    static async getFaceOrderList(params) {
        let vo = await HttpUtil.post('user/get-face-order-list', params)
        if (!vo.isDone()) {
            return new TableDataVo()
        }
        return new TableDataVo().load(vo.d)
    }

    /**
     * 更新面相概述
     * @param params
     */
    static async updateFaceReport(params) {
        let vo = await HttpUtil.post('user/update-face-report', params)
        if (!vo.isDone()) {
            return new TableDataVo()
        }
        return new TableDataVo().load(vo.d)
    }

    /**
     * 获取佣金列
     * @param params
     */
    static async getBrokerageList(params) {
        let vo = await HttpUtil.post('user/get-brokerage-list', params)
        if (!vo.isDone()) {
            return new TableDataVo()
        }
        return new TableDataVo().load(vo.d)
    }

    /**
     * 更新提现申请
     * @param params
     */
    static async updateWithdraw(params) {
        let vo = await HttpUtil.post('user/update-withdraw', params)
        if (!vo.isDone()) {
            return new TableDataVo()
        }
        return new TableDataVo().load(vo.d)
    }
}
//# sourceMappingURL=UserApi.js.map
