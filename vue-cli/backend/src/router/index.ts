import Vue from 'vue'
import Router, { RouteConfig } from 'vue-router'
import C from '@/constants/C'

/* Layout */
import Layout from '@/layout/index.vue'

/* Router modules */
// import componentsRouter from './modules/demo/components'
// import chartsRouter from './modules/demo/charts'
// import tableRouter from './modules/demo/table'
// import nestedRouter from './modules/demo/nested'
import { testerRouter } from '@/router/modules/developer'
import DebugUtil from '@/utils/DebugUtil'

Vue.use(Router)

/*
  Note: sub-menu only appear when children.length>=1
  Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
*/

/*
  name:'router-name'             the name field is required when using <keep-alive>, it should also match its component's name property
                                 detail see : https://vuejs.org/v2/guide/components-dynamic-async.html#keep-alive-with-Dynamic-Components
  redirect:                      if set to 'noredirect', no redirect action will be trigger when clicking the breadcrumb
  meta: {
    roles: ['admin', 'editor']   will control the page roles (allow setting multiple roles)
    title: 'title'               the name showed in subMenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon showed in the sidebar
    hidden: true                 if true, this route will not show in the sidebar (default is false)
    alwaysShow: true             if true, will always show the root menu (default is false)
                                 if false, hide the root menu when has less or equal than one children route
    breadcrumb: false            if false, the item will be hidden in breadcrumb (default is true)
    noCache: true                if true, the page will not be cached (default is false)
    affix: true                  if true, the tag will affix in the tags-view
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
*/

/**
 ConstantRoutes
 a base page that does not have permission requirements
 all roles can be accessed
 */
export const constantRoutes: RouteConfig[] = [
  {
    path: '/redirect',
    component: Layout,
    meta: { hidden: true },
    children: [
      {
        path: '/redirect/:path*',
        component: () => import(/* webpackChunkName: "redirect" */ '@/views/redirect/index.vue')
      }
    ]
  },
  {
    path: '/login',
    component: () => import(/* webpackChunkName: "login" */ '@/views/login/index.vue'),
    meta: { hidden: true }
  },
  {
    path: '/auth-redirect',
    component: () => import(/* webpackChunkName: "auth-redirect" */ '@/views/login/auth-redirect.vue'),
    meta: { hidden: true }
  },
  {
    path: '/404',
    component: () => import(/* webpackChunkName: "404" */ '@/views/error-page/404.vue'),
    meta: { hidden: true }
  },
  {
    path: '/401',
    component: () => import(/* webpackChunkName: "401" */ '@/views/error-page/401.vue'),
    meta: { hidden: true }
  },
  {
    path: '/',
    component: Layout,
    redirect: '/home',
    children: [
      {
        path: 'home',
        component: () => import(/* webpackChunkName: "home" */ '@/views/home/Home.vue'),
        name: 'Dashboard',
        meta: {
          title: 'dashboard',
          icon: 'dashboard',
          affix: true
        }
      }
    ]
  },
  {
    path: '/profile',
    component: Layout,
    redirect: '/profile/index',
    meta: { hidden: true },
    children: [
      {
        path: 'index',
        component: () => import(/* webpackChunkName: "profile" */ '@/views/profile/index.vue'),
        name: 'Profile',
        meta: {
          title: 'profile',
          icon: 'user',
          noCache: true
        }
      }
    ]
  }
]

let debug = DebugUtil.isDebug()
let asyncRoutesConfig: RouteConfig[] = []
if (debug) {
  asyncRoutesConfig.push(testerRouter)
  // asyncRoutesConfig.push(demoRouter)
}
let asyncRoutesCommon = [
  {
    path: '/account',
    component: Layout,
    redirect: '/account',
    meta: {
      roles: ['admin'],
      icon: 'tree',
      title: 'accountManage'
    },
    children: [
      {
        path: 'manage',
        component: () => import(/* webpackChunkName: "dashboard" */ '@/views/account-manage/UserManage.vue'),
        name: 'manage',
        meta: {
          title: 'userManage',
          icon: 'tree',
          roles: ['admin'] // or you can only set roles in sub nav
        }
      },
      {
        path: 'member',
        component: () => import(/* webpackChunkName: "dashboard" */ '@/views/account-manage/AdminManage.vue'),
        name: 'member',
        meta: {
          title: 'userAdmin',
          icon: 'tree',
          roles: ['admin'] // or you can only set roles in sub nav
        }
      }
    ]
  },
  {
    path: '/hospital',
    component: Layout,
    redirect: '/hospital',
    meta: {
      roles: ['admin'],
      icon: 'lock',
      title: 'hospitalManage'
    },
    children: [
      {
        path: 'hospital',
        component: () => import(/* webpackChunkName: "dashboard" */ '@/views/hospital/HospitalManage.vue'),
        name: 'hospital',
        meta: {
          title: 'hospitalManage',
          icon: 'lock',
          roles: ['admin'] // or you can only set roles in sub nav
        }
      },
      {
        path: 'hospitalRequest',
        component: () => import(/* webpackChunkName: "dashboard" */ '@/views/hospital/HospitalRequestManage.vue'),
        name: 'hospitalRequest',
        meta: {
          title: 'hospitalRequest',
          icon: 'lock',
          roles: ['admin'] // or you can only set roles in sub nav
        }
      },
      {
        path: 'hospitalOrder',
        component: () => import(/* webpackChunkName: "dashboard" */ '@/views/hospital/HospitalOrderManage.vue'),
        name: 'hospitalOrder',
        meta: {
          title: 'hospitalOrder',
          icon: 'lock',
          roles: ['admin'] // or you can only set roles in sub nav
        }
      }
    ]
  },
  {
    path: '/face',
    component: Layout,
    redirect: '/face',
    meta: {
      roles: ['admin'],
      icon: 'like',
      title: 'faceManage'
    },
    children: [
      {
        path: 'faceOrder',
        component: () => import(/* webpackChunkName: "dashboard" */ '@/views/face/FaceOrderManage.vue'),
        name: 'faceOrder',
        meta: {
          title: 'faceManage',
          icon: 'eye-on',
          roles: ['admin'] // or you can only set roles in sub nav
        }
      }
    ]
  },
  {
    path: '/brokerage',
    component: Layout,
    redirect: '/brokerage',
    meta: {
      roles: ['admin'],
      icon: 'money',
      title: 'brokerageManage'
    },
    children: [
      {
        path: 'brokerage',
        component: () => import(/* webpackChunkName: "dashboard" */ '@/views/brokerage/BrokerageManage.vue'),
        name: 'brokerage',
        meta: {
          title: 'brokerageManage',
          icon: 'money',
          roles: ['admin'] // or you can only set roles in sub nav
        }
      }
    ]
  },
  {
    path: '/agent',
    component: Layout,
    redirect: '/agent',
    meta: {
      roles: ['admin'],
      icon: 'user',
      title: 'agentManage'
    },
    children: [
      {
        path: 'agent',
        component: () => import(/* webpackChunkName: "dashboard" */ '@/views/agent/AgentManage.vue'),
        name: 'agent',
        meta: {
          title: 'agentManage',
          icon: 'user',
          roles: ['admin'] // or you can only set roles in sub nav
        }
      }
    ]
  },
  {
    path: '/config',
    component: Layout,
    redirect: '/config',
    meta: {
      roles: ['admin'],
      icon: 'tab',
      title: 'InfoConfigManage'
    },
    children: [
      {
        path: 'config',
        component: () => import(/* webpackChunkName: "dashboard" */ '@/views/info/InfoConfigManage.vue'),
        name: 'config',
        meta: {
          title: 'InfoConfigManage',
          icon: 'tab',
          roles: ['admin'] // or you can only set roles in sub nav
        }
      }
    ]
  }
]

// @ts-ignore
asyncRoutesConfig = asyncRoutesConfig.concat(asyncRoutesCommon)
/**
 * asyncRoutes
 * the routes that need to be dynamically loaded based on user roles
 */
export const asyncRoutes: RouteConfig[] = asyncRoutesConfig

const createRouter = () => new Router({
  // mode: 'history',  // Disabled due to Github Pages doesn't support this, enable this if you need.
  scrollBehavior: (to, from, savedPosition) => {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  },
  base: process.env.BASE_URL,
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter();
  (router as any).matcher = (newRouter as any).matcher // reset router
}

export default router
